------------------ ROCKEY4SMART Software Developer's Kit --------------------

Default password:
    
    P1:C44C, P2:C8F8, P3:0799, P4:C43B

------------------ Folder Content List --------------------------------------

API                 Programming API libraries comprise:
                    Static Win32 library
                    Dynamic Win32 library
                    COM component
                    OCX component
                    Libraries for other programming languages
Docs                Documentations for product utilities and programming APIs
Support for Win98   Driver and related component for Windows 98SE
Include             Header file
Management          License manager, production and remote update tools
Samples             Programming samples for VC, VB, Delphi, and .NET etc.
Tools               Encryption tools for different kinds of application and
                    accessorial utilities like editor tool etc.







