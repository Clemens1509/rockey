How to use Ry4S.x32

1.Copy Ry4S.x32 to Director\Xtras\.
2.Start Director.
3.Open Message Window of Director for test.
4.Input command,shouxlib and show all loaded xtra. The information must includes rockey,otherwise that means Ry4S.x32 not in the directory,xtras.

Resutl samples:

-- Welcome to Director --
showxlib
-- XLibraries:
--   Xtra: rockey
--   Xtra: filextra
--   Xtra: uihelper
--   Xtra: javaconvert
--   Xtra: quicktimesupport
--   Xtra: netlingo
--   Xtra: multiuser
--   Xtra: xmlparser
--   Xtra: mui
--   Xtra: fileio
--   Xtra: beatnik
--   Xtra: activex

The first Xtra is rockey.


5.Use command,interface, you can see rockey interfaces.Command format: put interface(xtra "rockey")

Sample:

put interface(xtra "rockey")
-- "xtra ROCKEY -- ROCKEY functions for Lingo,Release v1.00
new object me

-- ROCKEY Interface --
* rockey integer function,integer handle,integer lp1,integer lp2,integer p1,integer p2,integer p3,integer p4,string buf -- ROCKEY interface
* get_p1 -- Get return value in P1 parameter
* get_p2 -- Get return value in P2 parameter
* get_p3 -- Get return value in P3 parameter
* get_p4 -- Get return value in P4 parameter
* get_lp1 -- Get return value in lp1 parameter
* get_lp2 -- Get return value in lp2 parameter
* get_handle -- Get return value in handle parameter
* get_buf -- Get return value in buf parameter

-- Misc functions --
* hex2dec string hex --convert hexadecimal number to decimal number.
* dec2hex int dec    --convert decimal number to hexadecimal number.

rockey is the main function. It realizes functions as following:
find,open,read/write,calculate,get return code of seed and so on.

rockey function code definition:

define  RY_FIND                        	1		//Find dongle
#define  RY_FIND_NEXT			2		//Find next dongle
#define  RY_OPEN                        3		//Open dongle
#define  RY_CLOSE                       4		//Close dongle
#define  RY_READ                        5		//Read dongle
#define  RY_WRITE                       6		//Write donge
#define  RY_RANDOM                      7		//Generate random number
#define  RY_SEED                        8		//Generate return code of seed
#define  RY_WRITE_USERID		9		//Write user ID
#define  RY_READ_USERID			10		//Read user ID
#define  RY_SET_MOUDLE			11		//Set module
#define  RY_CHECK_MOUDLE		12		//Check module
#define  RY_WRITE_ARITHMETIC            13		//Write arithmetic
#define  RY_CALCULATE1			14		//Calculate 1
#define  RY_CALCULATE2			15		//Calculate 2
#define  RY_CALCULATE3			16		//Calculate 3
#define  RY_DECREASE			17		//Decrease module unit

handle:handle of dongle,
lp1,lp2:long parameters,
p1,p2,p3,p4:short parameters, 
buf:buffer for data

Detailed explanation,please refer to document.


The return value of rockey function shows operation state:

//return value 

0		//Success
5		//Wrong password or hardware ID
6               //Set hardware ID error
7		//Read/write address or length error
8		//No such command
9		//Inside error
10		//Read error
11		//Write error
12		//Random number error
13		//Seed code error
14		//Calculate error
15		//Not open dongle before operation
16		//Too much dongles opened(>16)
17		//No more dongles
18		//Use FindNext befor using Find
19		//Decrease error
20		//Arithmetic instruction error
21		//Arithmetic operator error
22		//Const number can't use on first arithmetic instruction
23		//Const number can't use on last arithmetic instruction
24		//Const value> 63

0x102		//Unknown system
0xffff		//Unknown error

0x,Hex



6.Samples

(1) Find DEMO dongle
	
        From the manual document,the input parameters of find dongle are function=p1,p2,p3,p4(password).DEMO password are 0xc44c,0xc8f8,0x0799,0xc43b.
        You need change password from hex to dec format by using calculater or hex2dec function.
	

put hex2dec("c44c")
-- "50252"

0xc44c=50252, 

put hex2dec("c8f8")
-- "51448"
put hex2dec("0799")
-- "1945"
put hex2dec("c43b")
-- "50235"

Input theses parameters as following

put rockey(1,0,0,0,50252,51448,1945,50235,"")
-- 0

return value:0=success,else is error code. Hardware ID is returned by Lp1.By callingget_lp1(),lp1 value can be get.

put get_lp1()
-- -1995600092  //hardware ID

-1995600092 is hardware ID. Using dec2hex() function to get its Hex format.

put dec2hex(-1995600092)
-- "890d8f24"//hardware ID(HEX)


(2)Open DEMO dongle



put rockey(3,0,-1995600092,0,50252,51448,1945,50235,"")
-- 0

Return value:0=success,else is error code. Dongle's handle is returned by parameter,handle.By calling get_handle(),handle can be get.

put get_handle()
-- 4

4 is dongle's handle value.

(3)Read user memory


put rockey(5,4,0,0,0,23,0,0,"")
-- 0

Return value:0=success,else is error code. User data is saved in parameter,buf.By calling get_buf(),the data can be get.

put get_buf()
-- "www.ftsafe.com"


Readed data is "www.ftsafe.com"

Other function of rockey(),please refer to document.




	


