------------------ ROCKEY4SMART Software Developer's Kit --------------------

This folder comprises libraries files for different programming languages

------------------ Folder Content List --------------------------------------

BCB         Static library for Borland C++ Builder
COM         COM component
Delphi      Delphi DCU library
Dynamic     Dynamic Win32 library
Java        Java class file
OCX         ActiveX control component
Static      Static Win32 library
Xtra        DirectorX plug-in


