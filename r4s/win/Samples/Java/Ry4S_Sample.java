import java.lang.*;


public class Ry4S_Sample
{
	public static void main(String[] args)
	{
		int k;
		short func;
		short[] handle=new short[1];
		short[] handleAll=new short[1024];
		int[] lp1=new int[1];
		int[] lp2=new int[2];
		short[] p1=new short[1];
		short[] p2=new short[1];
		short[] p3=new short[1];
		short[] p4=new short[1];
		byte[]  buffer=new byte[1024];
		short	retval;
		short[]	rc= new short[4];
		Ry4S rockey=new Ry4S();

		p1[0] = -(0xffff-0xc44c)-1;
		p2[0] = -(0xffff-0xc8f8)-1;
		p3[0] = 	0x0799;
		p4[0] = -(0xffff-0xc43b)-1;

		retval=rockey.Rockey(rockey.RY_FIND,handle,lp1,lp2,p1,p2,p3,p4,buffer);
		if(retval!=rockey.ERR_SUCCESS)
		{
			System.out.println("No Rockey4Smart Found!Err:"+retval);
			return;
		}
		System.out.println("Find Rockey4Smart:"+lp1[0]);

		retval=rockey.Rockey(rockey.RY_OPEN,handle,lp1,lp2,p1,p2,p3,p4,buffer);
		if(retval!=rockey.ERR_SUCCESS)
		{
			System.out.println("Open Rockey4Smart Failed!");
			return;
		}
		int i=1;
		handleAll[i-1]=handle[0];
		while(retval==rockey.ERR_SUCCESS)
		{
			retval=rockey.Rockey(rockey.RY_FIND_NEXT,handle,lp1,lp2,p1,p2,p3,p4,buffer);
			if(retval==rockey.ERR_NOMORE)break;
			if(retval!=rockey.ERR_SUCCESS)
			{
				System.out.println("Find next Rockey4Smart failed!");
				return;
			}
			retval=rockey.Rockey(rockey.RY_OPEN,handle,lp1,lp2,p1,p2,p3,p4,buffer);
			if(retval!=rockey.ERR_SUCCESS)
			{
                      		System.out.println("Open Rockey4Smart Failed!");
				return;
			}
			handleAll[i]=handle[0];
			i++;
			System.out.println("Find another Rockey4Smart:"+lp1[0]);
		}

		for(int j=0;j<i;j++)
		{
			handle[0]=handleAll[j];
			System.out.println();
			System.out.println("Current Rockey4Smart handle is: "+handle[0]);
			p1[0]=3;
			p2[0]=6;
//			buffer[0]=12;	buffer[1]=34;	buffer[2]=56;
//			buffer[3]=78;	buffer[4]=90;	buffer[5]=20;

			buffer[0]='A';	buffer[1]='C';	buffer[2]='c';
			buffer[3]='B';	buffer[4]='D';	buffer[5]='E';

			retval = rockey.Rockey(rockey.RY_WRITE,handle,lp1,lp2,p1,p2,p3,p4,buffer);//write dongle
			if(retval!=rockey.ERR_SUCCESS)
			{
				System.out.println("Write Rockey4Smart Failed!");
				return;
			}
			System.out.println("Write Text: "+buffer[0]+buffer[1]+buffer[2]+buffer[3]+buffer[4]+buffer[5]);

			p1[0]=3;
			p2[0]=6;
			for(int m=0;m<6;m++)buffer[m]=0;
			retval = rockey.Rockey(rockey.RY_READ,handle,lp1,lp2,p1,p2,p3,p4,buffer);//read dongle
			if(retval!=rockey.ERR_SUCCESS)
			{
				System.out.println("Read Rockey4Smart Failed!");
				return;
			}
			
			System.out.println("Read Text: "+buffer[0]+buffer[1]+buffer[2]+buffer[3]+buffer[4]+buffer[5]);

			retval=rockey.Rockey(rockey.RY_RANDOM,handle,lp1,lp2,p1,p2,p3,p4,buffer);
			if(retval!=rockey.ERR_SUCCESS)
			{
				System.out.println("Get random number failed!");
				return;
			}
			System.out.println("Random number: "+p1[0]);

			lp2[0]=0x12345678;
			retval=rockey.Rockey(rockey.RY_SEED,handle,lp1,lp2,p1,p2,p3,p4,buffer);
			if(retval!=rockey.ERR_SUCCESS)
			{
				System.out.println("Get Seed failed!");
				return;
			}
			System.out.println("Seed: "+p1[0]+","+p2[0]+","+p3[0]+","+p4[0]);
			rc[0] = p1[0];
			rc[1] = p2[0];
			rc[2] = p3[0];
			rc[3] = p4[0];

			lp1[0] = 0x88888888;
			retval = rockey.Rockey(rockey.RY_WRITE_USERID,handle,lp1,lp2,p1,p2,p3,p4,buffer);
			if(retval!=rockey.ERR_SUCCESS)
			{
				System.out.println("Write UserId failed!");
				return;
			}
			System.out.println("Write User ID: "+lp1[0]);

			lp1[0]=0;
			retval=rockey.Rockey(rockey.RY_READ_USERID,handle,lp1,lp2,p1,p2,p3,p4,buffer);
			if(retval!=rockey.ERR_SUCCESS)
			{
				System.out.println("Read UserID failed!");
				return;
			}
			System.out.println("Read User ID: "+lp1[0]);

			p1[0]=7;
			p2[0]=0x2121;
			p3[0]=0;
			retval = rockey.Rockey(rockey.RY_SET_MODULE,handle,lp1,lp2,p1,p2,p3,p4,buffer);
			if(retval!=rockey.ERR_SUCCESS)
			{
				System.out.println("Set module failed!");
				return;
			}
			System.out.println("Set module 7,pass="+p2[0]+",Decreasement not allowed!");

			p1[0] = 7;
			retval = rockey.Rockey(rockey.RY_CHECK_MODULE,handle,lp1,lp2,p1,p2,p3,p4,buffer);
			if(retval!=rockey.ERR_SUCCESS)
			{
				System.out.println("Chech Module Failed!");
				return;
			}
			System.out.print("Check module 7:");
			if(p2[0]!=0)System.out.println("Allow!");
			else System.out.println("Not Allow!");
			if(p3[0]!=0)System.out.println("Allow decresement!");
			else System.out.println("Decrsement not allowed!");

			p1[0]=0;
			buffer[0]='H';buffer[1]='=';buffer[2]='H';
			buffer[3]='^';buffer[4]='H';buffer[5]=',';
			buffer[6]='A';buffer[7]='=';buffer[8]='A';
			buffer[9]='*';buffer[10]='2';buffer[11]='3';
			buffer[12]=',';buffer[13]='F';buffer[14]='=';
			buffer[15]='B';buffer[16]='*';buffer[17]='1';
			buffer[18]='7';buffer[19]=',';buffer[20]='A';
			buffer[21]='=';buffer[22]='A';buffer[23]='+';
			buffer[24]='F';buffer[25]=',';buffer[26]='A';
			buffer[27]='=';buffer[28]='A';buffer[29]='+';
			buffer[30]='G';buffer[31]=',';buffer[32]='A';
			buffer[33]='=';buffer[34]='A';buffer[35]='<';
			buffer[36]='C';buffer[37]=',';buffer[38]='A';
			buffer[39]='=';buffer[40]='A';buffer[41]='^';
			buffer[42]='D';buffer[43]=',';buffer[44]='B';
			buffer[45]='=';buffer[46]='B';buffer[47]='^';
			buffer[48]='B';buffer[49]=',';buffer[50]='C';
			buffer[51]='=';buffer[52]='C';buffer[53]='^';
			buffer[54]='C';buffer[55]=',';buffer[56]='D';
			buffer[57]='=';buffer[58]='D';buffer[59]='^';
			buffer[60]='D';

			retval=rockey.Rockey(rockey.RY_WRITE_ARITHMETIC,handle,lp1,lp2,p1,p2,p3,p4,buffer);
			if(retval!=rockey.ERR_SUCCESS)
			{
				System.out.println("write arithmetic failed!");
				return;
			}
			System.out.println("Write Arithmetic 1,Succeed!");

			lp1[0] = 0;
			lp2[0] = 7;
			p1[0] = 5;
			p2[0] = 3;
			p3[0] = 1;
			p4[0] = -1;
			retval = rockey.Rockey(rockey.RY_CALCULATE1,handle,lp1,lp2,p1,p2,p3,p4,buffer);
			if(retval!=rockey.ERR_SUCCESS)
			{
				System.out.println("Caculate failed");
				return;
			}
			System.out.println("Calculate Input: p1=5, p2=3, p3=1, p4=0xffff");
			System.out.println("Result = ((5*23 + 3*17 + 0x2121) < 1) ^ 0xffff = BC71");
			System.out.println("Calculate Output: p1="+p1[0]+",p2="+p2[0]+",p3="+p3[0]+",p4="+p4[0]);

			p1[0]=10;
			buffer[0]='A';buffer[1]='=';buffer[2]='A';buffer[3]='+';buffer[4]='B';buffer[5]=',';
			buffer[6]='A';buffer[7]='=';buffer[8]='A';buffer[9]='+';buffer[10]='C';buffer[11]=',';
			buffer[12]='A';buffer[13]='=';buffer[14]='A';buffer[15]='+';buffer[16]='D';buffer[17]=',';
			buffer[18]='A';buffer[19]='=';buffer[20]='A';buffer[21]='+';buffer[22]='E';buffer[23]=',';
			buffer[24]='A';buffer[25]='=';buffer[26]='A';buffer[27]='+';buffer[28]='F';buffer[29]=',';
			buffer[30]='A';buffer[31]='=';buffer[32]='A';buffer[33]='+';buffer[34]='G';buffer[35]=',';
			buffer[36]='A';buffer[37]='=';buffer[38]='A';buffer[39]='+';buffer[40]='H';buffer[41]=0;

			retval=rockey.Rockey(rockey.RY_WRITE_ARITHMETIC,handle,lp1,lp2,p1,p2,p3,p4,buffer);
			if(retval!=rockey.ERR_SUCCESS)
			{
				System.out.println("Write caculate2 failed!");
				return;
			}
			System.out.println("Write caculate2,succeed!");

			lp1[0]=10;
			lp2[0]=0x12345678;
			p1[0]=1;
			p2[0]=2;
			p3[0]=3;
			p4[0]=4;
			retval=rockey.Rockey(rockey.RY_CALCULATE2,handle,lp1,lp2,p1,p2,p3,p4,buffer);
			if(retval!=rockey.ERR_SUCCESS)
			{
				System.out.println("Calculate2 failed!");
				return;
			}
			System.out.println("Calculate Input: p1=1, p2=2, p3=3, p4=4");
			System.out.println("Result = "+rc[0]+"+"+rc[1]+"+"+rc[2]+"+"+rc[3]+"+ 1 + 2 + 3 + 4");
			System.out.println("Caculate result: p1="+p1[0]+",p2="+p2[0]+",p3="+p3[0]+",p4="+p4[0]);

			p1[0]=9;
			p2[0]=5;
			p3[0]=1;
			retval=rockey.Rockey(rockey.RY_SET_MODULE,handle,lp1,lp2,p1,p2,p3,p4,buffer);
			if(retval!=rockey.ERR_SUCCESS)
			{
				System.out.println("Set module failed!");
				return;
			}

			p1[0]=17;
			buffer[0]='A';buffer[1]='=';buffer[2]='E';
			buffer[3]='|';buffer[4]='E';buffer[5]=',';
			buffer[6]='B';buffer[7]='=';buffer[8]='F';
			buffer[9]='|';buffer[10]='F';buffer[11]=',';
			buffer[12]='C';buffer[13]='=';buffer[14]='G';
			buffer[15]='|';buffer[16]='G';buffer[17]=',';
			buffer[18]='D';buffer[19]='=';buffer[20]='H';
			buffer[21]='|';buffer[22]='H';

			retval=rockey.Rockey(rockey.RY_WRITE_ARITHMETIC,handle,lp1,lp2,p1,p2,p3,p4,buffer);
			if(retval!=rockey.ERR_SUCCESS)
			{
				System.out.println("write arithmetic3 failed!");
				return;
			}
			System.out.println("Write arithmetic 3,Succeed!");

			lp1[0]=17;
			lp2[0]=6;
			p1[0]=1;
			p2[0]=2;
			p3[0]=3;
			p4[0]=4;
			retval = rockey.Rockey(rockey.RY_CALCULATE3,handle,lp1,lp2,p1,p2,p3,p4,buffer);
			if(retval!=rockey.ERR_SUCCESS)
			{
				System.out.println("");
				return;
			}
			System.out.println("Show module from 6: p1="+p1[0]+",p2="+p2[0]+",p3="+p3[0]+",p4="+p4[0]);

			p1[0]=9;
			retval=rockey.Rockey(rockey.RY_DECREASE,handle,lp1,lp2,p1,p2,p3,p4,buffer);
			if(retval!=rockey.ERR_SUCCESS)
			{
				System.out.println("Decrese failed!");
				return;
			}
			System.out.println("Decrease module 9.");


			lp1[0] = 17;
			lp2[0] = 6;
			p1[0] = 1;
			p2[0] = 2;
			p3[0] = 3;
			p4[0] = 4;

			retval=rockey.Rockey(rockey.RY_CALCULATE3,handle,lp1,lp2,p1,p2,p3,p4,buffer);
			if(retval!=rockey.ERR_SUCCESS)
			{
				System.out.println("caculate 3 failed!");
				return;
			}
			System.out.println("Show module from 6: p1="+p1[0]+",p2="+p2[0]+",p3="+p3[0]+",p4="+p4[0]);

			//Des test
			System.out.println("DES test start...");
			byte[] keybuf=new byte[16];
			p1[0]=rockey.DES_SINGLE_MODE;
			for(k=0;k<8;k++)keybuf[k]=(byte)k;
			retval=rockey.Rockey(rockey.RY_SET_DES_KEY,handle,lp1,lp2,p1,p2,p3,p4,buffer);
			if(retval!=rockey.ERR_SUCCESS)
			{
				System.out.println("Set Des key failed!");
				return;
			}
			System.out.println("Des key:");
			for(k=0;k<8;k++)System.out.print(keybuf[k]);
			System.out.println();
			System.out.println("Set DES Key successfully!");
			System.out.println("Des key:");
			for(k=0;k<8;k++)System.out.print(keybuf[k]);
			System.out.println();

			//Encrypt
			p1[0]=rockey.DES_SINGLE_MODE;
			p2[0]=128;
			for(k=0;k<p2[0];k++)buffer[k]='a';
			System.out.println("Data before encrypt:");
			for(k=0;k<p2[0];k++)System.out.print(buffer[k]);
			System.out.println();
			retval=rockey.Rockey(rockey.RY_DES_ENC,handle,lp1,lp2,p1,p2,p3,p4,buffer);
			if(retval!=rockey.ERR_SUCCESS)
			{
				System.out.println("Des encrypt failed!");
				return;
			}
			System.out.println("DES encrypt successfully!");
			System.out.println("Data after encrypt:");
			for(k=0;k<p2[0];k++)System.out.print(buffer[k]);
			System.out.println();
			
			//Decrypt
			retval=rockey.Rockey(rockey.RY_DES_DEC,handle,lp1,lp2,p1,p2,p3,p4,buffer);
			if(retval!=rockey.ERR_SUCCESS)
			{
				System.out.println("Des decrypt failed!");
				return;
			}
			System.out.println("DES decrypt successfully!");
			System.out.println("Data after decrypt:");
			for(k=0;k<p2[0];k++)System.out.print(buffer[k]);
			System.out.println();

			//RSA
			//Set RSA N
			buffer[0]=(byte)191;	buffer[1]=(byte)25;	buffer[2]=(byte)33;	buffer[3]=(byte)32;
			buffer[4]=(byte)213;	buffer[5]=(byte)241;	buffer[6]=(byte)45;	buffer[7]=(byte)44;
			buffer[8]=(byte)252;	buffer[9]=(byte)169;	buffer[10]=(byte)197;	buffer[11]=(byte)176;
			buffer[12]=(byte)59;	buffer[13]=(byte)209;	buffer[14]=(byte)241;	buffer[15]=(byte)92;
			buffer[16]=(byte)142;	buffer[17]=(byte)136;	buffer[18]=(byte)144;	buffer[19]=(byte)40;
			buffer[20]=(byte)170;	buffer[21]=(byte)94;	buffer[22]=(byte)147;	buffer[23]=(byte)97;
			buffer[24]=(byte)185;	buffer[25]=(byte)135;	buffer[26]=(byte)234;	buffer[27]=(byte)134;
			buffer[28]=(byte)143;	buffer[29]=(byte)84;	buffer[30]=(byte)152;	buffer[31]=(byte)23;
			buffer[32]=(byte)27;	buffer[33]=(byte)72;	buffer[34]=(byte)238;	buffer[35]=(byte)245;
			buffer[36]=(byte)2;	buffer[37]=(byte)144;	buffer[38]=(byte)91;	buffer[39]=(byte)70;
			buffer[40]=(byte)57;	buffer[41]=(byte)20;	buffer[42]=(byte)192;	buffer[43]=(byte)98;
			buffer[44]=(byte)77;	buffer[45]=(byte)31;	buffer[46]=(byte)111;	buffer[47]=(byte)227;
			buffer[48]=(byte)16;	buffer[49]=(byte)115;	buffer[50]=(byte)182;	buffer[51]=(byte)211;
			buffer[52]=(byte)95;	buffer[53]=(byte)143;	buffer[54]=(byte)108;	buffer[55]=(byte)241;
			buffer[56]=(byte)115;	buffer[57]=(byte)2;	buffer[58]=(byte)248;	buffer[59]=(byte)62;
			buffer[60]=(byte)121;	buffer[61]=(byte)134;	buffer[62]=(byte)55;	buffer[63]=(byte)10;
			buffer[64]=(byte)30;	buffer[65]=(byte)74;	buffer[66]=(byte)145;	buffer[67]=(byte)132;
			buffer[68]=(byte)79;	buffer[69]=(byte)159;	buffer[70]=(byte)156;	buffer[71]=(byte)18;
			buffer[72]=(byte)223;	buffer[73]=(byte)175;	buffer[74]=(byte)84;	buffer[75]=(byte)197;
			buffer[76]=(byte)130;	buffer[77]=(byte)17;	buffer[78]=(byte)95;	buffer[79]=(byte)230;
			buffer[80]=(byte)200;	buffer[81]=(byte)202;	buffer[82]=(byte)68;	buffer[83]=(byte)202;
			buffer[84]=(byte)132;	buffer[85]=(byte)22;	buffer[86]=(byte)150;	buffer[87]=(byte)18;
			buffer[88]=(byte)193;	buffer[89]=(byte)127;	buffer[90]=(byte)10;	buffer[91]=(byte)42;
			buffer[92]=(byte)158;	buffer[93]=(byte)129;	buffer[94]=(byte)185;	buffer[95]=(byte)153;
			buffer[96]=(byte)219;	buffer[97]=(byte)226;	buffer[98]=(byte)70;	buffer[99]=(byte)56;
			buffer[100]=(byte)100;buffer[101]=(byte)74;	buffer[102]=(byte)149;buffer[103]=(byte)14;
			buffer[104]=(byte)92;	buffer[105]=(byte)14;	buffer[106]=(byte)201;buffer[107]=(byte)106;
			buffer[108]=(byte)93;	buffer[109]=(byte)221;buffer[110]=(byte)96;	buffer[111]=(byte)221;
			buffer[112]=(byte)148;buffer[113]=(byte)233;buffer[114]=(byte)46;	buffer[115]=(byte)75;
			buffer[116]=(byte)195;buffer[117]=(byte)70;	buffer[118]=(byte)77;	buffer[119]=(byte)157;
			buffer[120]=(byte)54;	buffer[121]=(byte)99;	buffer[122]=(byte)95;	buffer[123]=(byte)233;
			buffer[124]=(byte)8;	buffer[125]=(byte)180;buffer[126]=(byte)4;	buffer[127]=(byte)178;	

			retval=rockey.Rockey(rockey.RY_SET_RSAKEY_N,handle,lp1,lp2,p1,p2,p3,p4,buffer);
			if(retval!=rockey.ERR_SUCCESS)
			{
				System.out.println("Set Rsakey N failed!");
				return;
			}
			System.out.println("Set RSAKey N successfully!");

			//Set RSA D
			buffer[0]=(byte)117;	buffer[1]=(byte)86;	buffer[2]=(byte)160;	buffer[3]=(byte)104;
			buffer[4]=(byte)137;	buffer[5]=(byte)51;	buffer[6]=(byte)8;	buffer[7]=(byte)4;
			buffer[8]=(byte)73;	buffer[9]=(byte)178;	buffer[10]=(byte)136;	buffer[11]=(byte)200;
			buffer[12]=(byte)145;	buffer[13]=(byte)26;	buffer[14]=(byte)66;	buffer[15]=(byte)84;
			buffer[16]=(byte)48;	buffer[17]=(byte)143;	buffer[18]=(byte)169;	buffer[19]=(byte)74;
			buffer[20]=(byte)251;	buffer[21]=(byte)86;	buffer[22]=(byte)25;	buffer[23]=(byte)188;
			buffer[24]=(byte)162;	buffer[25]=(byte)3;	buffer[26]=(byte)162;	buffer[27]=(byte)131;
			buffer[28]=(byte)107;	buffer[29]=(byte)81;	buffer[30]=(byte)3;	buffer[31]=(byte)49;
			buffer[32]=(byte)251;	buffer[33]=(byte)133;	buffer[34]=(byte)157;	buffer[35]=(byte)164;
			buffer[36]=(byte)147;	buffer[37]=(byte)138;	buffer[38]=(byte)72;	buffer[39]=(byte)81;
			buffer[40]=(byte)111;	buffer[41]=(byte)137;	buffer[42]=(byte)65;	buffer[43]=(byte)48;
			buffer[44]=(byte)154;	buffer[45]=(byte)11;	buffer[46]=(byte)42;	buffer[47]=(byte)215;
			buffer[48]=(byte)188;	buffer[49]=(byte)180;	buffer[50]=(byte)175;	buffer[51]=(byte)54;
			buffer[52]=(byte)79;	buffer[53]=(byte)40;	buffer[54]=(byte)163;	buffer[55]=(byte)60;
			buffer[56]=(byte)123;	buffer[57]=(byte)20;	buffer[58]=(byte)96;	buffer[59]=(byte)196;
			buffer[60]=(byte)188;	buffer[61]=(byte)10;	buffer[62]=(byte)254;	buffer[63]=(byte)213;
			buffer[64]=(byte)17;	buffer[65]=(byte)244;	buffer[66]=(byte)224;	buffer[67]=(byte)61;
			buffer[68]=(byte)197;	buffer[69]=(byte)77;	buffer[70]=(byte)6;	buffer[71]=(byte)3;
			buffer[72]=(byte)22;	buffer[73]=(byte)32;	buffer[74]=(byte)157;	buffer[75]=(byte)117;
			buffer[76]=(byte)92;	buffer[77]=(byte)218;	buffer[78]=(byte)114;	buffer[79]=(byte)245;
			buffer[80]=(byte)73;	buffer[81]=(byte)10;	buffer[82]=(byte)47;	buffer[83]=(byte)221;
			buffer[84]=(byte)35;	buffer[85]=(byte)24;	buffer[86]=(byte)123;	buffer[87]=(byte)235;
			buffer[88]=(byte)79;	buffer[89]=(byte)237;	buffer[90]=(byte)175;	buffer[91]=(byte)153;
			buffer[92]=(byte)209;	buffer[93]=(byte)193;	buffer[94]=(byte)156;	buffer[95]=(byte)80;
			buffer[96]=(byte)253;	buffer[97]=(byte)255;	buffer[98]=(byte)54;	buffer[99]=(byte)169;
			buffer[100]=(byte)111;buffer[101]=(byte)113;buffer[102]=(byte)196;buffer[103]=(byte)202;
			buffer[104]=(byte)121;buffer[105]=(byte)77;	buffer[106]=(byte)27;	buffer[107]=(byte)100;
			buffer[108]=(byte)179;buffer[109]=(byte)12;	buffer[110]=(byte)211;buffer[111]=(byte)140;
			buffer[112]=(byte)217;buffer[113]=(byte)44;	buffer[114]=(byte)182;buffer[115]=(byte)71;
			buffer[116]=(byte)208;buffer[117]=(byte)247;buffer[118]=(byte)92;	buffer[119]=(byte)218;
			buffer[120]=(byte)41;	buffer[121]=(byte)105;buffer[122]=(byte)234;buffer[123]=(byte)216;
			buffer[124]=(byte)85;	buffer[125]=(byte)118;buffer[126]=(byte)54;	buffer[127]=(byte)8;
			retval=rockey.Rockey(rockey.RY_SET_RSAKEY_D,handle,lp1,lp2,p1,p2,p3,p4,buffer);
			if(retval!=rockey.ERR_SUCCESS)
			{
				System.out.println("Set Rsakey D failed!");
				return;
			}
			System.out.println("Set RSAKey D successfully!");
		
			//RSA public key encrypt
			p1[0]=rockey.RSA_PUBLIC_KEY;
			p2[0]=20;				//length
			p3[0]=rockey.RSA_ROCKEY_PADDING;
			for(k=0;k<p2[0];k++)buffer[k]='A';
			System.out.println("Data before encrypt:");
			for(k=0;k<p2[0];k++)System.out.print(buffer[k]);
			System.out.println();
			retval=rockey.Rockey(rockey.RY_RSA_ENC,handle,lp1,lp2,p1,p2,p3,p4,buffer);
			if(retval!=rockey.ERR_SUCCESS)
			{
				System.out.println("Rsa public key encrypt failed!");
				return;
			}
			System.out.println("Rsa public key encrypt successfully!");
			System.out.println("Data after encrypt:");
			for(k=0;k<128;k++)System.out.print(buffer[k]+",");
			System.out.println();

			//RSA private key decrypt
			p1[0]=rockey.RSA_PRIVATE_KEY;
			p2[0]=0;				//length
			//p3[0]=rockey.RSA_ROCKEY_PADDING;
			
			retval=rockey.Rockey(rockey.RY_RSA_DEC,handle,lp1,lp2,p1,p2,p3,p4,buffer);
			if(retval!=rockey.ERR_SUCCESS)
			{
				System.out.println("Rsa public key encrypt failed!");
				return;
			}
			System.out.println("Rsa private key decrypt successfully!");
			System.out.println("Data after decrypt:"+retval+','+p2[0]);
			for(k=0;k<p2[0];k++)System.out.print(buffer[k]);
			System.out.println();



			retval=rockey.Rockey(rockey.RY_CLOSE,handle,lp1,lp2,p1,p2,p3,p4,buffer);
			if(retval!=rockey.ERR_SUCCESS)
			{
				System.out.println("Close Rockey4Smart failed!");
				return;
			}
		}
	}
}
