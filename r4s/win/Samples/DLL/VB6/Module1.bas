Attribute VB_Name = "Module1"
' Basic form:
'(1) Find dongle
'Input:
'    function = 0
'    *p1 = pass1
'    *p2 = pass2
'    *p3 = pass3
'    *p4 = pass4
'Output:
'    *lp1, Hardware ID of dongle
'Return value
'    0 means success,else is error code
'
'(2) Search next dongle
'Input:
'    function = 1
'    *p1 = pass1
'    *p2 = pass2
'    *p3 = pass3
'    *p4 = pass4
'Output:
'    *lp1, Hardware ID of dongle
'Return value
'    0 means success,else is error code
'
'(3) Open dongle
'Input:
'    function = 2
'    *p1 = pass1
'    *p2 = pass2
'    *p3 = pass3
'    *p4 = pass4
'    *lp1 = hardware ID
'Output:
'    *handle, dongle's handle
'Return value
'    0 means success,else is error code
'
'(4) Close dongle
'Input:
'    function = 3
'    *handle = dongle's handle
'Return value:
'    0 means success,else is error code
'
'(5) Read dongle
'Input:
'    function = 4
'    *handle = dongle's handle
'    *p1 = pos
'    *p2 = length
'    buffer = poiter of buffer
'Output:
'    buffer,readed data
'Return value
'    0 means success,else is error code.
'
'(6) Write dongle
 'Input:
 '    function = 5
 '   *handle = dongle's handle
 '   *p1 = pos
 '   *p2 = length
 '   buffer = pointer of buffer
'Return value:
'    0 means success,else is error code.
'
'(7) Generate random number
'Input:
'    function = 6
'    *handle = dongle's handle
'Output:
'    *p1 = random number
'    0 means success, else is error code
'
'(8) Generate seed code
'Input:
'   function = 7
'   *handle = dongle's handle
'   *lp2 = seed code
'Output:
'    *p1 = return code 1
'    *p2 = return code 2
'    *p3 = return code 3
'    *p4 = return code 4
'Return value:
'    0 means success,else is error code
'
'(9) Write user ID [*]
'Input:
'    function = 8
'    *handle = dongle's handle
'    *lp1 =User ID
'Return value:
'    0 means success,else is error code
'
'(10) Read user ID
'Input:
'     function = 9
'     *handle = dongle's handle
'Return value:
'     *lp1 = User ID
'     0 means success,else is error code
'
'(11) Set module[*]
'Input:
'     function = 10
'     *handle = dongle's handle
'     *p1 = module number
'     *p2 = user module password
'     *p3 = allow decrease or not(1 = allow, 0 = not allow)
'Return value:
'     0 means success,else is error code
'
'(12) Check module
'Input:
'     function = 11
'     *handle = dongle's handle
'     *p1 = module number
'Output:
'     *p2 = 1 this module is valid
'     *p3 = 1 this module is allowed decrease
'Return value:
'     0 means success,else is error code
'
'(13) Write arithmetic [*]
'Input:
'     function = 12
'     *handle = dongle's handle
'     *p1 = pos
'     buffer = arithmetic instruction string
'Return value:
'     0 means success,else is error code
'
'(14) Calculate 1 (module content, HID high 16bit, HID low 16bit, random number)
'Input:
'     function = 13
'     *handle = dongle's number
'     *lp1 = calculate begin pos
'     *lp2 = module number
'     *p1 = input value 1
'     *p2 = input value 2
'     *p3 = input value 3
'     *p4 = input value 4
'Output:
'     *p1 = return value 1
'     *p2 = return value 2
'     *p3 = return value 3
'     *p4 = return value 4
'Return value:
'     0 means success,else is error code
'
'(15) Calculate 2
'Input:
'     function = 14
'     *handle = dongle's handle
'     *lp1 = calculate begin pos
'     *lp2 = seed code
'     *p1 = input value 1
'     *p2 = input value 2
'     *p3 = input value 3
'     *p4 = input value 4
'Output:
'     *p1 = return value 1
'     *p2 = return value 2
'     *p3 = return value 3
'     *p4 = return value 4
'Return value:
'     0 means success,else is error code
'
'(16) Calculate 3
'Input:
'     function = 15
'     *handle = dongle's handle
'     *lp1 = calculate begin pos
'     *lp2 = password begin pos
'     *p1 = input value 1
'     *p2 = input value 2
'     *p3 = input value 3
'     *p4 = input value 4
'Output:
'     *p1 = return value 1
'     *p2 = return value 2
'     *p3 = return value 3
'     *p4 = return value 4
'Return value:
'
'(17) Decrease module unit
'Input:
'     function = 16
'     *handle = dongle;s handle
'     *p1 = Unit number
'Return value:
'   0 means success,else is error code


Declare Function Rockey Lib "Ry4S.dll" (ByVal fcode As Integer, ByRef handle As Integer, ByRef lp1 As Long, ByRef lp2 As Long, ByRef p1 As Integer, ByRef p2 As Integer, ByRef p3 As Integer, ByRef p4 As Integer, ByVal buffer As Any) As Integer

Global Const RY_FIND = 1                                'Find dongle
Global Const RY_FIND_NEXT = 2                           'Find next dongle
Global Const RY_OPEN = 3                                'Open dongle
Global Const RY_CLOSE = 4                               'Close dongle
Global Const RY_READ = 5                                'Read dongle
Global Const RY_WRITE = 6                               'Write dongle
Global Const RY_RANDOM = 7                              'Generate random number
Global Const RY_SEED = 8                                'Generate seed code
Global Const RY_WRITE_USERID = 9                        'Write User ID
Global Const RY_READ_USERID = 10                        'Read User ID
Global Const RY_SET_MOUDLE = 11                         'Set module
Global Const RY_CHECK_MOUDLE = 12                       'Check module
Global Const RY_WRITE_ARITHMETIC = 13                   'Write arithmetic
Global Const RY_CALCULATE1 = 14                         'Calculate 1
Global Const RY_CALCULATE2 = 15                         'Calculate 2
Global Const RY_CALCULATE3 = 16                         'Calculate 3
Global Const RY_DECREASE = 17                           'Decrease module unit

' Error code list
Global Const ERR_SUCCESS = 0                            'no error,success.

Global Const ERR_NO_ROCKEY = 3                          'No ROCKEY4 smart dongle
Global Const ERR_INVALID_PASSWORD = 4                   'Found ROCKEY4 smart dongle,but base password is wrong
Global Const ERR_INVALID_PASSWORD_OR_ID = 5             'Wrong password or Rockey4 smart hardware ID
Global Const ERR_SETID = 6                              'Set ROCKEY4 smart dongle HID wrong
Global Const ERR_INVALID_ADDR_OR_SIZE = 7               'Read/write address or length wrong
Global Const ERR_UNKNOWN_COMMAND = 8                    'No such command
Global Const ERR_NOTBELEVEL3 = 9                        'Inside error
Global Const ERR_READ = 10                              'Read data error
Global Const ERR_WRITE = 11                             'Write data error
Global Const ERR_RANDOM = 12                            'Random error
Global Const ERR_SEED = 13                              'Seed error
Global Const ERR_CALCULATE = 14                         'Calculate error
Global Const ERR_NO_OPEN = 15                           'No open dongle before operating it
Global Const ERR_OPEN_OVERFLOW = 16                     'Open too much dongles(>16)
Global Const ERR_NOMORE = 17                            'No more dongles
Global Const ERR_NEED_FIND = 18                         'use FindNext before using Find
Global Const ERR_DECREASE = 19                          'Decrease error

Global Const ERR_AR_BADCOMMAND = 20                     'Arithmetic instruction error
Global Const ERR_AR_UNKNOWN_OPCODE = 21                 'Arithmetic operator error
Global Const ERR_AR_WRONGBEGIN = 22                     'Const number can't use on first arithmetic instruction
Global Const ERR_AR_WRONG_END = 23                      'Const number can't use on last arithmetic instruction
Global Const ERR_AR_VALUEOVERFLOW = 24                  'Const number> 63
Global Const ERR_UNKNOWN = &HFFFF                       'Unknown error

Global Const ERR_RECEIVE_NULL = &H100                   'Receive null

Public Function Cuint(ByVal v As Integer) As Long
Dim r As Long

If (v > 0) Then
r = v
Else
r = 65536 + v
End If
Cuint = r
End Function
