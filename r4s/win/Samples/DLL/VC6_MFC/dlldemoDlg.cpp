// dlldemoDlg.cpp : implementation file
//

#include "stdafx.h"
#include "dlldemo.h"
#include "dlldemoDlg.h"
#include "ry4s.h"

#ifdef _DEBUG
#define new DEBUG_NEW
#undef THIS_FILE
static char THIS_FILE[] = __FILE__;
#endif

/////////////////////////////////////////////////////////////////////////////
// CAboutDlg dialog used for App About

CString& HexBufferToString(CString& str,BYTE* buf,int len)
{
	str = "0x";
	for(int k=0;k<len;k++)
	{
		CString strTemp;
		strTemp.Format("%02x",buf[k]);
		str += strTemp;
	}
	return str;
}
void SetN(BYTE* buffer)
{
	buffer[0]=(char)191;	buffer[1]=(char)25;		buffer[2]=(char)33;		buffer[3]=(char)32;
	buffer[4]=(char)213;	buffer[5]=(char)241;	buffer[6]=(char)45;		buffer[7]=(char)44;
	buffer[8]=(char)252;	buffer[9]=(char)169;	buffer[10]=(char)197;	buffer[11]=(char)176;
	buffer[12]=(char)59;	buffer[13]=(char)209;	buffer[14]=(char)241;	buffer[15]=(char)92;
	buffer[16]=(char)142;	buffer[17]=(char)136;	buffer[18]=(char)144;	buffer[19]=(char)40;
	buffer[20]=(char)170;	buffer[21]=(char)94;	buffer[22]=(char)147;	buffer[23]=(char)97;
	buffer[24]=(char)185;	buffer[25]=(char)135;	buffer[26]=(char)234;	buffer[27]=(char)134;
	buffer[28]=(char)143;	buffer[29]=(char)84;	buffer[30]=(char)152;	buffer[31]=(char)23;
	buffer[32]=(char)27;	buffer[33]=(char)72;	buffer[34]=(char)238;	buffer[35]=(char)245;
	buffer[36]=(char)2;		buffer[37]=(char)144;	buffer[38]=(char)91;	buffer[39]=(char)70;
	buffer[40]=(char)57;	buffer[41]=(char)20;	buffer[42]=(char)192;	buffer[43]=(char)98;
	buffer[44]=(char)77;	buffer[45]=(char)31;	buffer[46]=(char)111;	buffer[47]=(char)227;
	buffer[48]=(char)16;	buffer[49]=(char)115;	buffer[50]=(char)182;	buffer[51]=(char)211;
	buffer[52]=(char)95;	buffer[53]=(char)143;	buffer[54]=(char)108;	buffer[55]=(char)241;
	buffer[56]=(char)115;	buffer[57]=(char)2;		buffer[58]=(char)248;	buffer[59]=(char)62;
	buffer[60]=(char)121;	buffer[61]=(char)134;	buffer[62]=(char)55;	buffer[63]=(char)10;
	buffer[64]=(char)30;	buffer[65]=(char)74;	buffer[66]=(char)145;	buffer[67]=(char)132;
	buffer[68]=(char)79;	buffer[69]=(char)159;	buffer[70]=(char)156;	buffer[71]=(char)18;
	buffer[72]=(char)223;	buffer[73]=(char)175;	buffer[74]=(char)84;	buffer[75]=(char)197;
	buffer[76]=(char)130;	buffer[77]=(char)17;	buffer[78]=(char)95;	buffer[79]=(char)230;
	buffer[80]=(char)200;	buffer[81]=(char)202;	buffer[82]=(char)68;	buffer[83]=(char)202;
	buffer[84]=(char)132;	buffer[85]=(char)22;	buffer[86]=(char)150;	buffer[87]=(char)18;
	buffer[88]=(char)193;	buffer[89]=(char)127;	buffer[90]=(char)10;	buffer[91]=(char)42;
	buffer[92]=(char)158;	buffer[93]=(char)129;	buffer[94]=(char)185;	buffer[95]=(char)153;
	buffer[96]=(char)219;	buffer[97]=(char)226;	buffer[98]=(char)70;	buffer[99]=(char)56;
	buffer[100]=(char)100;	buffer[101]=(char)74;	buffer[102]=(char)149;	buffer[103]=(char)14;
	buffer[104]=(char)92;	buffer[105]=(char)14;	buffer[106]=(char)201;	buffer[107]=(char)106;
	buffer[108]=(char)93;	buffer[109]=(char)221;	buffer[110]=(char)96;	buffer[111]=(char)221;
	buffer[112]=(char)148;	buffer[113]=(char)233;	buffer[114]=(char)46;	buffer[115]=(char)75;
	buffer[116]=(char)195;	buffer[117]=(char)70;	buffer[118]=(char)77;	buffer[119]=(char)157;
	buffer[120]=(char)54;	buffer[121]=(char)99;	buffer[122]=(char)95;	buffer[123]=(char)233;
	buffer[124]=(char)8;	buffer[125]=(char)180;	buffer[126]=(char)4;	buffer[127]=(char)178;
}
void SetD(BYTE* buffer)
{
	buffer[0]=(char)117;	buffer[1]=(char)86;		buffer[2]=(char)160;	buffer[3]=(char)104;
	buffer[4]=(char)137;	buffer[5]=(char)51;		buffer[6]=(char)8;		buffer[7]=(char)4;
	buffer[8]=(char)73;		buffer[9]=(char)178;	buffer[10]=(char)136;	buffer[11]=(char)200;
	buffer[12]=(char)145;	buffer[13]=(char)26;	buffer[14]=(char)66;	buffer[15]=(char)84;
	buffer[16]=(char)48;	buffer[17]=(char)143;	buffer[18]=(char)169;	buffer[19]=(char)74;
	buffer[20]=(char)251;	buffer[21]=(char)86;	buffer[22]=(char)25;	buffer[23]=(char)188;
	buffer[24]=(char)162;	buffer[25]=(char)3;		buffer[26]=(char)162;	buffer[27]=(char)131;
	buffer[28]=(char)107;	buffer[29]=(char)81;	buffer[30]=(char)3;		buffer[31]=(char)49;
	buffer[32]=(char)251;	buffer[33]=(char)133;	buffer[34]=(char)157;	buffer[35]=(char)164;
	buffer[36]=(char)147;	buffer[37]=(char)138;	buffer[38]=(char)72;	buffer[39]=(char)81;
	buffer[40]=(char)111;	buffer[41]=(char)137;	buffer[42]=(char)65;	buffer[43]=(char)48;
	buffer[44]=(char)154;	buffer[45]=(char)11;	buffer[46]=(char)42;	buffer[47]=(char)215;
	buffer[48]=(char)188;	buffer[49]=(char)180;	buffer[50]=(char)175;	buffer[51]=(char)54;
	buffer[52]=(char)79;	buffer[53]=(char)40;	buffer[54]=(char)163;	buffer[55]=(char)60;
	buffer[56]=(char)123;	buffer[57]=(char)20;	buffer[58]=(char)96;	buffer[59]=(char)196;
	buffer[60]=(char)188;	buffer[61]=(char)10;	buffer[62]=(char)254;	buffer[63]=(char)213;
	buffer[64]=(char)17;	buffer[65]=(char)244;	buffer[66]=(char)224;	buffer[67]=(char)61;
	buffer[68]=(char)197;	buffer[69]=(char)77;	buffer[70]=(char)6;		buffer[71]=(char)3;
	buffer[72]=(char)22;	buffer[73]=(char)32;	buffer[74]=(char)157;	buffer[75]=(char)117;
	buffer[76]=(char)92;	buffer[77]=(char)218;	buffer[78]=(char)114;	buffer[79]=(char)245;
	buffer[80]=(char)73;	buffer[81]=(char)10;	buffer[82]=(char)47;	buffer[83]=(char)221;
	buffer[84]=(char)35;	buffer[85]=(char)24;	buffer[86]=(char)123;	buffer[87]=(char)235;
	buffer[88]=(char)79;	buffer[89]=(char)237;	buffer[90]=(char)175;	buffer[91]=(char)153;
	buffer[92]=(char)209;	buffer[93]=(char)193;	buffer[94]=(char)156;	buffer[95]=(char)80;
	buffer[96]=(char)253;	buffer[97]=(char)255;	buffer[98]=(char)54;	buffer[99]=(char)169;
	buffer[100]=(char)111;	buffer[101]=(char)113;	buffer[102]=(char)196;	buffer[103]=(char)202;
	buffer[104]=(char)121;	buffer[105]=(char)77;	buffer[106]=(char)27;	buffer[107]=(char)100;
	buffer[108]=(char)179;	buffer[109]=(char)12;	buffer[110]=(char)211;	buffer[111]=(char)140;
	buffer[112]=(char)217;	buffer[113]=(char)44;	buffer[114]=(char)182;	buffer[115]=(char)71;
	buffer[116]=(char)208;	buffer[117]=(char)247;	buffer[118]=(char)92;	buffer[119]=(char)218;
	buffer[120]=(char)41;	buffer[121]=(char)105;	buffer[122]=(char)234;	buffer[123]=(char)216;
	buffer[124]=(char)85;	buffer[125]=(char)118;	buffer[126]=(char)54;	buffer[127]=(char)8;
}


class CAboutDlg : public CDialog
{
public:
	CAboutDlg();

// Dialog Data
	//{{AFX_DATA(CAboutDlg)
	enum { IDD = IDD_ABOUTBOX };
	//}}AFX_DATA

	// ClassWizard generated virtual function overrides
	//{{AFX_VIRTUAL(CAboutDlg)
	protected:
	virtual void DoDataExchange(CDataExchange* pDX);    // DDX/DDV support
	//}}AFX_VIRTUAL

// Implementation
protected:
	//{{AFX_MSG(CAboutDlg)
	//}}AFX_MSG
	DECLARE_MESSAGE_MAP()
};

CAboutDlg::CAboutDlg() : CDialog(CAboutDlg::IDD)
{
	//{{AFX_DATA_INIT(CAboutDlg)
	//}}AFX_DATA_INIT
}

void CAboutDlg::DoDataExchange(CDataExchange* pDX)
{
	CDialog::DoDataExchange(pDX);
	//{{AFX_DATA_MAP(CAboutDlg)
	//}}AFX_DATA_MAP
}

BEGIN_MESSAGE_MAP(CAboutDlg, CDialog)
	//{{AFX_MSG_MAP(CAboutDlg)
		// No message handlers
	//}}AFX_MSG_MAP
END_MESSAGE_MAP()

/////////////////////////////////////////////////////////////////////////////
// CDlldemoDlg dialog

CDlldemoDlg::CDlldemoDlg(CWnd* pParent /*=NULL*/)
	: CDialog(CDlldemoDlg::IDD, pParent)
{
	//{{AFX_DATA_INIT(CDlldemoDlg)
	//}}AFX_DATA_INIT
	// Note that LoadIcon does not require a subsequent DestroyIcon in Win32
	m_hIcon = AfxGetApp()->LoadIcon(IDR_MAINFRAME);
}

void CDlldemoDlg::DoDataExchange(CDataExchange* pDX)
{
	CDialog::DoDataExchange(pDX);
	//{{AFX_DATA_MAP(CDlldemoDlg)
	DDX_Control(pDX, IDC_LIST_INFO, mList);
	//}}AFX_DATA_MAP
}

BEGIN_MESSAGE_MAP(CDlldemoDlg, CDialog)
	//{{AFX_MSG_MAP(CDlldemoDlg)
	ON_WM_SYSCOMMAND()
	ON_WM_PAINT()
	ON_WM_QUERYDRAGICON()
	ON_BN_CLICKED(IDC_BUTTON_TEST, OnButtonTest)
	//}}AFX_MSG_MAP
END_MESSAGE_MAP()

/////////////////////////////////////////////////////////////////////////////
// CDlldemoDlg message handlers

BOOL CDlldemoDlg::OnInitDialog()
{
	CDialog::OnInitDialog();

	// Add "About..." menu item to system menu.

	// IDM_ABOUTBOX must be in the system command range.
	ASSERT((IDM_ABOUTBOX & 0xFFF0) == IDM_ABOUTBOX);
	ASSERT(IDM_ABOUTBOX < 0xF000);

	CMenu* pSysMenu = GetSystemMenu(FALSE);
	if (pSysMenu != NULL)
	{
		CString strAboutMenu;
		strAboutMenu.LoadString(IDS_ABOUTBOX);
		if (!strAboutMenu.IsEmpty())
		{
			pSysMenu->AppendMenu(MF_SEPARATOR);
			pSysMenu->AppendMenu(MF_STRING, IDM_ABOUTBOX, strAboutMenu);
		}
	}

	// Set the icon for this dialog.  The framework does this automatically
	//  when the application's main window is not a dialog
	SetIcon(m_hIcon, TRUE);			// Set big icon
	SetIcon(m_hIcon, FALSE);		// Set small icon
	
	// TODO: Add extra initialization here
	
	return TRUE;  // return TRUE  unless you set the focus to a control
}

void CDlldemoDlg::OnSysCommand(UINT nID, LPARAM lParam)
{
	if ((nID & 0xFFF0) == IDM_ABOUTBOX)
	{
		CAboutDlg dlgAbout;
		dlgAbout.DoModal();
	}
	else
	{
		CDialog::OnSysCommand(nID, lParam);
	}
}

// If you add a minimize button to your dialog, you will need the code below
//  to draw the icon.  For MFC applications using the document/view model,
//  this is automatically done for you by the framework.

void CDlldemoDlg::OnPaint() 
{
	if (IsIconic())
	{
		CPaintDC dc(this); // device context for painting

		SendMessage(WM_ICONERASEBKGND, (WPARAM) dc.GetSafeHdc(), 0);

		// Center icon in client rectangle
		int cxIcon = GetSystemMetrics(SM_CXICON);
		int cyIcon = GetSystemMetrics(SM_CYICON);
		CRect rect;
		GetClientRect(&rect);
		int x = (rect.Width() - cxIcon + 1) / 2;
		int y = (rect.Height() - cyIcon + 1) / 2;

		// Draw the icon
		dc.DrawIcon(x, y, m_hIcon);
	}
	else
	{
		CDialog::OnPaint();
	}
}

// The system calls this to obtain the cursor to display while the user drags
//  the minimized window.
HCURSOR CDlldemoDlg::OnQueryDragIcon()
{
	return (HCURSOR) m_hIcon;
}

void CDlldemoDlg::OnButtonTest() 
{
	// TODO: Add your control notification handler code here
#ifdef __cplusplus
	typedef WORD (CALLBACK *ROCKEY) (WORD, WORD*, DWORD*, DWORD*, WORD*, WORD*, WORD*, WORD*, BYTE*);
#else
	WORD Rockey(WORD function, WORD* handle, DWORD* lp1,  DWORD* lp2, WORD* p1, WORD* p2, WORD* p3, WORD* p4, BYTE* buffer);
#endif

	HINSTANCE hDll;
	ROCKEY Rockey;
	WORD handle[16], p1, p2, p3, p4, retcode;
	DWORD lp1, lp2;
	BYTE buffer[1024];
	WORD rc[4];
	int i, j;

	char cmd[] = "H=H^H, A=A*23, F=B*17, A=A+F, A=A+G, A=A<C, A=A^D, B=B^B, C=C^C, D=D^D";
	char cmd1[] = "A=A+B, A=A+C, A=A+D, A=A+E, A=A+F, A=A+G, A=A+H";
	char cmd2[] = "A=E|E, B=F|F, C=G|G, D=H|H";
	char errmsg[1000];

	// Load Library of Rockey4Smart
	hDll = GetModuleHandle("..\\Ry4S.dll");
	if (hDll == NULL)
	{
		hDll = LoadLibrary("..\\Ry4S.dll");
		if (hDll == NULL)
		{
			sprintf(errmsg,"Can't find Ry4S.dll");
			AfxMessageBox(errmsg);
			return;
		}
	}
	//Get interface of Ry4S.dll
	Rockey = (ROCKEY)GetProcAddress(hDll, "Rockey");

	//password
	p1 = 0xc44c;
	p2 = 0xc8f8;
	p3 = 0x0799;
	p4 = 0xc43b;

	lp1 = 0;
	//Find Rockey4Smart with specified password
	retcode = Rockey(RY_FIND, &handle[0], &lp1, &lp2, &p1, &p2, &p3, &p4, buffer);
	if (retcode)
	{
		ShowERR(retcode);
		return;
	}
	sprintf(errmsg,"Find Rock: %08X", lp1);
	mList.AddString(errmsg);

	p1 = 0xc44c;
	p2 = 0xc8f8;
	p3 = 0x0799;
	p4 = 0xc43b;
	//Open Rockey4Smart 
	retcode = Rockey(RY_OPEN, &handle[0], &lp1, &lp2, &p1, &p2, &p3, &p4, buffer);
	if (retcode)
	{
		ShowERR(retcode);
		return;
	}

	i = 1;
	while (retcode == 0)
	{
		//Find next Rockey4Smart
		retcode = Rockey(RY_FIND_NEXT, &handle[i], &lp1, &lp2, &p1, &p2, &p3, &p4, buffer);
		if (retcode == ERR_NOMORE) break;
		if (retcode)
		{
			ShowERR(retcode);
			return;
		}

		retcode = Rockey(RY_OPEN, &handle[i], &lp1, &lp2, &p1, &p2, &p3, &p4, buffer);
		if (retcode)
		{
			ShowERR(retcode);
			return;
		}

		i++;
		sprintf(errmsg,"Find Rock: %08X", lp1);
		mList.AddString(errmsg);
	}

	//Operate on each Rockey4Smart
	for (j=0;j<i;j++)
	{
		p1 = 498;
		p2 = 6;
	
		strcpy((char*)buffer, "Hello1");
		//Write data to user memory
		retcode = Rockey(RY_WRITE, &handle[j], &lp1, &lp2, &p1, &p2, &p3, &p4, buffer);
		if (retcode)
		{
			ShowERR(retcode);
			return;
		}
		sprintf(errmsg,"Write: Hello1");
		mList.AddString(errmsg);

		p1 = 498;
		p2 = 6;
		memset(buffer, 0, 64);
		//Read data from user memory
		retcode = Rockey(RY_READ, &handle[j], &lp1, &lp2, &p1, &p2, &p3, &p4, buffer);
		if (retcode)
		{
			ShowERR(retcode);
			return;
		}
		sprintf(errmsg,"Read: %s", buffer);
		mList.AddString(errmsg);

	    //Generate random number
		retcode = Rockey(RY_RANDOM, &handle[j], &lp1, &lp2, &p1, &p2, &p3, &p4, buffer);
		if (retcode)
		{
			ShowERR(retcode);
			return;
		}
		sprintf(errmsg,"Random: %04X,%04X,%04X,%04X", p1,p2,p3,p4);
		mList.AddString(errmsg);

		lp2 = 0x12345678;
		//Get return code of seed
		retcode = Rockey(RY_SEED, &handle[j], &lp1, &lp2, &p1, &p2, &p3, &p4, buffer);
		if (retcode)
		{
			ShowERR(retcode);
			return;
		}
		sprintf(errmsg,"Seed: %04X %04X %04X %04X", p1, p2, p3, p4);
		mList.AddString(errmsg);
		rc[0] = p1;
		rc[1] = p2;
		rc[2] = p3;
		rc[3] = p4;

		lp1 = 0x88888888;//USER ID
		//Write User ID
		retcode = Rockey(RY_WRITE_USERID, &handle[j], &lp1, &lp2, &p1, &p2, &p3, &p4, buffer);
		if (retcode)
		{
			ShowERR(retcode);
			return;
		}
		sprintf(errmsg,"Write User ID: %08X", lp1);
		mList.AddString(errmsg);

		lp1 = 0;
		//Read User ID
		retcode = Rockey(RY_READ_USERID, &handle[j], &lp1, &lp2, &p1, &p2, &p3, &p4, buffer);
		if (retcode)
		{
			ShowERR(retcode);
			return;
		}
		sprintf(errmsg,"Read User ID: %08X", lp1);
		mList.AddString(errmsg);

		p1 = 7;     //module number
		p2 = 0x2121;//module word
		p3 = 0;     //decrease not allow
		//Set module
		retcode = Rockey(RY_SET_MODULE, &handle[j], &lp1, &lp2, &p1, &p2, &p3, &p4, buffer);
		if (retcode)
		{
			ShowERR(retcode);
			return;
		}
		sprintf(errmsg,"Set Moudle 7: Module word = %04X Decrease no allowed", p2);
		mList.AddString(errmsg);

		p1 = 7;
		//Check module
		retcode = Rockey(RY_CHECK_MODULE, &handle[j], &lp1, &lp2, &p1, &p2, &p3, &p4, buffer);
		if (retcode)
		{
			ShowERR(retcode);
			return;
		}
		sprintf(errmsg,"Check Moudle 7: ");
		mList.AddString(errmsg);

		if (p2)	
			sprintf(errmsg,"valid,    ");
		else 
			sprintf(errmsg,"invalid,   ");
		mList.AddString(errmsg);
		if (p3) 
			sprintf(errmsg,"Allow Decrease");
		else 
			sprintf(errmsg,"Not Allow Decrease");
		mList.AddString(errmsg);

		p1 = 0;
		strcpy((char*)buffer, cmd);
		//Write user defined arithmetic to dongle
		retcode = Rockey(RY_WRITE_ARITHMETIC, &handle[j], &lp1, &lp2, &p1, &p2, &p3, &p4, buffer);
		if (retcode)
		{
			ShowERR(retcode);
			return;
		}
		sprintf(errmsg,"Write Arithmetic 1");
		mList.AddString(errmsg);

		lp1 = 0;
		lp2 = 7;
		p1 = 5;
		p2 = 3;
		p3 = 1;
		p4 = 0xffff;
		//Calculate,the result is decided by user defined arithmetic wrote to dongle
		retcode = Rockey(RY_CALCULATE1, &handle[j], &lp1, &lp2, &p1, &p2, &p3, &p4, buffer);
		if (retcode)
		{
			ShowERR(retcode);
			return;
		}
		sprintf(errmsg,"Calculate Input: p1=5, p2=3, p3=1, p4=0xffff");
		mList.AddString(errmsg);
		sprintf(errmsg,"Result = ((5*23 + 3*17 + 0x2121) < 1) ^ 0xffff = BC71");
		mList.AddString(errmsg);
		sprintf(errmsg,"Calculate Output: p1=%x, p2=%x, p3=%x, p4=%x", p1, p2, p3, p4);
		mList.AddString(errmsg);

		p1 = 10;
		strcpy((char*)buffer, cmd1);
		retcode = Rockey(RY_WRITE_ARITHMETIC, &handle[j], &lp1, &lp2, &p1, &p2, &p3, &p4, buffer);
		if (retcode)
		{
			ShowERR(retcode);
			return;
		}
		sprintf(errmsg,"Write Arithmetic 2");
		mList.AddString(errmsg);

		lp1 = 10;
		lp2 = 0x12345678;
		p1 = 1;
		p2 = 2;
		p3 = 3;
		p4 = 4;
		retcode = Rockey(RY_CALCULATE2, &handle[j], &lp1, &lp2, &p1, &p2, &p3, &p4, buffer);
		if (retcode)
		{
			ShowERR(retcode);
			return;
		}
		sprintf(errmsg,"Calculate Input: p1=1, p2=2, p3=3, p4=4");
		mList.AddString(errmsg);
		sprintf(errmsg,"Result = %04x + %04x + %04x + %04x + 1 + 2 + 3 + 4 = %04x", rc[0], rc[1], rc[2], rc[3], (WORD)(rc[0]+rc[1]+rc[2]+rc[3]+10));
		mList.AddString(errmsg);
		sprintf(errmsg,"Calculate Output: p1=%x, p2=%x, p3=%x, p4=%x", p1, p2, p3, p4);
		mList.AddString(errmsg);

		// Set Decrease
		p1 = 9;
		p2 = 0x5;
		p3 = 1;
		retcode = Rockey(RY_SET_MODULE, &handle[j], &lp1, &lp2, &p1, &p2, &p3, &p4, buffer);
		if (retcode)
		{
			ShowERR(retcode);
			return;
		}

		p1 = 17;
		strcpy((char*)buffer, cmd2);
		retcode = Rockey(RY_WRITE_ARITHMETIC, &handle[j], &lp1, &lp2, &p1, &p2, &p3, &p4, buffer);
		if (retcode)
		{
			ShowERR(retcode);
			return;
		}
		sprintf(errmsg,"Write Arithmetic 3");
		mList.AddString(errmsg);

		lp1 = 17;
		lp2 = 6;
		p1 = 1;
		p2 = 2;
		p3 = 3;
		p4 = 4;
		retcode = Rockey(RY_CALCULATE3, &handle[j], &lp1, &lp2, &p1, &p2, &p3, &p4, buffer);
		if (retcode)
		{
			ShowERR(retcode);
			return;
		}
		sprintf(errmsg,"Show Module from 6: p1=%x, p2=%x, p3=%x, p4=%x", p1, p2, p3, p4);
		mList.AddString(errmsg);

		// Decrease
		p1 = 9;
		retcode = Rockey(RY_DECREASE, &handle[j], &lp1, &lp2, &p1, &p2, &p3, &p4, buffer);
		if (retcode)
		{
			ShowERR(retcode);
			return;
		}
		sprintf(errmsg,"Decrease module 9");
		mList.AddString(errmsg);

		lp1 = 17;
		lp2 = 6;
		p1 = 1;
		p2 = 2;
		p3 = 3;
		p4 = 4;
		retcode = Rockey(RY_CALCULATE3, &handle[j], &lp1, &lp2, &p1, &p2, &p3, &p4, buffer);
		if (retcode)
		{
			ShowERR(retcode);
			return;
		}
		sprintf(errmsg,"Show Module from 6: p1=%x, p2=%x, p3=%x, p4=%x", p1, p2, p3, p4);
		mList.AddString(errmsg);


		//DES test
		mList.AddString("des test start!");
		const int nKeyLen[2] = {8,16};

		//set DES key buffer
		unsigned char keybuf[16];
		p1=DES_SINGLE_MODE;
		for(int k=0;k<nKeyLen[p1];k++)
		{
			keybuf[k]=k; //des key
		}

		//set DES key
		retcode = Rockey(RY_SET_DES_KEY, &handle[j], &lp1, &lp2, &p1, &p2, &p3, &p4, keybuf);
		if (retcode)
		{
			ShowERR(retcode);
			return;
		}

		//set rude data to be encrypt
		p2 = 24;
		//p2 must be multiple of 8
//		int k;
		for(k=0;k<p2;k++)
		{
			buffer[k]='A';
		}		
		mList.AddString("des plane data:");
		CString str;
		HexBufferToString(str,buffer,p2);
		mList.AddString(str);
		//DES encrypt
		retcode = Rockey(RY_DES_ENC, &handle[j], &lp1, &lp2, &p1, &p2, &p3, &p4, buffer);

		if (retcode)
		{
			ShowERR(retcode);
			return;
		}
		mList.AddString("des cipher data:");
		HexBufferToString(str,buffer,p2);
		mList.AddString(str);
		//DES decrypt
		retcode = Rockey(RY_DES_DEC, &handle[j], &lp1, &lp2, &p1, &p2, &p3, &p4,buffer);
		if (retcode)
		{
			ShowERR(retcode);
			return;
		}
		mList.AddString("des decrypt data:");
		HexBufferToString(str,buffer,p2);
		mList.AddString(str);

		mList.AddString("des test over!");


		//RSA test
		mList.AddString("rsa test start!");
		
		//set key N
		SetN(buffer);
		retcode = Rockey(RY_SET_RSAKEY_N, &handle[j], &lp1, &lp2, &p1, &p2, &p3, &p4,buffer);
		if (retcode)
		{
			ShowERR(retcode);
			return;
		}

		//set key D
		SetD(buffer);
		retcode = Rockey(RY_SET_RSAKEY_D, &handle[j], &lp1, &lp2, &p1, &p2, &p3, &p4,buffer);
		if (retcode)
		{
			ShowERR(retcode);
			return;
		}
		//set rude data to be encrypt
		memset(buffer,0,8);
		for(k=0;k<24;k++)
		{
			buffer[k] = 'A';
		}
		//buffer[k]=0x00;
		mList.AddString("rsa plane data:");
		HexBufferToString(str,buffer,24);
		mList.AddString(str);

		//set pad type
		p2 = 24;
		p3 = RSA_ROCKEY_PADDING;

		//rsa public key encrypt
		p1 = RSA_PUBLIC_KEY;
		retcode = Rockey(RY_RSA_ENC, &handle[j], &lp1, &lp2, &p1, &p2, &p3, &p4,buffer);
		if (retcode)
		{
			ShowERR(retcode);
			return;
		}
		
		mList.AddString("rsa cipher data:");
		HexBufferToString(str,buffer,24);
		mList.AddString(str);
		//rsa private key decrypt
		p1 = RSA_PRIVATE_KEY;
		retcode = Rockey(RY_RSA_DEC, &handle[j], &lp1, &lp2, &p1, &p2, &p3, &p4,buffer);
		if (retcode)
		{
			ShowERR(retcode);
			return;
		}
		mList.AddString("rsa decrypt data:");
		HexBufferToString(str,buffer,24);
		mList.AddString(str);

		mList.AddString("rsa test over!");		
		retcode = Rockey(RY_CLOSE, &handle[j], &lp1, &lp2, &p1, &p2, &p3, &p4, buffer);
		if (retcode)
		{
			ShowERR(retcode);
			return;
		}
		sprintf(errmsg,"Close Rockey4Smart");
		mList.AddString(errmsg);
	}

	FreeLibrary(hDll);	
}

void CDlldemoDlg::ShowERR(WORD retcode)
{
	char errmsg[100];

	if (retcode == 0) return;
	sprintf(errmsg,"Error Code: %d", retcode);
	AfxMessageBox(errmsg);
}
