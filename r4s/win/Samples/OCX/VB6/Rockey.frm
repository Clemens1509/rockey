VERSION 5.00
Object = "{5E898AE2-F2F8-46C1-A19A-4286C7B154EB}#1.0#0"; "Ry4S.ocx"
Begin VB.Form Form1 
   Caption         =   "Form1"
   ClientHeight    =   6090
   ClientLeft      =   5445
   ClientTop       =   4335
   ClientWidth     =   6285
   LinkTopic       =   "Form1"
   ScaleHeight     =   6090
   ScaleWidth      =   6285
   Begin RY4SLib.Ry4S RockeyCtrl1 
      Height          =   495
      Left            =   2520
      TabIndex        =   18
      Top             =   4320
      Visible         =   0   'False
      Width           =   1815
      _Version        =   65536
      _ExtentX        =   3201
      _ExtentY        =   873
      _StockProps     =   0
   End
   Begin VB.CommandButton Close 
      Caption         =   "Close"
      Height          =   375
      Left            =   4680
      TabIndex        =   16
      Top             =   4320
      Width           =   1215
   End
   Begin VB.CommandButton Cal2 
      Caption         =   "Calculate2"
      Height          =   375
      Left            =   4680
      TabIndex        =   15
      Top             =   3285
      Width           =   1215
   End
   Begin VB.CommandButton Cal1 
      Caption         =   "Calculate 1"
      Height          =   375
      Left            =   4680
      TabIndex        =   14
      Top             =   2715
      Width           =   1215
   End
   Begin VB.CommandButton Cal3 
      Caption         =   "Calculate 3"
      Height          =   375
      Left            =   4680
      TabIndex        =   13
      Top             =   3840
      Width           =   1215
   End
   Begin VB.CommandButton CheckModule 
      Caption         =   "Check Module"
      Height          =   375
      Left            =   4680
      TabIndex        =   12
      Top             =   2160
      Width           =   1215
   End
   Begin VB.CommandButton SetModule 
      Caption         =   "SetModule"
      Height          =   375
      Left            =   4680
      TabIndex        =   11
      Top             =   1605
      Width           =   1215
   End
   Begin VB.CommandButton Seed 
      Caption         =   "Seed"
      Height          =   375
      Left            =   4680
      TabIndex        =   10
      Top             =   5040
      Width           =   1215
   End
   Begin VB.CommandButton UserId 
      Caption         =   "Write UserId"
      Height          =   375
      Left            =   4680
      TabIndex        =   9
      Top             =   5520
      Width           =   1215
   End
   Begin VB.CommandButton ReadUserId 
      Caption         =   "ReadUserId"
      Height          =   375
      Left            =   4680
      TabIndex        =   8
      Top             =   1035
      Width           =   1215
   End
   Begin VB.CommandButton Random 
      Caption         =   "Random"
      Height          =   375
      Left            =   3120
      TabIndex        =   7
      Top             =   5520
      Width           =   1215
   End
   Begin VB.CommandButton Demo 
      Caption         =   "Demo"
      Height          =   375
      Left            =   4680
      TabIndex        =   6
      Top             =   480
      Width           =   1215
   End
   Begin VB.CommandButton Read 
      Caption         =   "Read"
      Height          =   375
      Left            =   3120
      TabIndex        =   5
      Top             =   5040
      Width           =   1215
   End
   Begin VB.CommandButton Write 
      Caption         =   "Write"
      Height          =   375
      Left            =   1680
      TabIndex        =   4
      Top             =   5520
      Width           =   1215
   End
   Begin VB.CommandButton FindNext 
      Caption         =   "FindNext"
      Height          =   375
      Left            =   1680
      TabIndex        =   3
      Top             =   5040
      Width           =   1215
   End
   Begin VB.ListBox List1 
      Height          =   4350
      Left            =   360
      TabIndex        =   2
      Top             =   480
      Width           =   3975
   End
   Begin VB.CommandButton Find 
      Caption         =   "Find"
      Height          =   375
      Left            =   120
      TabIndex        =   1
      Top             =   5040
      Width           =   1215
   End
   Begin VB.CommandButton Open 
      Caption         =   "Open"
      Height          =   375
      Left            =   120
      TabIndex        =   0
      Top             =   5520
      Width           =   1215
   End
   Begin VB.Label Label1 
      AutoSize        =   -1  'True
      Caption         =   "Please Click Demo Button First"
      Height          =   195
      Left            =   2040
      TabIndex        =   17
      Top             =   120
      Width           =   2175
   End
End
Attribute VB_Name = "Form1"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = True
Attribute VB_Exposed = False
Const RY_FIND = 1                      'Find dongle
Const RY_FIND_NEXT = 2                 'Find next dongle
Const RY_OPEN = 3                      'Open dongle
Const RY_CLOSE = 4                     'Close dongle
Const RY_READ = 5                      'Read dongle
Const RY_WRITE = 6                     'Write dongle
Const RY_RANDOM = 7                    'Generate random number
Const RY_SEED = 8                      'Generate return code of seed
Const RY_WRITE_USERID = 9              'Write user ID
Const RY_READ_USERID = 10              'Read user ID
Const RY_SET_MOUDLE = 11               'Set module
Const RY_CHECK_MOUDLE = 12             'Check module
Const RY_SET_MODULE = 11               'Set module
Const RY_CHECK_MODULE = 12             'Check module
Const RY_WRITE_ARITHMETIC = 13         'Write arithmetic
Const RY_CALCULATE1 = 14               'Calculate 1
Const RY_CALCULATE2 = 15               'Calculate 2
Const RY_CALCULATE3 = 16               'Calculate 3
Const RY_DECREASE = 17                 'Decrease module unit

Const cmd0 = "H=H^H, A=A*23, F=B*17, A=A+F, A=A+G, A=A<C, A=A^D, B=B^B, C=C^C, D=D^D"
Const cmd1 = "A=A+B, A=A+C, A=A+D, A=A+E, A=A+F, A=A+G, A=A+H"
Const cmd2 = "A=E|E, B=F|F, C=G|G, D=H|H"

Public err As Integer

Public sd1 As Integer
Public sd2 As Integer
Public sd3 As Integer
Public sd4 As Integer

Private Sub Demo_Click()
    Find_Click
    Open_Click
    Write_Click
    Read_Click
    Random_Click
    Seed_Click
    UserId_Click
    ReadUserId_Click
    SetModule_Click
    CheckModule_Click
    Cal1_Click
    Cal2_Click
    Cal3_Click
    Close
End Sub

Private Sub Form_Load()
    RockeyCtrl1.P1 = &HC44C
    RockeyCtrl1.P2 = &HC8F8
    RockeyCtrl1.P3 = &H799
    RockeyCtrl1.P4 = &HC43B
End Sub

Private Sub Find_Click()
    RockeyCtrl1.P1 = &HC44C
    RockeyCtrl1.P2 = &HC8F8
    RockeyCtrl1.P3 = &H799
    RockeyCtrl1.P4 = &HC43B
    err = RockeyCtrl1.Rockey(RY_FIND)
    List1.AddItem "Find"
    List1.AddItem RockeyCtrl1.ErrToStr(err)
    List1.AddItem "Find Rockey : " + Str(RockeyCtrl1.Lp1)
End Sub

Private Sub Open_Click()
    RockeyCtrl1.P1 = &HC44C
    RockeyCtrl1.P2 = &HC8F8
    RockeyCtrl1.P3 = &H799
    RockeyCtrl1.P4 = &HC43B
    err = RockeyCtrl1.Rockey(RY_OPEN)
    List1.AddItem "Open"
    List1.AddItem RockeyCtrl1.ErrToStr(err)
End Sub

Private Sub FindNext_Click()
    err = RockeyCtrl1.Rockey(RY_FIND_NEXT)
    List1.AddItem "Find Next"
    List1.AddItem RockeyCtrl1.ErrToStr(err)
End Sub



Private Sub Write_Click()
    RockeyCtrl1.P1 = 3
    RockeyCtrl1.P2 = 5
    RockeyCtrl1.Buffer = "Hello"
    err = RockeyCtrl1.Rockey(RY_WRITE)
    List1.AddItem "Write"
    List1.AddItem RockeyCtrl1.ErrToStr(err)
End Sub

Private Sub Read_Click()
    RockeyCtrl1.P1 = 3
    RockeyCtrl1.P2 = 5
    err = RockeyCtrl1.Rockey(RY_READ)
    List1.AddItem "Read"
    List1.AddItem RockeyCtrl1.ErrToStr(err)
    List1.AddItem RockeyCtrl1.Buffer
End Sub

Private Sub Random_Click()
    err = RockeyCtrl1.Rockey(RY_RANDOM)
    List1.AddItem "Random"
    List1.AddItem RockeyCtrl1.ErrToStr(err)
    List1.AddItem RockeyCtrl1.P1
End Sub

Private Sub Seed_Click()
    RockeyCtrl1.Lp2 = &H12334578
    err = RockeyCtrl1.Rockey(RY_SEED)
    List1.AddItem "Seed"
    List1.AddItem RockeyCtrl1.ErrToStr(err)
    List1.AddItem RockeyCtrl1.P1
    List1.AddItem RockeyCtrl1.P2
    List1.AddItem RockeyCtrl1.P3
    List1.AddItem RockeyCtrl1.P4
    sd1 = RockeyCtrl1.P1
    sd2 = RockeyCtrl1.P2
    sd3 = RockeyCtrl1.P3
    sd4 = RockeyCtrl1.P4
End Sub
Private Sub UserId_Click()
    RockeyCtrl1.Lp1 = 8888888
    err = RockeyCtrl1.Rockey(RY_WRITE_USERID)
    List1.AddItem "Write UserId"
    List1.AddItem RockeyCtrl1.ErrToStr(err)
    List1.AddItem RockeyCtrl1.Lp1
End Sub

Private Sub ReadUserId_Click()
    err = RockeyCtrl1.Rockey(RY_READ_USERID)
    List1.AddItem "Read UserId"
    List1.AddItem RockeyCtrl1.ErrToStr(err)
    List1.AddItem RockeyCtrl1.Lp1
End Sub

Private Sub SetModule_Click()
    RockeyCtrl1.P1 = 7
    RockeyCtrl1.P2 = &H2121
    RockeyCtrl1.P3 = 0
    err = RockeyCtrl1.Rockey(RY_SET_MODULE)
    List1.AddItem "Set Module"
    List1.AddItem RockeyCtrl1.ErrToStr(err)
    List1.AddItem RockeyCtrl1.P3
End Sub
Private Sub CheckModule_Click()
    RockeyCtrl1.P1 = 7
    err = RockeyCtrl1.Rockey(RY_CHECK_MODULE)
    List1.AddItem "Check Module"
    List1.AddItem RockeyCtrl1.ErrToStr(err)
    List1.AddItem RockeyCtrl1.P2
    List1.AddItem RockeyCtrl1.P3
End Sub
Public Sub WriteArithmetic(cmd As String, idx As Integer)
    RockeyCtrl1.Buffer = cmd
    err = RockeyCtrl1.Rockey(RY_WRITE_ARITHMETIC)
    List1.AddItem "Write Arithmetic"
    List1.AddItem RockeyCtrl1.ErrToStr(err)
    List1.AddItem "Write Arithmetic " + Str(idx)
End Sub
Public Sub ExecCalculate(idx As Integer)
    List1.AddItem "Calculate Input: p1=" + Str(RockeyCtrl1.P1) + " p2=" + Str(RockeyCtrl1.P2) + " p3=" + Str(RockeyCtrl1.P3) + " p4=" + Str(RockeyCtrl1.P4)
    err = RockeyCtrl1.Rockey(idx)
    List1.AddItem "Exec Calculate"
    List1.AddItem RockeyCtrl1.ErrToStr(err)
    List1.AddItem "Exec Calculate " + Str(idx)
End Sub

Private Sub Cal1_Click()
    RockeyCtrl1.P1 = 0
    WriteArithmetic cmd0, 1
    
    RockeyCtrl1.Lp1 = 0
    RockeyCtrl1.Lp2 = 7
    RockeyCtrl1.P1 = 5
    RockeyCtrl1.P2 = 3
    RockeyCtrl1.P3 = 1
    RockeyCtrl1.P4 = &HFFFF
    ExecCalculate RY_CALCULATE1
    List1.AddItem "Result = 5*23 + 3*17 + 0x2121 < 1 ^ 0xffff = BC71"
    List1.AddItem "Calculate Output: p1=" + Str(RockeyCtrl1.P1) + " p2=" + Str(RockeyCtrl1.P2) + " p3=" + Str(RockeyCtrl1.P3) + " p4=" + Str(RockeyCtrl1.P4)
End Sub

Private Sub Cal2_Click()
    RockeyCtrl1.P1 = 10
    WriteArithmetic cmd1, 2
    
    RockeyCtrl1.Lp1 = 10
    RockeyCtrl1.Lp2 = &H12345678
    RockeyCtrl1.P1 = 1
    RockeyCtrl1.P2 = 2
    RockeyCtrl1.P3 = 3
    RockeyCtrl1.P4 = 4
    ExecCalculate RY_CALCULATE2
    List1.AddItem "Result =sd1 + sd2 + sd3 + sd4 + 1 + 2 + 3 + 4 = " + Str(1 + 2 + 3 + 4 + 10)
    List1.AddItem "Calculate Output: p1=" + Str(RockeyCtrl1.P1) + " p2=" + Str(RockeyCtrl1.P2) + " p3=" + Str(RockeyCtrl1.P3) + " p4=" + Str(RockeyCtrl1.P4)
End Sub

Private Sub Cal3_Click()
    RockeyCtrl1.P1 = 9
    RockeyCtrl1.P2 = 5
    RockeyCtrl1.P3 = 1
    err = RockeyCtrl1.Rockey(RY_SET_MODULE)
    List1.AddItem "Set Module 9"
    List1.AddItem RockeyCtrl1.ErrToStr(err)
    
    RockeyCtrl1.P1 = 17
    WriteArithmetic cmd2, 3
    RockeyCtrl1.Lp1 = 17
    RockeyCtrl1.Lp2 = 6
    RockeyCtrl1.P1 = 1
    RockeyCtrl1.P2 = 2
    RockeyCtrl1.P3 = 3
    RockeyCtrl1.P4 = 4
    ExecCalculate (RY_CALCULATE3)
    List1.AddItem "Calculate Output: p1=" + Str(RockeyCtrl1.P1) + " p2=" + Str(RockeyCtrl1.P2) + " p3=" + Str(RockeyCtrl1.P3) + " p4=" + Str(RockeyCtrl1.P4)
    
    RockeyCtrl1.P1 = 9
    err = RockeyCtrl1.Rockey(RY_DECREASE)
    List1.AddItem "Decrease Module 9"
    List1.AddItem RockeyCtrl1.ErrToStr(err)
    RockeyCtrl1.Lp1 = 17
    RockeyCtrl1.Lp2 = 6
    RockeyCtrl1.P1 = 1
    RockeyCtrl1.P2 = 2
    RockeyCtrl1.P3 = 3
    RockeyCtrl1.P4 = 4
    ExecCalculate (RY_CALCULATE3)
    List1.AddItem "Calculate Output: p1=" + Str(RockeyCtrl1.P1) + " p2=" + Str(RockeyCtrl1.P2) + " p3=" + Str(RockeyCtrl1.P3) + " p4=" + Str(RockeyCtrl1.P4)
End Sub

Private Sub Close_Click()
    err = RockeyCtrl1.Rockey(RY_CLOSE)
    List1.AddItem "Close"
    List1.AddItem RockeyCtrl1.ErrToStr(err)
End Sub
