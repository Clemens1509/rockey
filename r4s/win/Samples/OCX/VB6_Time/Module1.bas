Attribute VB_Name = "Module1"
' System time structure
Public Type SYSTEMTIME
    wYear As Integer
    wMonth As Integer
    wDayOfWeek As Integer
    wDay As Integer
    wHour As Integer
    wMinute As Integer
    wSecond As Integer
    wMilliseconds As Integer
End Type

Declare Sub GetSystemTime Lib "Kernel32.dll" (lpSystemTime As SYSTEMTIME)
Declare Sub GetLocalTime Lib "kernel32" (lpSystemTime As SYSTEMTIME)
