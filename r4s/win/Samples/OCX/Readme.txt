------------------ ROCKEY4SMART Software Developer's Kit --------------------

This folder comprises the OCX samples for VB6

Notes:
    Before use the sample, please register the OCX components in DOS 
	commandline console.
    Register:
        regsvr32 Ry4S.ocx 
    Unregister:
        regsvr32 /u Ry4S.ocx

------------------ Folder Content List --------------------------------------

VB6             Common function sample
VB6_Count       Sample to show how to use timer unit
VB6_Time        Sample to show how to use counter unit


