VERSION 5.00
Object = "{5E898AE2-F2F8-46C1-A19A-4286C7B154EB}#1.0#0"; "Ry4S.ocx"
Begin VB.Form Form1 
   Caption         =   "Form1"
   ClientHeight    =   6045
   ClientLeft      =   60
   ClientTop       =   450
   ClientWidth     =   6525
   LinkTopic       =   "Form1"
   ScaleHeight     =   6045
   ScaleWidth      =   6525
   StartUpPosition =   3  'Windows Default
   Begin RY4SLib.Ry4S Ry4S1 
      Height          =   495
      Left            =   480
      TabIndex        =   2
      Top             =   5160
      Visible         =   0   'False
      Width           =   1815
      _Version        =   65536
      _ExtentX        =   3201
      _ExtentY        =   873
      _StockProps     =   0
   End
   Begin VB.CommandButton Demo 
      Caption         =   "Demo"
      Height          =   495
      Left            =   4680
      TabIndex        =   1
      Top             =   5160
      Width           =   1335
   End
   Begin VB.ListBox List1 
      Height          =   4545
      Left            =   480
      TabIndex        =   0
      Top             =   480
      Width           =   5535
   End
End
Attribute VB_Name = "Form1"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = True
Attribute VB_Exposed = False
Const RY_FIND = 1                      'Find dongle
Const RY_FIND_NEXT = 2                 'Find next dongle
Const RY_OPEN = 3                      'Open dongle
Const RY_CLOSE = 4                     'Close dongle
Const RY_READ = 5                      'Read dongle
Const RY_WRITE = 6                     'Write dongle
Const RY_RANDOM = 7                    'Generate random number
Const RY_SEED = 8                      'Generate return code of seed code
Const RY_WRITE_USERID = 9              'Write User ID
Const RY_READ_USERID = 10              'Read User ID
Const RY_SET_MOUDLE = 11               'Set module
Const RY_CHECK_MOUDLE = 12             'Check module
Const RY_SET_MODULE = 11               'Set module
Const RY_CHECK_MODULE = 12             'Check module
Const RY_WRITE_ARITHMETIC = 13         'Write arithmetic
Const RY_CALCULATE1 = 14               'Calculate 1
Const RY_CALCULATE2 = 15               'Calculate 2
Const RY_CALCULATE3 = 16               'Calculate 3
Const RY_DECREASE = 17                 'Decrease module unit
Const RY_SET_COUNTER = 20              'Set counter
Const RY_GET_COUNTER = 21              'Get counter
Const RY_DEC_COUNTER = 22              'Decrease counter
Const RY_SET_TIMER = 23                'Set timer
Const RY_GET_TIMER = 24                'Get timer
Const RY_ADJUST_TIMER = 25             'Adjusst timer
Const RY_SET_TIMER_ITV = 26
Const RY_GET_TIMER_ITV = 27
Const RY_DEC_TIMER = 28
Const RY_SET_PASSWORD = 50             'Generate new password

Const RY_SET_COUNTER_EX = &HA0            'set counter,value type changed from WORD to DWORD
Const RY_GET_COUNTER_EX = &HA1            'get counter,value type changed from WORD to DWORD

Const RY_SET_TIMER_EX = &HA2               'set timer,time value type changed from WORD to DWORD
Const RY_GET_TIMER_EX = &HA3               'get timer,time value type changed from WORD to DWORD
Const RY_ADJUST_TIMER_EX = &HA4            'adjust timer, time value type changed from WORD to DWORD

Const RY_UPDATE_GEN_HEADER_EX = &HA5    'generate update file header,specialize in updating RSA key pair
Const RY_UPDATE_GEN_EX = &HA6           'generate update file content,specialize in updating RSA key pair
Const RY_UPDATE_CHECK_EX = &HA7         'update file checking,specialize in updating RSA key pair
Const RY_RY_UPDATE_EX = &HA8            'update cipher file,specialize in updating RSA key pair
Const RY_SET_UPDATE_KEY = &HA9          'set update RSA key pair



Const ERR_SUCCESS = 0                    '//success
Const ERR_NO_ROCKEY = 3                  '//No ROCKEY4 smart dongle
Const ERR_INVALID_PASSWORD = 4           '//Found ROCKEY4 smart dongle,but base password is wrong
Const ERR_INVALID_PASSWORD_OR_ID = 5     ' //Wrong password or Rockey4 smart HID
Const ERR_SETID = 6                      '//Set ROCKEY4 smart dongle HID wrong
Const ERR_INVALID_ADDR_OR_SIZE = 7       '//Read/write address or length wrong
Const ERR_UNKNOWN_COMMAND = 8            '//No such command
Const ERR_NOTBELEVEL3 = 9                '//Inside error
Const ERR_READ = 10                      ' //Read error
Const ERR_WRITE = 11                     ' //Wriet error
Const ERR_RANDOM = 12                    ' //Random error
Const ERR_SEED = 13                      ' //Seed code error
Const ERR_CALCULATE = 14                 ' //Calculate error
Const ERR_NO_OPEN = 15                   ' //No open dongle before operating it
Const ERR_OPEN_OVERFLOW = 16             ' //open too much dongles (more than 16)
Const ERR_NOMORE = 17                    ' //No more dongles
Const ERR_NEED_FIND = 18                 ' //use FindNext before using Find
Const ERR_DECREASE = 19                  ' //Decrease error
Const ERR_AR_BADCOMMAND = 20             ' //Arithmetic instruction error
Const ERR_AR_UNKNOWN_OPCODE = 21         ' //Arithmetic operator error
Const ERR_AR_WRONGBEGIN = 22             ' //Const number can't use on first arithmetic instruction
Const ERR_AR_WRONG_END = 23              ' //Const number can't use on last arithmetic instruction
Const ERR_AR_VALUEOVERFLOW = 24          ' //Const number> 63 in arithmetic
Const ERR_TOOMUCHTHREAD = 25             ' //Too many ( > 100) threads open the dongle in the single process

Const ERR_INVALID_RY4S = 30              '  //Try operation on a non-Rockey4Smart dongle

Const RY_VERSION = 100


Const cmd0 = "H=H^H, A=A*23, F=B*17, A=A+F, A=A+G, A=A<C, A=A^D, B=B^B, C=C^C, D=D^D"
Const cmd1 = "A=A+B, A=A+C, A=A+D, A=A+E, A=A+F, A=A+G, A=A+H"
Const cmd2 = "A=E|E, B=F|F, C=G|G, D=H|H"

Public retcode As Integer

Public sd1 As Integer
Public sd2 As Integer
Public sd3 As Integer
Public sd4 As Integer

Private Sub Demo_Click()

Dim curline As Integer

 Ry4S1.P1 = &HC44C
 Ry4S1.P2 = &HC8F8
 Ry4S1.P3 = &H799
 Ry4S1.P4 = &HC43B

curline = 0

'Find dongle
retcode = Ry4S1.Rockey(RY_FIND)
If (retcode <> ERR_SUCCESS) Then
    List1.List(curline) = "Failed to find dongle,error code:" & retcode
    Exit Sub
End If
List1.List(curline) = "Dongle HID:" & Ry4S1.Lp1

curline = curline + 1
'Open dongle
retcode = Ry4S1.Rockey(RY_OPEN)
If (retcode <> ERR_SUCCESS) Then
    List1.List(curline) = "Failed to open dongle,error code:" & retcode
    Exit Sub
End If
List1.List(curline) = "Open dongle successfully!"
curline = curline + 1

'Get version information
retcode = Ry4S1.Rockey(RY_VERSION)
If (retcode <> ERR_SUCCESS) Then
    List1.List(curline) = "Failed to get version,error code:" & retcode
    Exit Sub
End If

If (Ry4S1.Lp2 < 259) Then
    List1.List(curline) = "Old dongle doesn't support new TIME and COUNT interfaces,please use v1.03 dongle for test,error code:" & retcode
    Exit Sub
End If


'Adjust time
Ry4S1.P1 = 0  'Timer unit No
Ry4S1.Buffer = 10 'Timer value��WORD��
Ry4S1.P3 = 1  'flag��1 means decrease allowed

'Set counter
retcode = Ry4S1.Rockey(RY_SET_COUNTER_EX)
If (retcode <> ERR_SUCCESS) Then
    List1.List(curline) = "Failed to set counter,error code:" & retcode
    Exit Sub
End If
List1.List(curline) = "Set counter successfully!"
curline = curline + 1
List1.List(curline) = "Set,Unit��0"
curline = curline + 1
List1.List(curline) = "Set,Value��10"

'Get counter
curline = curline + 1

Ry4S1.P3 = 1  'flag��1 means decrease allowed

Ry4S1.Buffer = 0
retcode = Ry4S1.Rockey(RY_GET_COUNTER_EX)
If (retcode <> ERR_SUCCESS) Then
   List1.List(curline) = "Failed to get counter,error code:" & retcode
End If
List1.List(curline) = "Get counter successfully!"
curline = curline + 1
List1.List(curline) = "Get,Unit��0"
curline = curline + 1
List1.List(curline) = "Get,Value��" & Ry4S1.Buffer




End Sub

