
#include <vcl.h>
#pragma hdrstop
#include "Unit1.h"

//---------------------------------------------------------------------------
#include "Ry4S.h"
#pragma comment(lib,"Ry4S_BCB.LIB")
//---------------------------------------------------------------------------

#pragma package(smart_init)
#pragma resource "*.dfm"
TForm1 *Form1;

__fastcall TForm1::TForm1(TComponent* Owner)
        : TForm(Owner)
{
}

//###########################################################################


void _fastcall TForm1::ShowError(WORD wRetCode)
{
    if (wRetCode == 0) return;
    Memo1->Lines->Add("Error Code: " + AnsiString(wRetCode));
}
//---------------------------------------------------------------------------

void __fastcall TForm1::Button1Click(TObject *Sender)
{
    WORD handle[10];
    WORD p1, p2, p3, p4, wRetCode;
    DWORD lp1, lp2;
    BYTE buffer[1024];
    WORD rc[4];
    int i, j;
    char tbuf[1024];
    AnsiString sMsg;

    char cmd[] = "H=H^H, A=A*23, F=B*17, A=A+F, A=A+G, A=A<C, A=A^D, B=B^B, C=C^C, D=D^D";
    char cmd1[] = "A=A+B, A=A+C, A=A+D, A=A+E, A=A+F, A=A+G, A=A+H";
    char cmd2[] = "A=E|E, B=F|F, C=G|G, D=H|H";

    p1 = 0xc44c;
    p2 = 0xc8f8;
    p3 = 0x0799;
    p4 = 0xc43b;

    Memo1->Lines->Add("Finding dongle...");
    //Find dongle with specified password
    wRetCode = Rockey(RY_FIND,&handle[0],&lp1, &lp2, &p1, &p2, &p3, &p4, buffer);
    if (wRetCode)
    {
        ShowError(wRetCode);
        return;
    }

    wsprintf(tbuf, "Rockey4Smart 1 ID: 0x%08X", lp1);
    Memo1->Lines->Add(tbuf);

    wRetCode = Rockey(RY_OPEN, &handle[0], &lp1, &lp2, &p1, &p2, &p3, &p4, buffer);
    if (wRetCode)
    {
        ShowError(wRetCode);
        return;
    }

    i = 1;

    //find next dongle with the same password
    while (wRetCode == 0)
    {
        wRetCode = Rockey(RY_FIND_NEXT, &handle[i], &lp1, &lp2, &p1, &p2, &p3, &p4, buffer);
        if (wRetCode == ERR_NOMORE) break;
        if (wRetCode)
        {
            ShowError(wRetCode);
            return;
        }
        wRetCode = Rockey(RY_OPEN, &handle[i], &lp1, &lp2, &p1, &p2, &p3, &p4, buffer);
        if (wRetCode)
        {
            ShowError(wRetCode);
            return;
        }
        i++;

        wsprintf(tbuf, "Rockey4Smart %d ID: 0x%08X",i, lp1);
        Memo1->Lines->Add(tbuf);
    }
    Memo1->Lines->Add("------------------------------------------------------------------");
    Memo1->Lines->Add("");

    for (j=0;j<i;j++)
    {
        sMsg="Rockey4Smart "+IntToStr(j+1)+" :";
        Memo1->Lines->Add(sMsg);
        p1 = 0;
        p2 = 500;
        p1 = 0;
        p2 = 400;

        Memo1->Lines->Add("Reading user data memory...");
        memset(buffer, '\0', 512);
        wRetCode = Rockey(RY_READ,&handle[j], &lp1, &lp2, &p1, &p2, &p3, &p4, buffer);
        if (wRetCode)
        {
            ShowError(wRetCode);
            return;
        }

        wsprintf(tbuf, "Readed: %s", buffer);
        Memo1->Lines->Add(tbuf);
        Memo1->Lines->Add("");


        Memo1->Lines->Add("Getting random number from dongle...");
        wRetCode = Rockey(RY_RANDOM,&handle[j], &lp1, &lp2, &p1, &p2, &p3, &p4, buffer);
        if (wRetCode)
        {
            ShowError(wRetCode);
            return;
        }
        
        wsprintf(tbuf, "Random: %04X", p1);
        Memo1->Lines->Add(tbuf);
        Memo1->Lines->Add("");

        Memo1->Lines->Add("Getting return code of seed");
        lp2 = 0x12345678;
        wRetCode = Rockey(RY_SEED, &handle[j], &lp1, &lp2, &p1, &p2, &p3, &p4, buffer);
        if (wRetCode)
        {
            ShowError(wRetCode);
            return;
        }

        wsprintf(tbuf, "Return code: %04X %04X %04X %04X", p1, p2, p3, p4);
        Memo1->Lines->Add(tbuf);
        Memo1->Lines->Add("");

        rc[0] = p1;
        rc[1] = p2;
        rc[2] = p3;
        rc[3] = p4;

        lp1 = 0x88888888;
        sMsg="Writing User ID:0x88888888...";
        Memo1->Lines->Add(sMsg);
        wRetCode = Rockey(RY_WRITE_USERID, &handle[j], &lp1, &lp2, &p1, &p2, &p3, &p4, buffer);
        if (wRetCode)
        {
            ShowError(wRetCode);
            return;
        }
        Memo1->Lines->Add("");

       // wsprintf(tbuf, "Write User ID: %08X", lp1);
        //Memo1->Lines->Add(tbuf);

        Memo1->Lines->Add("Reading User ID...");
        lp1 = 0;
        wRetCode = Rockey(RY_READ_USERID, &handle[j], &lp1, &lp2, &p1, &p2, &p3, &p4, buffer);
        if (wRetCode)
        {
            ShowError(wRetCode);
            return;
        }

        wsprintf(tbuf, "Readed User ID: 0x%08X", lp1);
        Memo1->Lines->Add(tbuf);
        Memo1->Lines->Add("");

        Memo1->Lines->Add("Setting module 7: Pass=0x2121, Decrease=not allow...");
        p1 = 7;      //module number
        p2 = 0x2121; //module word
        p3 = 0;      //not allow decrease
        wRetCode = Rockey(RY_SET_MODULE, &handle[j], &lp1, &lp2, &p1, &p2, &p3, &p4, buffer);
        if (wRetCode)
        {
            ShowError(wRetCode);
            return;
        }

        Memo1->Lines->Add("Set module successfully");
        Memo1->Lines->Add("");

        Memo1->Lines->Add("Checking module 7...");
        p1 = 7;
        //check module
        wRetCode = Rockey(RY_CHECK_MODULE, &handle[j], &lp1, &lp2, &p1, &p2, &p3, &p4, buffer);
        if (wRetCode)
        {
            ShowError(wRetCode);
            return;
        }

        strcpy(tbuf, "Check Moudle 7: ");
        if (p2)
            strcat(tbuf, "Valid   ,");
        else
            strcat(tbuf, "Invalid   ,");
        if (p3)
            strcat(tbuf, "Allow Decrease");
        else
            strcat(tbuf, "Not Allow Decrease");
        Memo1->Lines->Add(tbuf);
        Memo1->Lines->Add("");

        Memo1->Lines->Add("Writing Arithmetic 1...");
        p1 = 0;
        strcpy((char*)buffer, cmd);
        wRetCode = Rockey(RY_WRITE_ARITHMETIC, &handle[j], &lp1, &lp2, &p1, &p2, &p3, &p4, buffer);
        if (wRetCode)
        {
            ShowError(wRetCode);
            return;
        }

        Memo1->Lines->Add("Write Arithmetic 1 successfully.");
        Memo1->Lines->Add("");

        lp1 = 0;
        lp2 = 7;
        p1 = 5;
        p2 = 3;
        p3 = 1;
        p4 = 0xffff;
        wRetCode = Rockey(RY_CALCULATE1, &handle[j], &lp1, &lp2, &p1, &p2, &p3, &p4, buffer);
        if (wRetCode)
        {
            ShowError(wRetCode);
            return;
        }

        Memo1->Lines->Add("Calculate 1, Input: p1=5, p2=3, p3=1, p4=0xffff");
        Memo1->Lines->Add("Result = ((5*23 + 3*17 + 0x2121) < 1) ^ 0xffff = BC71");
        wsprintf(tbuf, "Calculate 1, Output: p1=%x, p2=%x, p3=%x, p4=%x", p1, p2, p3, p4);
        Memo1->Lines->Add(tbuf);
        Memo1->Lines->Add("");

        Memo1->Lines->Add("Writing Arithmetic 2...");
        p1 = 10;
        strcpy((char*)buffer, cmd1);
        wRetCode = Rockey(RY_WRITE_ARITHMETIC, &handle[j], &lp1, &lp2, &p1, &p2, &p3, &p4, buffer);
        if (wRetCode)
        {
            ShowError(wRetCode);
            return;
        }

        Memo1->Lines->Add("Write Arithmetic 2 successfully.");
        Memo1->Lines->Add("");

        lp1 = 10;
        lp2 = 0x12345678;
        p1 = 1;
        p2 = 2;
        p3 = 3;
        p4 = 4;
        wRetCode = Rockey(RY_CALCULATE2, &handle[j], &lp1, &lp2, &p1, &p2, &p3, &p4, buffer);
        if (wRetCode)
        {
            ShowError(wRetCode);
            return;
        }

        Memo1->Lines->Add("Calculate 2, Input: p1=1, p2=2, p3=3, p4=4");
        wsprintf(tbuf, "Result = %04x + %04x + %04x + %04x + 1 + 2 + 3 + 4 = %04x", rc[0], rc[1], rc[2], rc[3], (WORD)(rc[0]+rc[1]+rc[2]+rc[3]+10));
        Memo1->Lines->Add(tbuf);
        wsprintf(tbuf, "Calculate 2, Output: p1=%x, p2=%x, p3=%x, p4=%x", p1, p2, p3, p4);
        Memo1->Lines->Add(tbuf);
        Memo1->Lines->Add("");

        // Set Decrease
        p1 = 9;
        p2 = 0x5;
        p3 = 1;
        wRetCode = Rockey(RY_SET_MODULE, &handle[j], &lp1, &lp2, &p1, &p2, &p3, &p4, buffer);
        if (wRetCode)
        {
            ShowError(wRetCode);
            return;
        }
        Memo1->Lines->Add("Writing Arithmetic 3...");
        p1 = 17;
        strcpy((char*)buffer, cmd2);
        wRetCode = Rockey(RY_WRITE_ARITHMETIC, &handle[j], &lp1, &lp2, &p1, &p2, &p3, &p4, buffer);
        if (wRetCode)
        {
            ShowError(wRetCode);
            return;
        }

        Memo1->Lines->Add("Write Arithmetic 3 successfully.");
        Memo1->Lines->Add("");

        lp1 = 17;
        lp2 = 6;
        p1 = 1;
        p2 = 2;
        p3 = 3;
        p4 = 4;
        wRetCode = Rockey(RY_CALCULATE3, &handle[j], &lp1, &lp2, &p1, &p2, &p3, &p4, buffer);
        if (wRetCode)
        {
            ShowError(wRetCode);
            return;
        }

        wsprintf(tbuf, "Show Module from 6: p1=%x, p2=%x, p3=%x, p4=%x", p1, p2, p3, p4);
        Memo1->Lines->Add(tbuf);
        Memo1->Lines->Add("");

        // Decrease
        p1 = 9;
        wRetCode = Rockey(RY_DECREASE, &handle[j], &lp1, &lp2, &p1, &p2, &p3, &p4, buffer);
        if (wRetCode)
        {
            ShowError(wRetCode);
            return;
        }

        Memo1->Lines->Add("Decrease module 9");

        lp1 = 17;
        lp2 = 6;
        p1 = 1;
        p2 = 2;
        p3 = 3;
        p4 = 4;
        wRetCode = Rockey(RY_CALCULATE3, &handle[j], &lp1, &lp2, &p1, &p2, &p3, &p4, buffer);
        if (wRetCode)
        {
            ShowError(wRetCode);
            return;
        }

        wsprintf(tbuf, "Show Module from 6: p1=%x, p2=%x, p3=%x, p4=%x", p1, p2, p3, p4);
        Memo1->Lines->Add(tbuf);
        Memo1->Lines->Add("");
        //DES test
        Memo1->Lines->Add("DES test start!");

        const int nKeyLen[2] = {8,16};
        //set DES key buffer
        unsigned char keybuf[16];
        p1=DES_SINGLE_MODE;
        for(int k=0;k<nKeyLen[p1];k++)
        {
            keybuf[k]=k; //des key
        }

        //set DES key
        wRetCode = Rockey(RY_SET_DES_KEY, &handle[j], &lp1, &lp2, &p1, &p2, &p3, &p4, keybuf);
        if (wRetCode)
        {
            ShowError(wRetCode);
            return;
        }

        //set rude data to be encrypt
        p2 = 24;
        //p2 must be multiple of 8
        int k;
        for(k=0;k<p2;k++)
        {
            buffer[k]='A';
        }

        Memo1->Lines->Add("DES plane data:");
        String str;
        HexBufferToString(str,buffer,p2);
        Memo1->Lines->Add(str);
        Memo1->Lines->Add("");

        //DES encrypt
        wRetCode = Rockey(RY_DES_ENC, &handle[j], &lp1, &lp2, &p1, &p2, &p3, &p4, buffer);
        if (wRetCode)
        {
            ShowError(wRetCode);
            return;
        }

        Memo1->Lines->Add("DES cipher data:");
        HexBufferToString(str,buffer,p2);
        Memo1->Lines->Add(str);
        Memo1->Lines->Add("");

        //DES decrypt
        wRetCode = Rockey(RY_DES_DEC, &handle[j], &lp1, &lp2, &p1, &p2, &p3, &p4,buffer);
        if (wRetCode)
        {
            ShowError(wRetCode);
            return;
        }

        Memo1->Lines->Add("Data after DES decryption:");
        HexBufferToString(str,buffer,p2);
        Memo1->Lines->Add(str);
        Memo1->Lines->Add("DES test over!");
        Memo1->Lines->Add("");
        //RSA test
        Memo1->Lines->Add("RSA test start!");
        
        //set key N
        SetN(buffer);
        wRetCode = Rockey(RY_SET_RSAKEY_N, &handle[j], &lp1, &lp2, &p1, &p2, &p3, &p4,buffer);
        if (wRetCode)
        {
            ShowError(wRetCode);
            return;
        }
        //set key D
        SetD(buffer);
        wRetCode = Rockey(RY_SET_RSAKEY_D, &handle[j], &lp1, &lp2, &p1, &p2, &p3, &p4,buffer);
        if (wRetCode)
        {
            ShowError(wRetCode);
            return;
        }
        //set rude data to be encrypt
        memset(buffer,0,8);
        for(k=0;k<120;k++)
        {
            buffer[k] = 'A';
        }

        buffer[k]='\0';
        Memo1->Lines->Add("RSA plane data:");
        HexBufferToString(str,buffer,120);
        Memo1->Lines->Add(str);
        Memo1->Lines->Add("");

        //set pad type
        p2 = 119;
        p3 = RSA_ROCKEY_PADDING;
        //rsa public key encrypt
        p1 = RSA_PUBLIC_KEY;
        wRetCode = Rockey(RY_RSA_ENC, &handle[j], &lp1, &lp2, &p1, &p2, &p3, &p4,buffer);
        if (wRetCode)
        {
            ShowError(wRetCode);
            return;
        }

        Memo1->Lines->Add("RSA cipher data:");
        HexBufferToString(str,buffer,128);
        Memo1->Lines->Add(str);
        Memo1->Lines->Add("");

        //rsa private key decrypt
        p1 = RSA_PRIVATE_KEY;
        wRetCode = Rockey(RY_RSA_DEC, &handle[j], &lp1, &lp2, &p1, &p2, &p3, &p4,buffer);
        if (wRetCode)
        {
            ShowError(wRetCode);
            return;
        }

        Memo1->Lines->Add("Data after RSA decryption:");
        HexBufferToString(str,buffer,120);
        Memo1->Lines->Add(str);
        Memo1->Lines->Add("RSA test over!");

        wRetCode = Rockey(RY_CLOSE, &handle[j], &lp1, &lp2, &p1, &p2, &p3, &p4, buffer);
        if (wRetCode)
        {
            ShowError(wRetCode);
            return;
        }

        Memo1->Lines->Add("Close Rockey4Smart Successfully!");
        Memo1->Lines->Add("------------------------------------------------------------------");
    }
}
//---------------------------------------------------------------------------

String& TForm1::HexBufferToString(String& str,BYTE* buf,int len)
{
    str = "0x";
    for(int k=0;k<len;k++)
    {
        String strTemp;
        strTemp=IntToHex(buf[k],2);
        str += strTemp;
    }
    return str;
}
//---------------------------------------------------------------------------

void TForm1::SetN(BYTE* buffer)
{
    buffer[0]=(char)191;    buffer[1]=(char)25;     buffer[2]=(char)33;     buffer[3]=(char)32;
    buffer[4]=(char)213;    buffer[5]=(char)241;    buffer[6]=(char)45;     buffer[7]=(char)44;
    buffer[8]=(char)252;    buffer[9]=(char)169;    buffer[10]=(char)197;   buffer[11]=(char)176;
    buffer[12]=(char)59;    buffer[13]=(char)209;   buffer[14]=(char)241;   buffer[15]=(char)92;
    buffer[16]=(char)142;   buffer[17]=(char)136;   buffer[18]=(char)144;   buffer[19]=(char)40;
    buffer[20]=(char)170;   buffer[21]=(char)94;    buffer[22]=(char)147;   buffer[23]=(char)97;
    buffer[24]=(char)185;   buffer[25]=(char)135;   buffer[26]=(char)234;   buffer[27]=(char)134;
    buffer[28]=(char)143;   buffer[29]=(char)84;    buffer[30]=(char)152;   buffer[31]=(char)23;
    buffer[32]=(char)27;    buffer[33]=(char)72;    buffer[34]=(char)238;   buffer[35]=(char)245;
    buffer[36]=(char)2;     buffer[37]=(char)144;   buffer[38]=(char)91;    buffer[39]=(char)70;
    buffer[40]=(char)57;    buffer[41]=(char)20;    buffer[42]=(char)192;   buffer[43]=(char)98;
    buffer[44]=(char)77;    buffer[45]=(char)31;    buffer[46]=(char)111;   buffer[47]=(char)227;
    buffer[48]=(char)16;    buffer[49]=(char)115;   buffer[50]=(char)182;   buffer[51]=(char)211;
    buffer[52]=(char)95;    buffer[53]=(char)143;   buffer[54]=(char)108;   buffer[55]=(char)241;
    buffer[56]=(char)115;   buffer[57]=(char)2;     buffer[58]=(char)248;   buffer[59]=(char)62;
    buffer[60]=(char)121;   buffer[61]=(char)134;   buffer[62]=(char)55;    buffer[63]=(char)10;
    buffer[64]=(char)30;    buffer[65]=(char)74;    buffer[66]=(char)145;   buffer[67]=(char)132;
    buffer[68]=(char)79;    buffer[69]=(char)159;   buffer[70]=(char)156;   buffer[71]=(char)18;
    buffer[72]=(char)223;   buffer[73]=(char)175;   buffer[74]=(char)84;    buffer[75]=(char)197;
    buffer[76]=(char)130;   buffer[77]=(char)17;    buffer[78]=(char)95;    buffer[79]=(char)230;
    buffer[80]=(char)200;   buffer[81]=(char)202;   buffer[82]=(char)68;    buffer[83]=(char)202;
    buffer[84]=(char)132;   buffer[85]=(char)22;    buffer[86]=(char)150;   buffer[87]=(char)18;
    buffer[88]=(char)193;   buffer[89]=(char)127;   buffer[90]=(char)10;    buffer[91]=(char)42;
    buffer[92]=(char)158;   buffer[93]=(char)129;   buffer[94]=(char)185;   buffer[95]=(char)153;
    buffer[96]=(char)219;   buffer[97]=(char)226;   buffer[98]=(char)70;    buffer[99]=(char)56;
    buffer[100]=(char)100;  buffer[101]=(char)74;   buffer[102]=(char)149;  buffer[103]=(char)14;
    buffer[104]=(char)92;   buffer[105]=(char)14;   buffer[106]=(char)201;  buffer[107]=(char)106;
    buffer[108]=(char)93;   buffer[109]=(char)221;  buffer[110]=(char)96;   buffer[111]=(char)221;
    buffer[112]=(char)148;  buffer[113]=(char)233;  buffer[114]=(char)46;   buffer[115]=(char)75;
    buffer[116]=(char)195;  buffer[117]=(char)70;   buffer[118]=(char)77;   buffer[119]=(char)157;
    buffer[120]=(char)54;   buffer[121]=(char)99;   buffer[122]=(char)95;   buffer[123]=(char)233;
    buffer[124]=(char)8;    buffer[125]=(char)180;  buffer[126]=(char)4;    buffer[127]=(char)178;
}
void TForm1::SetD(BYTE* buffer)
{
    buffer[0]=(char)117;    buffer[1]=(char)86;     buffer[2]=(char)160;    buffer[3]=(char)104;
    buffer[4]=(char)137;    buffer[5]=(char)51;     buffer[6]=(char)8;      buffer[7]=(char)4;
    buffer[8]=(char)73;     buffer[9]=(char)178;    buffer[10]=(char)136;   buffer[11]=(char)200;
    buffer[12]=(char)145;   buffer[13]=(char)26;    buffer[14]=(char)66;    buffer[15]=(char)84;
    buffer[16]=(char)48;    buffer[17]=(char)143;   buffer[18]=(char)169;   buffer[19]=(char)74;
    buffer[20]=(char)251;   buffer[21]=(char)86;    buffer[22]=(char)25;    buffer[23]=(char)188;
    buffer[24]=(char)162;   buffer[25]=(char)3;     buffer[26]=(char)162;   buffer[27]=(char)131;
    buffer[28]=(char)107;   buffer[29]=(char)81;    buffer[30]=(char)3;     buffer[31]=(char)49;
    buffer[32]=(char)251;   buffer[33]=(char)133;   buffer[34]=(char)157;   buffer[35]=(char)164;
    buffer[36]=(char)147;   buffer[37]=(char)138;   buffer[38]=(char)72;    buffer[39]=(char)81;
    buffer[40]=(char)111;   buffer[41]=(char)137;   buffer[42]=(char)65;    buffer[43]=(char)48;
    buffer[44]=(char)154;   buffer[45]=(char)11;    buffer[46]=(char)42;    buffer[47]=(char)215;
    buffer[48]=(char)188;   buffer[49]=(char)180;   buffer[50]=(char)175;   buffer[51]=(char)54;
    buffer[52]=(char)79;    buffer[53]=(char)40;    buffer[54]=(char)163;   buffer[55]=(char)60;
    buffer[56]=(char)123;   buffer[57]=(char)20;    buffer[58]=(char)96;    buffer[59]=(char)196;
    buffer[60]=(char)188;   buffer[61]=(char)10;    buffer[62]=(char)254;   buffer[63]=(char)213;
    buffer[64]=(char)17;    buffer[65]=(char)244;   buffer[66]=(char)224;   buffer[67]=(char)61;
    buffer[68]=(char)197;   buffer[69]=(char)77;    buffer[70]=(char)6;     buffer[71]=(char)3;
    buffer[72]=(char)22;    buffer[73]=(char)32;    buffer[74]=(char)157;   buffer[75]=(char)117;
    buffer[76]=(char)92;    buffer[77]=(char)218;   buffer[78]=(char)114;   buffer[79]=(char)245;
    buffer[80]=(char)73;    buffer[81]=(char)10;    buffer[82]=(char)47;    buffer[83]=(char)221;
    buffer[84]=(char)35;    buffer[85]=(char)24;    buffer[86]=(char)123;   buffer[87]=(char)235;
    buffer[88]=(char)79;    buffer[89]=(char)237;   buffer[90]=(char)175;   buffer[91]=(char)153;
    buffer[92]=(char)209;   buffer[93]=(char)193;   buffer[94]=(char)156;   buffer[95]=(char)80;
    buffer[96]=(char)253;   buffer[97]=(char)255;   buffer[98]=(char)54;    buffer[99]=(char)169;
    buffer[100]=(char)111;  buffer[101]=(char)113;  buffer[102]=(char)196;  buffer[103]=(char)202;
    buffer[104]=(char)121;  buffer[105]=(char)77;   buffer[106]=(char)27;   buffer[107]=(char)100;
    buffer[108]=(char)179;  buffer[109]=(char)12;   buffer[110]=(char)211;  buffer[111]=(char)140;
    buffer[112]=(char)217;  buffer[113]=(char)44;   buffer[114]=(char)182;  buffer[115]=(char)71;
    buffer[116]=(char)208;  buffer[117]=(char)247;  buffer[118]=(char)92;   buffer[119]=(char)218;
    buffer[120]=(char)41;   buffer[121]=(char)105;  buffer[122]=(char)234;  buffer[123]=(char)216;
    buffer[124]=(char)85;   buffer[125]=(char)118;  buffer[126]=(char)54;   buffer[127]=(char)8;
}
void __fastcall TForm1::Button2Click(TObject *Sender)
{
   Memo1->Clear();
}
//---------------------------------------------------------------------------

