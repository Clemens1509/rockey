
#include <vcl.h>
#pragma hdrstop
#include "Unit1.h"

//---------------------------------------------------------------------------
#include "Ry4S.h"
#pragma comment(lib,"Ry4S_BCB.lib")
//---------------------------------------------------------------------------

#pragma package(smart_init)
#pragma resource "*.dfm"
TForm1 *Form1;

__fastcall TForm1::TForm1(TComponent* Owner)
        : TForm(Owner)
{
}

//###########################################################################


void _fastcall TForm1::ShowError(WORD wRetCode)
{
    if (wRetCode == 0) return;
    Memo1->Lines->Add("Error Code: " + AnsiString(wRetCode));
}
//---------------------------------------------------------------------------

void __fastcall TForm1::Button1Click(TObject *Sender)
{
    WORD handle;
    WORD p1, p2, p3, p4, wRetCode;
    DWORD lp1, lp2;
    BYTE buffer[1024];
    WORD rc[4];
    int i, j;
    char tbuf[1024];

    memset(tbuf,0,sizeof(tbuf));

    p1 = 0xc44c;
    p2 = 0xc8f8;
    p3 = 0x0799;
    p4 = 0xc43b;

    wRetCode = Rockey(RY_FIND,&handle,&lp1, &lp2, &p1, &p2, &p3, &p4, buffer);
    if (wRetCode)
    {
        ShowError(wRetCode);
        return;
    }

    wsprintf(tbuf, "Found Rockey4Smart: %08X", lp1);
    Memo1->Lines->Add(tbuf);

    wRetCode = Rockey(RY_OPEN, &handle, &lp1, &lp2, &p1, &p2, &p3, &p4, buffer);
    if (wRetCode)
    {
        ShowError(wRetCode);
        return;
    }

    wRetCode = Rockey(RY_VERSION, &handle, &lp1, &lp2, &p1, &p2, &p3, &p4, buffer);
    if (wRetCode)
    {
        ShowError(wRetCode);
        return;
    }
    if(lp2 <259)
    {
        Memo1->Lines->Add("Old dongles don't support new TIME and COUNT��please use V1.03 dongle for test.");
       ShowError(wRetCode);
       return;
    }

    i = 1;
    while (wRetCode == 0)
    {
        wRetCode = Rockey(RY_FIND_NEXT, &handle, &lp1, &lp2, &p1, &p2, &p3, &p4, buffer);
        if (wRetCode == ERR_NOMORE) break;
        if (wRetCode)
        {
            ShowError(wRetCode);
            return;
        }
        wRetCode = Rockey(RY_OPEN, &handle, &lp1, &lp2, &p1, &p2, &p3, &p4, buffer);
        if (wRetCode)
        {
            ShowError(wRetCode);
            return;
        }
        i++;

        wsprintf(tbuf, "Foundd Rockey4Smart: %08X", lp1);
        Memo1->Lines->Add(tbuf);
    }

    Memo1->Lines->Add("");

    for (j=0;j<i;j++)
    {
        p1 = 0; //Unit number
        p3 = 1; //1,decreasing. 0,increasing
        //buffer[0~3]-->module value
        //memset(buffer, '\0', 512);

        DWORD dwCount = 10;
        memset(buffer,0,sizeof(buffer));
        memcpy(buffer,&dwCount,sizeof(DWORD));
        //set count authorization
        wRetCode = Rockey(RY_SET_COUNTER_EX,&handle, &lp1, &lp2, &p1, &p2, &p3, &p4, buffer);
        if (wRetCode)
        {
            ShowError(wRetCode);
            return;
        }
        wsprintf(tbuf, "Set authorized using times. Unit %d, %d times",p1,dwCount);
        Memo1->Lines->Add(tbuf);

        memset(buffer,0,sizeof(buffer));
        wRetCode = Rockey(RY_GET_COUNTER_EX,&handle, &lp1, &lp2, &p1, &p2, &p3, &p4, buffer);
        if (wRetCode)
        {
            ShowError(wRetCode);
            return;
        }
        dwCount = *(DWORD*)buffer;
        wsprintf(tbuf, "Get authorized using times. Unit %d, %d times",p1,dwCount);
        Memo1->Lines->Add(tbuf);

        wsprintf(tbuf, "Close Rockey4Smart successfully! ");
        Memo1->Lines->Add(tbuf);

    }
}
//---------------------------------------------------------------------------

String& TForm1::HexBufferToString(String& str,BYTE* buf,int len)
{
    str = "0x";
    for(int k=0;k<len;k++)
    {
        String strTemp;
        strTemp=IntToHex(buf[k],2);
        str += strTemp;
    }
    return str;
}
//---------------------------------------------------------------------------

