#ifndef Unit1H
#define Unit1H
//---------------------------------------------------------------------------


#include <Classes.hpp>
#include <Controls.hpp>
#include <StdCtrls.hpp>
#include <Forms.hpp>

class TForm1 : public TForm
{
__published:	// IDE-managed Components
    TButton *Button1;
    TMemo *Memo1;
    void __fastcall Button1Click(TObject *Sender);
private:	// User declarations
public:		// User declarations
    __fastcall TForm1(TComponent* Owner);


//---------------------------------------------------------------------------
    void _fastcall ShowError(WORD wRetCode);
    String& HexBufferToString(String& str,BYTE* buf,int len);
    void SetN(BYTE* buffer);
    void SetD(BYTE* buffer);
};



extern PACKAGE TForm1 *Form1;
//---------------------------------------------------------------------------
#endif
