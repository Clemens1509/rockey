
#include <vcl.h>
#pragma hdrstop
#include "Unit1.h"

//---------------------------------------------------------------------------
#include "Ry4S.h"
#pragma comment(lib,"Ry4S_BCB.lib")
//---------------------------------------------------------------------------

#pragma package(smart_init)
#pragma resource "*.dfm"
TForm1 *Form1;

__fastcall TForm1::TForm1(TComponent* Owner)
        : TForm(Owner)
{
}

//###########################################################################


void _fastcall TForm1::ShowError(WORD wRetCode)
{
    if (wRetCode == 0) return;
    Memo1->Lines->Add("Error Code: " + AnsiString(wRetCode));
}
//---------------------------------------------------------------------------

void __fastcall TForm1::Button1Click(TObject *Sender)
{
    WORD handle;
    WORD p1, p2, p3, p4, wRetCode;
    DWORD lp1, lp2;
    BYTE buffer[1024];
    WORD rc[4];
    int i, j;
    char tbuf[1024];

    p1 = 0xc44c;
    p2 = 0xc8f8;
    p3 = 0x0799;
    p4 = 0xc43b;

    wRetCode = Rockey(RY_FIND,&handle,&lp1, &lp2, &p1, &p2, &p3, &p4, buffer);
    if (wRetCode)
    {
        ShowError(wRetCode);
        return;
    }

    wsprintf(tbuf, "Found Rockey4Smart: %08x", lp1);
    Memo1->Lines->Add(tbuf);

    wRetCode = Rockey(RY_OPEN, &handle, &lp1, &lp2, &p1, &p2, &p3, &p4, buffer);
    if (wRetCode)
    {
        ShowError(wRetCode);
        return;
    }

    wRetCode = Rockey(RY_VERSION, &handle, &lp1, &lp2, &p1, &p2, &p3, &p4, buffer);
    if (wRetCode)
    {
        ShowError(wRetCode);
        return;
    }
    if(lp2 <259)
    {
       Memo1->Lines->Add("Old dongles don't support new count and time,\nplease use V1.03 for test");
       ShowError(wRetCode);
       return;
    }


    i = 1;
    while (wRetCode == 0)
    {
        wRetCode = Rockey(RY_FIND_NEXT, &handle, &lp1, &lp2, &p1, &p2, &p3, &p4, buffer);
        if (wRetCode == ERR_NOMORE) break;
        if (wRetCode)
        {
            ShowError(wRetCode);
            return;
        }
        wRetCode = Rockey(RY_OPEN, &handle, &lp1, &lp2, &p1, &p2, &p3, &p4, buffer);
        if (wRetCode)
        {
            ShowError(wRetCode);
            return;
        }
        i++;

        wsprintf(tbuf, "Found Rockey4Smart: %08x", lp1);
        Memo1->Lines->Add(tbuf);
    }

    Memo1->Lines->Add("");

    for (j=0;j<i;j++)
    {
        p1 = 0;
        p2 = 0;
        p3 = 0;
        p4 = 0;
        memset(buffer, '\0', 512);


        SYSTEMTIME   time;
        GetLocalTime(&time);   //get the current local date and time.
        wsprintf(tbuf,"Local system time��%d-%d-%d %d:%d:%d",time.wYear,time.wMonth,time.wDay,time.wHour,time.wMinute,time.wSecond);
        Memo1->Lines->Add(tbuf);
        GetSystemTime(&time);//get the current system date and time. The system time is expressed in Coordinated Universal Time (UTC).
        wsprintf(tbuf,"UTC time:%d-%d-%d %d:%d:%d",time.wYear,time.wMonth,time.wDay,time.wHour,time.wMinute,time.wSecond);
        Memo1->Lines->Add(tbuf);

        time.wMonth = 12;
        time.wDay = 28;
        time.wHour = 12;
        time.wMinute = 0;

        wsprintf(tbuf,"Set authorized using date(UTC)��%d-%d-%d %d:%d:%d",time.wYear,time.wMonth,time.wDay,time.wHour,time.wMinute,time.wSecond);
        Memo1->Lines->Add(tbuf);

        memset(buffer, '\0', 512);

        memcpy(buffer,&time,sizeof(SYSTEMTIME));


       //set authorized using date
       p1 = 0;
       p3 = 1;

       wRetCode = Rockey(RY_SET_TIMER_EX,&handle, &lp1, &lp2, &p1, &p2, &p3, &p4, buffer);
        if (wRetCode)
        {
            ShowError(wRetCode);
            return;
        }
        SYSTEMTIME   timeEx;
        GetSystemTime(&timeEx);
        memset(buffer, '\0', 512);
        memcpy(buffer,&timeEx,sizeof(SYSTEMTIME));
        wRetCode = Rockey(RY_ADJUST_TIMER_EX,&handle,&lp1,&lp2,&p1,&p2,&p3,&p4,buffer);
        if(wRetCode)
        {
           ShowError(wRetCode);
        }

        SYSTEMTIME   timelic;
        //get authorized date
        wRetCode = Rockey(RY_GET_TIMER_EX,&handle, &lp1, &lp2, &p1, &p2, &p3, &p4, (BYTE*)(&timelic));
        if (wRetCode)
        {
            ShowError(wRetCode);
            return;
        }
        if(p3 == 1)
        {
         wsprintf(tbuf,"Got authorized using date(UTC)��%d-%d-%d %d:%d:%d",timelic.wYear,timelic.wMonth,timelic.wDay,timelic.wHour,timelic.wMinute,timelic.wSecond);
         Memo1->Lines->Add(tbuf);
         FILETIME ft;
         SystemTimeToFileTime(&timelic,&ft);
         FileTimeToLocalFileTime(&ft,&ft);
         FileTimeToSystemTime(&ft,&timelic);
         wsprintf(tbuf,"Got authorized using date(Local)��%d-%d-%d %d:%d:%d",timelic.wYear,timelic.wMonth,timelic.wDay,timelic.wHour,timelic.wMinute,timelic.wSecond);
         Memo1->Lines->Add(tbuf);
        }


       //set authorized using time��hour��
        p3 = 2;
        DWORD dwHour = 10;
        memset(buffer,0,sizeof(buffer));
        memcpy(buffer,&dwHour,sizeof(int));
        wRetCode = Rockey(RY_SET_TIMER_EX,&handle, &lp1, &lp2, &p1, &p2, &p3, &p4, buffer);
        if (wRetCode)
        {
            ShowError(wRetCode);
            return;
        }
        wsprintf(tbuf,"Set authoried using time. Unit %d, %d hours",p1,dwHour);
        Memo1->Lines->Add(tbuf);

        SYSTEMTIME   timeSys;
        GetSystemTime(&timeSys);
        memset(buffer, '\0', 512);
        memcpy(buffer,&timeSys,sizeof(SYSTEMTIME));
        wRetCode = Rockey(RY_ADJUST_TIMER_EX,&handle,&lp1,&lp2,&p1,&p2,&p3,&p4,buffer);
        if(wRetCode)
        {
           ShowError(wRetCode);
        }
          //get authorized using time(hour) 
        wRetCode = Rockey(RY_GET_TIMER_EX,&handle, &lp1, &lp2, &p1, &p2, &p3, &p4, buffer);
        if (wRetCode)
        {
            ShowError(wRetCode);
            return;
        }
        if(p3 == 2)
        {
          dwHour =*(DWORD*)buffer;
          wsprintf(tbuf,"Got authorized using time. Unit %d, %d hours",p1,dwHour);
          Memo1->Lines->Add(tbuf);
        }

        wRetCode = Rockey(RY_CLOSE, &handle, &lp1, &lp2, &p1, &p2, &p3, &p4, buffer);
        if (wRetCode)
        {
            ShowError(wRetCode);
            return;
        }

        Memo1->Lines->Add("Close Rockey4Smart Success!");
    }
}
//---------------------------------------------------------------------------

String& TForm1::HexBufferToString(String& str,BYTE* buf,int len)
{
    str = "0x";
    for(int k=0;k<len;k++)
    {
        String strTemp;
        strTemp=IntToHex(buf[k],2);
        str += strTemp;
    }
    return str;
}
//---------------------------------------------------------------------------

