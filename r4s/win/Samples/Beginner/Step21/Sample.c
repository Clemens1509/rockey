/************************************************************************
Check multi-modules
************************************************************************/

#include <windows.h>
#include <stdio.h>
#include "ry4s.h"		// Include Rockey4Smart Header File

void main()
{
	int i, j, rynum;
	WORD retcode;
	DWORD HID[16];

	WORD handle[16], p1, p2, p3, p4;	// Rockey4Smart Variable
	DWORD lp1, lp2;						// Rockey4Smart Variable
	BYTE buffer[1024];					// Rockey4Smart Variable
	char tbuf[256]={0};

	p1 = 0xc44c;	// Rockey4Smart Demo Password1
	p2 = 0xc8f8;	// Rockey4Smart Demo Password2
	p3 = 0;			// Program needn't Password3, Set to 0
	p4 = 0;			// Program needn't Password4, Set to 0

	// Try to find all Rockey4Smart
	for (i=0;i<16;i++)
	{
		if (0 == i)
		{
			retcode = Rockey(RY_FIND, &handle[i], &lp1, &lp2, &p1, &p2, &p3, &p4, buffer);
			if (retcode == ERR_NOMORE) break;
		}
		else
		{
			// Notice : lp1 = Last found hardware ID
			retcode = Rockey(RY_FIND_NEXT, &handle[i], &lp1, &lp2, &p1, &p2, &p3, &p4, buffer);
			if (retcode == ERR_NOMORE) break;
		}
		// Error
		if (retcode)	
		{
			printf("Error Code: %d\n", retcode);
			return;
		}

		printf("Found Rockey4Smart: %08X\n", lp1);
		HID[i] = lp1;	// Save HardID

		//Open Rockey4Smart
		retcode = Rockey(RY_OPEN, &handle[i], &lp1, &lp2, &p1, &p2, &p3, &p4, buffer);
		if (retcode)	// Error
		{
			printf("Error Code: %d\n", retcode);
			return;
		}
	}
	printf("\n");

	rynum = i;

	// Do our work
	for (i=0;i<rynum;i++)
	{
		printf("Rockey4Smart %08X module status: ", HID[i]);
		
		for (j=0;j<64;j++)
		{
			memset(tbuf,0x00,256);
			p1 = j;		// Module No
			//Check module
			retcode = Rockey(RY_CHECK_MODULE, &handle[i], &lp1, &lp2, &p1, &p2, &p3, &p4, buffer);
			// Error
			if (retcode)	
			{
				printf("Error Code: %d\n", retcode);
				return;
			}

			if (p2) 
			{
				sprintf(tbuf,"Module %d is valid!",j);
				printf("%s\n",tbuf);
			}
			else 
			{
				sprintf(tbuf,"Module %d is invalid!",j);
				printf("%s\n",tbuf);
			}
		}
		printf("\n");
	}

	// Close all opened Rockey4Smart
	for (i=0;i<rynum;i++)
	{
		retcode = Rockey(RY_CLOSE, &handle[i], &lp1, &lp2, &p1, &p2, &p3, &p4, buffer);
		if (retcode)
		{
			printf("Error Code: %d\n", retcode);
			return;
		}
	}
}
