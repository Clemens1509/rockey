/*******************************************************************
In order to use Rockey4Smart to encrypt or control application programs 
or modules running,the general way is writing a number to user memory and 
decreasing this number continually.

*******************************************************************/

#include <windows.h>
#include <stdio.h>
#include <conio.h>
#include "ry4s.h"

void ShowERR(WORD retcode)
{
	if (retcode == 0) return;
	printf("Error Code: %d\n", retcode);
}

void main()
{
	WORD handle[16], p1, p2, p3, p4, retcode;
	DWORD lp1, lp2;
	BYTE buffer[1024];
	char temp[20];
	int i, j,num;
    
	char* s = "100";
	p1 = 0xc44c;
	p2 = 0xc8f8;
	p3 = 0;
	p4 = 0;
	

  
	//Find Rockey4Smart with specified password
	retcode = Rockey(RY_FIND, &handle[0], &lp1, &lp2, &p1, &p2, &p3, &p4, buffer);
	if (retcode)
	{
		ShowERR(retcode);
		return;
	}
	printf("Find Rockey4Smart: %08X\n", lp1);
	//Open Rockey4Smart
	retcode = Rockey(RY_OPEN, &handle[0], &lp1, &lp2, &p1, &p2, &p3, &p4, buffer);
	if (retcode)
	{
		ShowERR(retcode);
		return;
	}

	i = 1;
	while (retcode == 0)
	{
		//Find next Rockey4Smart with the same password
		retcode = Rockey(RY_FIND_NEXT, &handle[i], &lp1, &lp2, &p1, &p2, &p3, &p4, buffer);
		if (retcode == ERR_NOMORE) break;
		if (retcode)
		{
			ShowERR(retcode);
			return;
		}

		retcode = Rockey(RY_OPEN, &handle[i], &lp1, &lp2, &p1, &p2, &p3, &p4, buffer);
		if (retcode)
		{
			ShowERR(retcode);
			return;
		}

		i++;
		printf("Find Rockey4Smart: %08X\n", lp1);
	}
	printf("\n");

	for (j=0;j<i;j++)
	{   
		
		p1 = 0;
		p2 = 1;
		strcpy((char*)buffer, "A");
		//Write data to user memory
		retcode = Rockey(RY_WRITE, &handle[j], &lp1, &lp2, &p1, &p2, &p3, &p4, buffer);
		if (retcode)
		{
			ShowERR(retcode);
			return;
		}
		printf("Write: b\n");
       
		
		p1 = 0;
		p2 = 1;
		memset(buffer, 0, 64);
		//Read data from user memory
		retcode = Rockey(RY_READ, &handle[j], &lp1, &lp2, &p1, &p2, &p3, &p4, buffer);
		if (retcode)
		{
			ShowERR(retcode);
			return;
		}
		printf("Read: %s\n", buffer);
        

		memcpy(temp, buffer, 2);
		//temp[2] = '\0';
		num=atoi(temp);

		if(num)
		{
		   printf("Hello FeiTian!\n"); 
			num--;
		}
		else
		{
			return;
		}
		p1 = 0;
		p2 = 1;
		sprintf(buffer, "%ld", num);
		retcode = Rockey(RY_WRITE, &handle[j], &lp1, &lp2, &p1, &p2, &p3, &p4, buffer);
		if (retcode)
		{
			ShowERR(retcode);
			return;
		}
		printf("Write: %ld\n",num);
		
		
		//Close Rockey4Smart handle
		retcode = Rockey(RY_CLOSE, &handle[j], &lp1, &lp2, &p1, &p2, &p3, &p4, buffer);
		if (retcode)
		{
			ShowERR(retcode);
			return;
		}

		printf("\n");
      

        } 
	

}
