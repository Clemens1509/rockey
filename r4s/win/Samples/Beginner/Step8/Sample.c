/******************************************************************
Show how to write and read user ID to dongle.
******************************************************************/
#include <windows.h>
#include <stdio.h>
#include "ry4s.h"		// Include Rockey4Smart Header File

void main()
{
	// ==============================================================
	WORD retcode;
	WORD handle, p1, p2, p3, p4;	// Rockey4Smart Variable
	DWORD lp1, lp2;					// Rockey4Smart Variable
	BYTE buffer[1024];				// Rockey4Smart Variable

	p1 = 0xc44c;	// Rockey4Smart Demo Password1
	p2 = 0xc8f8;	// Rockey4Smart Demo Password2
	p3 = 0x0799;	// Rockey4Smart Demo Password3
	p4 = 0xc43b;	// Rockey4Smart Demo Password4

	// Try to find specified Rockey4Smart
	retcode = Rockey(RY_FIND, &handle, &lp1, &lp2, &p1, &p2, &p3, &p4, buffer);
	//Not found
	if (retcode)	// Not found
	{
		printf("ROCKEY not found!\n");
		return;
	}
	
	//Open Rockey4Smart
	retcode = Rockey(RY_OPEN, &handle, &lp1, &lp2, &p1, &p2, &p3, &p4, buffer);
	// Error
	if (retcode)		
	{	printf("Error Code: %d\n", retcode);		
		return;	
	}
    

    lp1 = 0x88888888;  //User ID
	//Write user ID
	retcode = Rockey(RY_WRITE_USERID, &handle, &lp1, &lp2, &p1, &p2, &p3, &p4, buffer);
	//Error
	if (retcode) 
	{	
		printf("Error Code: %d\n", retcode);	
		return;
	}	
	printf("Write User ID: %08X\n", lp1);

	//Close Rockey4Smart handle
	retcode = Rockey(RY_CLOSE, &handle, &lp1, &lp2, &p1, &p2, &p3, &p4, buffer);
	if (retcode)
	{
		printf("Error Code: %d\n", retcode);
		return;
	}
	
	
}
