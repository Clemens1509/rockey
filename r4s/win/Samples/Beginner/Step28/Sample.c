#include <windows.h>
#include <stdio.h>
#include <conio.h>
#include "ry4s.h"

void ShowERR(WORD retcode)
{
	if (retcode == 0) return;
	printf("Error Code: %d\n", retcode);
}

void main()
{
	WORD handle[16], p1, p2, p3, p4, retcode;
    DWORD lp1, lp2;
	BYTE buffer[1024];
	WORD rc[4];

	
    
	int i, j;
  
	char cmd1[] = "A=E|E,B=F|F,C=G|G,D=H|H";


	p1 = 0xc44c;
	p2 = 0xc8f8;
	p3 = 0x0799;
	p4 = 0xc43b;

	//Find Rockey4Smart with specified password
	retcode = Rockey(RY_FIND, &handle[0], &lp1, &lp2, &p1, &p2, &p3, &p4, buffer);
	if (retcode)
	{
		ShowERR(retcode);
		return;
	}
	printf("Find Rockey4Smart: %08X\n", lp1);

	//Open Rockey4Smart
	retcode = Rockey(RY_OPEN, &handle[0], &lp1, &lp2, &p1, &p2, &p3, &p4, buffer);
	if (retcode)
	{
		ShowERR(retcode);
		return;
	}

	i = 1;
	while (retcode == 0)
	{
		//Find next Rockey4Smart with the same password
		retcode = Rockey(RY_FIND_NEXT, &handle[i], &lp1, &lp2, &p1, &p2, &p3, &p4, buffer);
		if (retcode == ERR_NOMORE) break;
		if (retcode)
		{
			ShowERR(retcode);
			return;
		}

		retcode = Rockey(RY_OPEN, &handle[i], &lp1, &lp2, &p1, &p2, &p3, &p4, buffer);
		if (retcode)
		{
			ShowERR(retcode);
			return;
		}

		i++;
	    
		printf("Find Rockey4Smart: %08X\n", lp1);
	}
	printf("\n");

	//Operate on each Rockey4Smart
	for (j=0;j<i;j++)
	{   
		
		
		lp2 = 0x12345678;
		//Get return code of seed
		retcode = Rockey(RY_SEED, &handle[j], &lp1, &lp2, &p1, &p2, &p3, &p4, buffer);
		if (retcode)
		{
			ShowERR(retcode);
			return;
		}
		printf("Return code of seed: %04X %04X %04X %04X\n", p1, p2, p3, p4);
		
		rc[0] = p1;
		rc[1] = p2;
		rc[2] = p3;
		rc[3] = p4;

		
		p1 = 0;
		strcpy((char*)buffer, cmd1);
		//Write user's arithmetic to dongle
		retcode = Rockey(RY_WRITE_ARITHMETIC, &handle[j], &lp1, &lp2, &p1, &p2, &p3, &p4, buffer);
		if (retcode)
		{
			ShowERR(retcode);
			return;
		}
		printf("Write Arithmetic\n");

		lp1 = 0;          //starting position
		lp2 = 0x12345678; //seed code
		p1 = 1;           //input value 1
		p2 = 2;           //input value 2
		p3 = 3;           //input value 3
		p4 = 4;           //input value 4
		//Calculate, the result is decided by user's arithmetic wrote to dongle
		retcode = Rockey(RY_CALCULATE2, &handle[j], &lp1, &lp2, &p1, &p2, &p3, &p4, buffer);
		if (retcode)
		{
			ShowERR(retcode);
			return;
		}
		printf("Calculate2 Input: p1=1, p2=2, p3=3, p4=4\n");
    	printf("Calculate2 Output: p1=%x, p2=%x, p3=%x, p4=%x\n", p1, p2, p3, p4);
		
		
		printf("\n");
	    if(rc[0]==p1 && rc[1]==p2 && rc[2]==p3 && rc[3]==p4)           
			printf("Hello FeiTian!\n");
		else 
			break;
	
		//Close Rockey4Smart handle
		retcode = Rockey(RY_CLOSE, &handle[j], &lp1, &lp2, &p1, &p2, &p3, &p4, buffer);
		if (retcode)
		{
			ShowERR(retcode);
			return;
		}

		printf("\n");
	}
}
