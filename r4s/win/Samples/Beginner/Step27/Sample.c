#include <windows.h>
#include <stdio.h>
#include <conio.h>
#include "ry4s.h"

void ShowERR(WORD retcode)
{
	if (retcode == 0) return;
	printf("Error Code: %d\n", retcode);
}

void main()
{
	WORD handle[16], p1, p2, p3, p4, retcode;
	DWORD findlp1,truelp1;

	DWORD lp1, lp2;
	BYTE buffer[1024];

	
    
	int i, j;
  
	char cmd[] = "A=E|E,B=F|F,C=G|G,D=H|H";


	p1 = 0xc44c;
	p2 = 0xc8f8;
	p3 = 0x0799;
	p4 = 0xc43b;

	//Find Rockey4Smart with specified password
	retcode = Rockey(RY_FIND, &handle[0], &lp1, &lp2, &p1, &p2, &p3, &p4, buffer);
	if (retcode)
	{
		ShowERR(retcode);
		return;
	}
	printf("Find Rockey4Smart: %08X\n", lp1);
	findlp1=lp1;
	
	//Open Rockey4Smart
	retcode = Rockey(RY_OPEN, &handle[0], &lp1, &lp2, &p1, &p2, &p3, &p4, buffer);
	if (retcode)
	{
		ShowERR(retcode);
		return;
	}

	i = 1;
	while (retcode == 0)
	{
		//Find next Rockey4Smart with the same password
		retcode = Rockey(RY_FIND_NEXT, &handle[i], &lp1, &lp2, &p1, &p2, &p3, &p4, buffer);
		if (retcode == ERR_NOMORE) break;
		if (retcode)
		{
			ShowERR(retcode);
			return;
		}

		retcode = Rockey(RY_OPEN, &handle[i], &lp1, &lp2, &p1, &p2, &p3, &p4, buffer);
		if (retcode)
		{
			ShowERR(retcode);
			return;
		}

		i++;
	    
		printf("Find Rockey4Smart: %08X\n", lp1);
	}
	printf("\n");

    //Operate on each Rockey4Smart
	for (j=0;j<i;j++)
	{   
	   
		p1 = 7;      //module number
		p2 = 0x2121; //module word
		p3 = 0;      //not allow decrease

		//Set module
		retcode = Rockey(RY_SET_MODULE, &handle[j], &lp1, &lp2, &p1, &p2, &p3, &p4, buffer);
		if (retcode)
		{
			ShowERR(retcode);
			return;
		}
		printf("Set Moudle 7: Pass = %04X Decrease no allow\n", p2);
	
		p1 = 0;
		strcpy((char*)buffer, cmd);
		//Write user's defined arithmetic to dongle
		retcode = Rockey(RY_WRITE_ARITHMETIC, &handle[j], &lp1, &lp2, &p1, &p2, &p3, &p4, buffer);
		if (retcode)
		{
			ShowERR(retcode);
			return;
		}
		printf("Write Arithmetic 1\n");
      
	 
		lp1 = 0;
		lp2 = 7;
		p1 = 1;
		p2 = 2;
		p3 = 3;
		p4 = 4;
		//Calculate,the result is decided by user's arithmetic wrote to dongle
		retcode = Rockey(RY_CALCULATE1, &handle[j], &lp1, &lp2, &p1, &p2, &p3, &p4, buffer);
		if (retcode)
		{
			ShowERR(retcode);
			return;
		}
		printf("Calculate Input: p1=1, p2=2, p3=3, p4=4\n");
		printf("Calculate Output: p1=%x, p2=%x, p3=%x, p4=%x\n", p1, p2, p3, p4);

		
		printf("\n");
        printf("Moudle 7 : %x\n", p3);
		truelp1=MAKELONG(p2,p1);
		printf("truelp1 : %x\n",truelp1);
		if (findlp1==truelp1)
           
			printf("Hello FeiTian!\n");
		else
			break;
	
		//Close Rockey4Smart handle
		retcode = Rockey(RY_CLOSE, &handle[j], &lp1, &lp2, &p1, &p2, &p3, &p4, buffer);
		if (retcode)
		{
			ShowERR(retcode);
			return;
		}

		printf("\n");
	}
}
