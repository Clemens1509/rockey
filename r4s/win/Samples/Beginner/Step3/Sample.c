/*************************************************************************
Show how to write data to user memory
*************************************************************************/
#include <windows.h>
#include <stdio.h>
#include "ry4s.h"		// Rockey4Smart Header File

void main()
{
	// ==============================================================
	WORD retcode;
	WORD handle, p1, p2, p3, p4;	// Rockey4Smart Variable
	DWORD lp1, lp2;					// Rockey4Smart Variable
	BYTE buffer[1024];				// Rockey4Smart Variable

	p1 = 0xc44c;	// Rockey4Smart Demo Password1
	p2 = 0xc8f8;	// Rockey4Smart Demo Password2
	p3 = 0;			// Program needn't Password3, Set to 0
	p4 = 0;			// Program needn't Password4, Set to 0

	// Try to find specified Rockey4Smart
	retcode = Rockey(RY_FIND, &handle, &lp1, &lp2, &p1, &p2, &p3, &p4, buffer);
	// Not found
	if (retcode)
	{
		printf("ROCKEY not found!\n");
		return;
	}
	
	//Open the Rockey4Smart dongle
	retcode = Rockey(RY_OPEN, &handle, &lp1, &lp2, &p1, &p2, &p3, &p4, buffer);
	if (retcode)	// Error	
	{	printf("Error Code: %d\n", retcode);		
		return;	
	}
    
	//Write to user data memory
	p1 = 0;	    // Memory Position
	p2 = 14;	// Data Length	
	strcpy((char*)buffer, "Hello FeiTian! ");	
	retcode = Rockey(RY_WRITE, &handle, &lp1, &lp2, &p1, &p2, &p3, &p4, buffer);
	//Error occurs
	if (retcode)	
	{	printf("Error Code: %d\n", retcode);
	    return;
	}        
		printf("Write: %s\n", buffer);

    //Close dongle handle
	retcode = Rockey(RY_CLOSE, &handle, &lp1, &lp2, &p1, &p2, &p3, &p4, buffer);
	if (retcode)
	{
		printf("Error Code: %d\n", retcode);
		return; 
	}

}
