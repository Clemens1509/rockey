/***************************************************************
Show how to operate on each of the dongles with the same password
***************************************************************/
#include <windows.h>
#include <stdio.h>
#include "ry4s.h"		// Include Rockey4Smart Header File


// Rockey Type
#define  TYPE_ROCKEY4			1
#define  TYPE_ROCKEY4P			2
#define  TYPE_ROCKEYUSB			3
#define  TYPE_ROCKEYUSBP		4
#define  TYPE_ROCKEYNET			5
#define  TYPE_ROCKEYUSBNET		6
#define  TYPE_ROCKEY4SMARTNET   10
#define  TYPE_ROCKEY4S          16

void main()
{
	int i, rynum;
	WORD retcode;
	WORD handle[16], p1, p2, p3, p4;	// Rockey4Smart Variable
	DWORD lp1, lp2=0;					// Rockey4Smart Variable
	BYTE buffer[1024];					// Rockey4Smart Variable

	p1 = 0xc44c;	// Rockey4Smart Demo Password1
	p2 = 0xc8f8;	// Rockey4Smart Demo Password2
	p3 = 0;			// Program needn't Password3, Set to 0
	p4 = 0;			// Program needn't Password4, Set to 0

	// Try to find all Rockey4Smart
	for (i=0;i<16;i++)
	{
		if (0 == i)
		{
			retcode = Rockey(RY_FIND, &handle[i], &lp1, &lp2, &p1, &p2, &p3, &p4, buffer);
			if (retcode == ERR_NOMORE) break;
		}
		else
		{
			// Notice : lp1 = Last found hardID
			retcode = Rockey(RY_FIND_NEXT, &handle[i], &lp1, &lp2, &p1, &p2, &p3, &p4, buffer);
			if (retcode == ERR_NOMORE) break;
		}
		// Error
		if (retcode)	
		{
			printf("Error Code: %d\n", retcode);
			return;
		}

		printf("Found Rockey4Smart: %08X  ", lp1);

		//Open Rockey4Smart
		retcode = Rockey(RY_OPEN, &handle[i], &lp1, &lp2, &p1, &p2, &p3, &p4, buffer);
		//Error
		if (retcode)
		{
			printf("Error Code: %d\n", retcode);
			return;
		}

		printf("(");
		switch(lp2)
		{
			case TYPE_ROCKEY4:
				printf("Rockey4 Standard Parallel Port");
				break;
			case TYPE_ROCKEY4P:
				printf("Rockey4 Plus Parallel Port");
				break;
			case TYPE_ROCKEYUSB:
				printf("Rockey4 Standard USB Port");
				break;
			case TYPE_ROCKEYUSBP:
				printf("Rockey4 Plus USB Port");
				break;
			case TYPE_ROCKEYNET:
				printf("Rockey4 Net Parallel Port");
				break;
			case TYPE_ROCKEYUSBNET:
				printf("Rockey4 Net USB Port");
				break;
			case TYPE_ROCKEY4S:
                 printf("Rockey4Smart USB Port");
				break;
			case TYPE_ROCKEY4SMARTNET:
				printf("Net Rockey4Smart USB Port");
				break;
			default:
				printf("Unknown Type");
		}
		printf(")\n");
	}
	printf("\n");

	rynum = i;

	// Do our work
	for (i=0;i<rynum;i++)
	{
		// Read Rockey4Smart user memory
		p1 = 0;		// Pos
		p2 = 10;	// Length
		buffer[10] = 0;
		retcode = Rockey(RY_READ, &handle[i], &lp1, &lp2, &p1, &p2, &p3, &p4, buffer);
		if (retcode)	// Error
		{
			printf("Error Code: %d\n", retcode);
			return;
		}
		printf("%s\n", buffer);		// Output
	}

	// Close all opened Rockey4Smart
	for (i=0;i<rynum;i++)
	{
		retcode = Rockey(RY_CLOSE, &handle[i], &lp1, &lp2, &p1, &p2, &p3, &p4, buffer);
		if (retcode)
		{
			printf("Error Code: %d\n", retcode);
			return;
		}
	}
}
