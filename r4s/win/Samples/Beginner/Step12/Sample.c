/***********************************************************
decrease module
***********************************************************/
#include <windows.h>
#include <stdio.h>
#include "ry4s.h"	// Include Rockey4Smart Header File

void main()
{
	// ==============================================================
	WORD retcode;
	WORD handle, p1, p2, p3, p4;	// Rockey4Smart Variable
	DWORD lp1, lp2;					// Rockey4Smart Variable
	BYTE buffer[1024];				// Rockey4Smart Variable

	p1 = 0xc44c;	// Rockey4Smart Demo Password1
	p2 = 0xc8f8;	// Rockey4Smart Demo Password2
        p3 = 0;		// Program needn't Password3, Set to 0
	p4 = 0;		// Program needn't Password4, Set to 0

	// Try to find specified Rockey4Smart
	retcode = Rockey(RY_FIND, &handle, &lp1, &lp2, &p1, &p2, &p3, &p4, buffer);
	//Not found
	if (retcode)	
	{
		printf("ROCKEY not found!\n");
		return;
	}
	
	//Open Rockey4Smart
	retcode = Rockey(RY_OPEN, &handle, &lp1, &lp2, &p1, &p2, &p3, &p4, buffer);
	//Error
	if (retcode)		
	{	printf("Error Code: %d\n", retcode);		
		return;	
	}
    

    p1 = 0;	
	//Check module property
	retcode = Rockey(RY_CHECK_MODULE, &handle, &lp1, &lp2, &p1, &p2, &p3, &p4, buffer);
	//Error
	if (retcode) 
	{		
		printf("Error Code: %d\n", retcode);	
		return;	
	}	

   	//This module is invalid
	if (p2!=1)
	{    
		printf("Update Please!\n");
		return;
	} 
	
	//This module can decrease
	if(p3==1)
	{     
		p1=0;    
		//Decreasespecified module
		retcode = Rockey(RY_DECREASE, &handle, &lp1, &lp2, &p1, &p2, &p3, &p4, buffer);   
		if(retcode)         
		{         
			printf("Error Code: %d\n", retcode);
			return;	
		}
	
	}
    
	// ==============================================================
	printf("Hello FeiTian!\n");

	//Close Rockey4Smart handle
	retcode = Rockey(RY_CLOSE, &handle, &lp1, &lp2, &p1, &p2, &p3, &p4, buffer);
	if (retcode)
	{
		printf("Error Code: %d\n", retcode);
		return;
	}
	
	
}
