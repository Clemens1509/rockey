/***************************************************************************
Show how to use different part of user memory to realize controling of different
application programs. In this way,you can use dongles with the same password to
control different programs.
***************************************************************************/
#include <windows.h>
#include <stdio.h>
#include <conio.h>
#include "ry4s.h"

void ShowERR(WORD retcode)
{
	if (retcode == 0) return;
	printf("Error Code: %d\n", retcode);
}

void main()
{
	WORD handle[16], p1, p2, p3, p4, retcode;
	WORD handleEnd;
	DWORD lp1, lp2;
	BYTE buffer[1024];
	
	int i, j;  
    
	p1 = 0xc44c;
	p2 = 0xc8f8;
	p3 = 0x0799;
	p4 = 0xc43b;


    //Find firt Rockey4Smart with specified password
	retcode = Rockey(RY_FIND, &handle[0], &lp1, &lp2, &p1, &p2, &p3, &p4, buffer);
	if (retcode)
	{
		ShowERR(retcode);
		return;
	}
	printf("Find Rockey4Smart: %08X\n", lp1);
	//Open Rockey4Smart
	retcode = Rockey(RY_OPEN, &handle[0], &lp1, &lp2, &p1, &p2, &p3, &p4, buffer);
	if (retcode)
	{
		ShowERR(retcode);
		return;
	}

	i = 1;
	while (retcode == 0)
	{
		//Find next Rockey4Smart with the same pass
		retcode = Rockey(RY_FIND_NEXT, &handle[i], &lp1, &lp2, &p1, &p2, &p3, &p4, buffer);
		if (retcode == ERR_NOMORE) break;
		if (retcode)
		{
			ShowERR(retcode);
			return;
		}

		retcode = Rockey(RY_OPEN, &handle[i], &lp1, &lp2, &p1, &p2, &p3, &p4, buffer);
		if (retcode)
		{
			ShowERR(retcode);
			return;
		}

		i++;
		printf("Find Rockey4Smart: %08X\n", lp1);
	}
	printf("\n");

	//Operate on each Rockey4Smart
	for (j=0;j<i;j++)
	{
		p1 = 0; //starting position for writing
		p2 = 5; //data length
		strcpy((char*)buffer, "Ver10");
		//Write data to user memory
		retcode = Rockey(RY_WRITE, &handle[j], &lp1, &lp2, &p1, &p2, &p3, &p4, buffer);
		if (retcode)
		{
			ShowERR(retcode);
			return;
		}
		printf("Write:%s\n",buffer);
		
		p1 = 0;
		p2 = 5;
		memset(buffer, 0, 64);
		//Read data from user memory
		retcode = Rockey(RY_READ, &handle[j], &lp1, &lp2, &p1, &p2, &p3, &p4, buffer);
		if (retcode)
		{
			ShowERR(retcode);
			return;
		}
		printf("Read: %s\n", buffer);
           	
		if (!strcmp(buffer,"Ver10")) 
		{
			handleEnd=handle[j];
			break;
		}
		
	}


    //Get a random number from Rockey4Smart
	retcode = Rockey(RY_RANDOM, &handleEnd, &lp1, &lp2, &p1, &p2, &p3, &p4, buffer);
	if (retcode)
	{
		ShowERR(retcode);
		return;
	}
	printf("Random: %04X\n", p1);

	lp2 = 0x12345678;
	//Get return code from seed code
	retcode = Rockey(RY_SEED, &handleEnd, &lp1, &lp2, &p1, &p2, &p3, &p4, buffer);
	if (retcode)
	{
		ShowERR(retcode);
		return;
	}
	printf("Return code of seed: %04X %04X %04X %04X\n", p1, p2, p3, p4);

    //Close Rockey4Smart handle
	retcode = Rockey(RY_CLOSE, &handleEnd, &lp1, &lp2, &p1, &p2, &p3, &p4, buffer);
	if (retcode)
	{
		ShowERR(retcode);
		return;
	}

	printf("\n");

}
