/*******************************************************************
Varify return code of seed code(0x12345678)
*******************************************************************/
#include <windows.h>
#include <stdio.h>
#include "ry4s.h"		// Include Rockey4Smart Header File

void main()
{
	// ==============================================================
	WORD retcode;
	WORD handle, p1, p2, p3, p4;	// Rockey4Smart Variable
	DWORD lp1, lp2;					// Rockey4Smart Variable
	BYTE buffer[1024];				// Rockey4Smart Variable

	p1 = 0xc44c;	// Rockey4Smart Demo Password1
	p2 = 0xc8f8;	// Rockey4Smart Demo Password2
	p3 = 0;			// Program needn't Password3, Set to 0
	p4 = 0;			// Program needn't Password4, Set to 0


	// Try to find specified Rockey4Smart
	retcode = Rockey(RY_FIND, &handle, &lp1, &lp2, &p1, &p2, &p3, &p4, buffer);
	//Not found
	if (retcode)	
	{
		printf("ROCKEY not found!\n");
		return;
	}
	
	//Open Rockey4Smart
	retcode = Rockey(RY_OPEN, &handle, &lp1, &lp2, &p1, &p2, &p3, &p4, buffer);
	//Error
	if (retcode)		
	{	printf("Error Code: %d\n", retcode);		
		return;	
	}
    
    //seed value
	lp2 = 0x12345678;	
	//get return code of seed
	retcode = Rockey(RY_SEED, &handle, &lp1, &lp2, &p1, &p2, &p3, &p4, buffer);
	//Error
	if (retcode)
	{		
		printf("Error Code: %d\n", retcode);
		return;	
    }
	
	if (p1==0xD03A && p2==0x94D6 && p3==0x96A9 && p4==0x7F54)
		
		printf("Hello FeiTian!\n");
	else
	{
		printf("Hello error!\n");
		return;
	}	

	//Close dongle handle
	retcode = Rockey(RY_CLOSE, &handle, &lp1, &lp2, &p1, &p2, &p3, &p4, buffer);
	if (retcode)
	{
		printf("Error Code: %d\n", retcode);
		return;
	}
	
	
}
