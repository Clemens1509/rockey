/********************************************************************
Verify User ID. If user ID is 0x88888888 output 'Hello FeiTian!' or output 'Hello DEMO!'
********************************************************************/
#include <windows.h>
#include <stdio.h>
#include "ry4s.h"	// Include Rockey4Smart Header File

void main()
{
	// ==============================================================
	WORD retcode;
	WORD handle, p1, p2, p3, p4;	// Rockey4Smart Variable
	DWORD lp1, lp2;					// Rockey4Smart Variable
	BYTE buffer[1024];				// Rockey4Smart Variable

	p1 = 0xc44c;	// Rockey4Smart Demo Password1
	p2 = 0xc8f8;	// Rockey4Smart Demo Password2
	p3 = 0;		// Program needn't Password3, Set to 0
	p4 = 0;		// Program needn't Password4, Set to 0

	// Try to find specified Rockey4Smart
	retcode = Rockey(RY_FIND, &handle, &lp1, &lp2, &p1, &p2, &p3, &p4, buffer);
	if (retcode)	// Not found
	{
		printf("ROCKEY not found!\n");
		return;
	}
	
	//Open Rockey4Smart
	retcode = Rockey(RY_OPEN, &handle, &lp1, &lp2, &p1, &p2, &p3, &p4, buffer);
	//Error
	if (retcode)		
	{	printf("Error Code: %d\n", retcode);		
		return;	
	}
    

    lp1 = 0;
	//Read User ID
	retcode = Rockey(RY_READ_USERID, &handle, &lp1, &lp2, &p1, &p2, &p3, &p4, buffer);
	//Error
	if (retcode) 
	{		
		printf("Error Code: %d\n", retcode);	
		return;	
	}	


	if (lp1==0x88888888)   
		
		printf("Hello FeiTian!\n");       
	else      
	{
		printf("Hello DEMO!\n");
		return;
	}	

    //Close Rockey4Smart
	retcode = Rockey(RY_CLOSE, &handle, &lp1, &lp2, &p1, &p2, &p3, &p4, buffer);
	if (retcode)
	{
		printf("Error Code: %d\n", retcode);
		return;
	}
	
	
}
