/*******************************************************************
Show how to Call finding Rockey4Smart function
********************************************************************/
#include <windows.h>
#include <stdio.h>
#include "ry4s.h"		// Include Rockey4Smart Header File

void main()
{
	// ==============================================================
	WORD retcode;
	WORD handle, p1, p2, p3, p4;	// Rockey4Smart Variable
	DWORD lp1, lp2;					// Rockey4Smart Variable
	BYTE buffer[1024];				// Rockey4Smart Variable

	p1 = 0xc44c;	// Rockey4Smart Demo Password1
	p2 = 0xc8f8;	// Rockey4Smart Demo Password2
	p3 = 0;			// Program needn't Password3, Set to 0
	p4 = 0;			// Program needn't Password4, Set to 0

	// Try to find specified Rockey4Smart
	retcode = Rockey(RY_FIND, &handle, &lp1, &lp2, &p1, &p2, &p3, &p4, buffer);
	if (retcode)	// Not found
	{
		printf("ROCKEY not found!\n");
		return;
	}

	// ==============================================================

	printf("Hello World!\n");
}
