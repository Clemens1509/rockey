#include <windows.h>
#include <stdio.h>
#include <conio.h>
#include "ry4s.h"

void ShowERR(WORD retcode)
{
	if (retcode == 0) return;
	printf("Error Code: %d\n", retcode);
}

void main()
{
	WORD handle[16], p1, p2, p3, p4, retcode;
		
	DWORD lp1, lp2;
	BYTE buffer[1024];
    
	int i, j;
  
	char cmd2[] = "A=E|E,B=F|F,C=G|G,D=H|H";


	p1 = 0xc44c;
	p2 = 0xc8f8;
	p3 = 0x0799;
	p4 = 0xc43b;
//	p1 = 0x6bfa;
//	p2 = 0xc3fc;
//	p3 = 0x7c47;
//	p4 = 0x5c4c;

	//Find Rockey4Smart with specified password
	retcode = Rockey(RY_FIND, &handle[0], &lp1, &lp2, &p1, &p2, &p3, &p4, buffer);
	if (retcode)
	{
		ShowERR(retcode);
		return;
	}
	printf("Find Rockey4Smart: %08X\n", lp1);

	//Open Rockey4Smart
	retcode = Rockey(RY_OPEN, &handle[0], &lp1, &lp2, &p1, &p2, &p3, &p4, buffer);
	if (retcode)
	{
		ShowERR(retcode);
		return;
	}

	i = 1;
	while (retcode == 0)
	{
		//Find next Rockey4Smart with the same password
		retcode = Rockey(RY_FIND_NEXT, &handle[i], &lp1, &lp2, &p1, &p2, &p3, &p4, buffer);
		if (retcode == ERR_NOMORE) break;
		if (retcode)
		{
			ShowERR(retcode);
			return;
		}

		retcode = Rockey(RY_OPEN, &handle[i], &lp1, &lp2, &p1, &p2, &p3, &p4, buffer);
		if (retcode)
		{
			ShowERR(retcode);
			return;
		}

		i++;
	    
		printf("Find Rockey4Smart: %08X\n", lp1);
	}
	printf("\n");

	//Operate on each dongle
	for (j=0;j<i;j++)
	{   

		p1 = 0;//module number
		p2 = 1;//module word
		p3 = 0;//not allow decrease
		//Set module 0
		retcode = Rockey(RY_SET_MODULE, &handle[j], &lp1, &lp2, &p1, &p2, &p3, &p4, buffer);
		if (retcode)
		{
			ShowERR(retcode);
			return;
		}
		printf("Set Moudle 0: Pass = %04X, Decrease not allow\n", p2);

		p1 = 1;
		p2 = 2;
		p3 = 0;
		//Set module 1
		retcode = Rockey(RY_SET_MODULE, &handle[j], &lp1, &lp2, &p1, &p2, &p3, &p4, buffer);
		if (retcode)
		{
			ShowERR(retcode);
			return;
		}
		printf("Set Moudle 1: Pass = %04X, Decrease not allow\n", p2);
		p1 = 2;
		p2 = 3;
		p3 = 0;
		//Set module 2
		retcode = Rockey(RY_SET_MODULE, &handle[j], &lp1, &lp2, &p1, &p2, &p3, &p4, buffer);
		if (retcode)
		{
			ShowERR(retcode);
			return;
		}
		printf("Set Moudle 2: Pass = %04X Decrease not allow\n", p2);
		p1 = 3;
		p2 = 4;
		p3 = 0;
		//Set module 3
		retcode = Rockey(RY_SET_MODULE, &handle[j], &lp1, &lp2, &p1, &p2, &p3, &p4, buffer);
		if (retcode)
		{
			ShowERR(retcode);
			return;
		}
		printf("Set Moudle 3: Pass = %04X Decrease not allow\n", p2);
        
		
		p1 = 0;
		strcpy((char*)buffer, cmd2);
		//Write user's defined arithmetic
		retcode = Rockey(RY_WRITE_ARITHMETIC, &handle[j], &lp1, &lp2, &p1, &p2, &p3, &p4, buffer);
		if (retcode)
		{
			ShowERR(retcode);
			return;
		}
		printf("Write Arithmetic\n");

		lp1 = 0;
		lp2 = 0;
		p1 = 0;
		p2 = 0;
		p3 = 0;
		p4 = 0;
		//Calculate, the result is decided by user's arithmetic wrote to dongle
		retcode = Rockey(RY_CALCULATE3, &handle[j], &lp1, &lp2, &p1, &p2, &p3, &p4, buffer);
		if (retcode)
		{
			ShowERR(retcode);
			return;
		}
		printf("Calculate3 Input: p1=0, p2=0, p3=0, p4=0\n");
				
		printf("\n");
		printf("Moudle 0: 0x%x\n",p1);
		printf("Moudle 1: 0x%x\n",p2);
		printf("Moudle 2: 0x%x\n",p3);
		printf("Moudle 3: 0x%x\n",p4);
        
		//Close Rockey4Smart handle
		retcode = Rockey(RY_CLOSE, &handle[j], &lp1, &lp2, &p1, &p2, &p3, &p4, buffer);
		if (retcode)
		{
			ShowERR(retcode);
			return;
		}

		printf("\n");
	}
}
