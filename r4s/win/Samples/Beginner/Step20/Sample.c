/*********************************************************************
Control application module running by setting dongle's module
*********************************************************************/

#include <windows.h>
#include <stdio.h>
#include <conio.h>
#include "ry4s.h"

void ShowERR(WORD retcode)
{
	if (retcode == 0) return;
	printf("Error Code: %d\n", retcode);
}

void main()
{
	WORD handle[16], p1, p2, p3, p4, retcode;
	DWORD lp1, lp2;
	BYTE buffer[1024];
	
	int i, j;
    
	p1 = 0xc44c;
	p2 = 0xc8f8;
	p3 = 0x0799;
	p4 = 0xc43b;


	//Find Rockey4Smart with specified password
	retcode = Rockey(RY_FIND, &handle[0], &lp1, &lp2, &p1, &p2, &p3, &p4, buffer);
	if (retcode)
	{
		ShowERR(retcode);
		return;
	}
	printf("Find Rockey4Smart: %08X\n", lp1);
	
	//Open Rockey4Smart
	retcode = Rockey(RY_OPEN, &handle[0], &lp1, &lp2, &p1, &p2, &p3, &p4, buffer);
	if (retcode)
	{
		ShowERR(retcode);
		return;
	}

	i = 1;
	while (retcode == 0)
	{
		//Find next Rockey4Smart with the same password
		retcode = Rockey(RY_FIND_NEXT, &handle[i], &lp1, &lp2, &p1, &p2, &p3, &p4, buffer);
		if (retcode == ERR_NOMORE) break;
		if (retcode)
		{
			ShowERR(retcode);
			return;
		}

		retcode = Rockey(RY_OPEN, &handle[i], &lp1, &lp2, &p1, &p2, &p3, &p4, buffer);
		if (retcode)
		{
			ShowERR(retcode);
			return;
		}

		i++;
		printf("Find Rockey4Smart: %08X\n", lp1);
	}
	printf("\n");

	//Operate on each Rockey4Smart
	for (j=0;j<i;j++)
	{
	    p1 = 0;      //module number
		p2 = 0x2121; //module word
		p3 = 0;      //not allow decrease
		//Set module
		retcode = Rockey(RY_SET_MODULE, &handle[j], &lp1, &lp2, &p1, &p2, &p3, &p4, buffer);
		if (retcode)
		{
			ShowERR(retcode);
			return;
		}
		printf("Set Moudle 0: Module word= %04X, Decrease not allow\n", p2);

		p1 = 0;
		//Check module
		retcode = Rockey(RY_CHECK_MODULE, &handle[j], &lp1, &lp2, &p1, &p2, &p3, &p4, buffer);
		if (retcode)
		{
			ShowERR(retcode);
			return;
		}
		printf("Check Moudle 0: ");
		if (p2) 
			printf("Run module 1!\n");
		else 
		{
			
			break;
		}
		
    	printf("\n");
		
		
		p1 = 8;       //module number
		p2 = 0xFFFF;  //module word
		p3 = 0;       //not allow decrease
		retcode = Rockey(RY_SET_MODULE, &handle[j], &lp1, &lp2, &p1, &p2, &p3, &p4, buffer);
		if (retcode)
		{
			ShowERR(retcode);
			return;
		}
		printf("Set Moudle 8: Pass = %04X, Decrease not allow\n", p2);

	
		p1 = 8;
		retcode = Rockey(RY_CHECK_MODULE, &handle[j], &lp1, &lp2, &p1, &p2, &p3, &p4, buffer);
		if (retcode)
		{
			ShowERR(retcode);
			return;
		}
		printf("Check Moudle 8: ");
		if (p2) 
			printf("Run module 2!\n");
		else
		{
			break;
		}
		retcode = Rockey(RY_CLOSE, &handle[j], &lp1, &lp2, &p1, &p2, &p3, &p4, buffer);
		if (retcode)
		{
			ShowERR(retcode);
			return;
		}

		printf("\n");

	}

}
