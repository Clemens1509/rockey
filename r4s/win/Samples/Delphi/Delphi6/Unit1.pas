unit Unit1;

interface

uses
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs,
  StdCtrls,RY4S;

type
  TForm1 = class(TForm)
    Button2: TButton;
    List: TListBox;
    procedure Button1Click(Sender: TObject);
    procedure Button2Click(Sender: TObject);
  private
    { Private declarations }
  public
    { Public declarations }
  end;

procedure SetN(buffer:pChar);
procedure SetD(buffer:pChar);

var
  Form1: TForm1;
  mBuf:array[0..1024] of Byte;
  mFun,mP1,mP2,mP3,mP4,rt:Word;
  lP1,lP2:LongWord;
  mHand:array[0..16] of Word;
  mHardID:array[0..16] of LongWord;
  rc:array[0..4] of Word;

implementation

{$R *.DFM}

procedure TForm1.Button1Click(Sender: TObject);
var
    i,num:Integer;
    str:string;
    strtemp:string;
    mRockeyNumber:Word;
    tmpBuf:array[0..1024] of Byte;
    DesKeyBuf:array[0..15] of byte;
    cStr:array[0..1024] of Char;
begin
R4S_Init();
    List.Items.Clear();
    mP1:=$c44c;
    mP2:=$c8f8;
    mP3:=$799;
    mP4:=$c43b;
    for i:=0 to 30 do
       mBuf[i]:=0;
    rt:=0;
    mFun:=1;
    rt:= Rockey(mFun,mHand[0],lP1,lP2,mP1,mP2,mP3,mP4,@mBuf);
    if rt<>0 then
    begin
        FmtStr(str,'Can not find ROCKEY,error:%x ',[rt]);
        List.Items.Add(str);
        exit;
    end;
    List.Items.Add('Find the first ROCKEY');

    mHardID[0]:=lp1;
    mFun:=RY_OPEN;
    rt:= Rockey(mFun,mHand[0],lP1,lP2,mP1,mP2,mP3,mP4,@mBuf);
    if rt<>0 then
    begin
        FmtStr(str,'Can not open ROCKEY,error:%x ',[rt]);
        List.Items.Add(str);
        exit;
    end;
    List.Items.Add('Open ROCKEY');

    i:=1;
    while rt = 0 do
    begin
        mFun:=RY_FIND_NEXT;
        rt:= Rockey(mFun,mHand[i],lP1,lP2,mP1,mP2,mP3,mP4,@mBuf);
        if rt<>0 then
           break;

        mHardID[i]:=lP1;
        FmtStr(str,'Find the %xROCKEY',[i+1]);
        List.Items.Add(str);

        mFun:=RY_OPEN;
        rt:= Rockey(mFun,mHand[i],lP1,lP2,mP1,mP2,mP3,mP4,@mBuf);
        if rt<>0 then
        begin
            FmtStr(str,'Can not open ROCKEY%x',[i+1]);
            List.Items.Add(str);
            break;
        end;
        FmtStr(str,'Open ROCKEY%x',[i+1]);
        List.Items.Add(str);
        i:=i+1;
    end;
    mRockeyNumber:=i;
for num:=0 to mRockeyNumber-1 do
begin
  List.Items.Add('*******************************************');
  FmtStr(str,'Begin to test the %x ROCKEY',[num+1]);
  List.Items.Add(str);
{Write}
  mFun:=RY_WRITE;
  for i:=0 to 500 do
      mBuf[i]:=31;
  mBuf[499]:=0;
  mP1:=0;
  mP2:=10;
   rt:= Rockey(mFun,mHand[num],lP1,lP2,mP1,mP2,mP3,mP4,@mBuf);
   if rt<>0 then
   begin
        FmtStr(str,'Write ROCKEY Error %x',[rt]);
        List.Items.Add(str);
        break;
   end;
   FmtStr(str,'Write ROCKEY Success',[i+1]);
   List.Items.Add('Write ROCKEY Success');
{Read}
  mFun:=RY_READ;
  for i:=0 to 500 do
      tmpBuf[i]:=0;
  mP1:=0;
  mP2:=500;
   rt:= Rockey(mFun,mHand[num],lP1,lP2,mP1,mP2,mP3,mP4,@tmpBuf);
   if rt<>0 then
   begin
        FmtStr(str,'Read ROCKEY Error',[rt]);
        List.Items.Add(str);
        break;
   end
   else
   begin
      for i:=0 to 10 do
          if mBuf[i] <> tmpBuf[i] then
              break;
      if i = 11 then
         List.Items.Add('Read ROCKEY Success')
      else
          List.Items.Add('Read ROCKEY Success,But have some differences from writed');
   end;
{Random}
  mFun:=RY_RANDOM;
   rt:= Rockey(mFun,mHand[num],lP1,lP2,mP1,mP2,mP3,mP4,@mBuf);
   if rt<>0 then
   begin
        FmtStr(str,'Detect Random number Error %x',[rt]);
        List.Items.Add(str);
        break;
   end;
   FmtStr(str,'Random Number%x:',[mP1]);
   List.Items.Add(str);
{Seed}
  mFun:=RY_SEED;
  lP2:=$12345678;
   rt:= Rockey(mFun,mHand[num],lP1,lP2,mP1,mP2,mP3,mP4,@mBuf);
   if rt<>0 then
   begin
        FmtStr(str,'Seed key Error %x',[rt]);
        List.Items.Add(str);
        break;
   end;
   FmtStr(str,'Seed%x:Return value:%x,%x,%x,%x',[lP2,mP1,mP2,mP3,mP4]);
   List.Items.Add(str);
   rc[0] := mP1; rc[1] := mP2;
   rc[2] := mP3; rc[3] := mP4;
{Write User ID}
  mFun:=RY_WRITE_USERID;
  lP1:=100;
   rt:= Rockey(mFun,mHand[num],lP1,lP2,mP1,mP2,mP3,mP4,@mBuf);
   if rt<>0 then
   begin
        FmtStr(str,'Write User ID Error %x',[rt]);
        List.Items.Add(str);
        break;
   end;
   FmtStr(str,'Write USER ID%x',[lP1]);
   List.Items.Add(str);
{Read User ID}
  mFun:=RY_READ_USERID;
  lP1:=0;
   rt:= Rockey(mFun,mHand[num],lP1,lP2,mP1,mP2,mP3,mP4,@mBuf);
   if rt<>0 then
   begin
        FmtStr(str,'Read User ID Error %x',[rt]);
        List.Items.Add(str);
        break;
   end;
   FmtStr(str,'The User ID is %x',[lP1]);
   List.Items.Add(str);
{Set module}
  mFun:=RY_SET_MOUDLE;
  mP1:=7;
  mP2:=$2122;
  mP3:=1;
   rt:= Rockey(mFun,mHand[num],lP1,lP2,mP1,mP2,mP3,mP4,@mBuf);
   if rt<>0 then
   begin
        FmtStr(str,'Set Module %x got error %x',[mP1,rt]);
        List.Items.Add(str);
        break;
   end;
   FmtStr(str,'Set module %x,value is %x,can be decrease',[mP1,mP2]);
   List.Items.Add(str);
{First check module}
  mFun:=RY_CHECK_MOUDLE;
  mP1:=7;
   rt:= Rockey(mFun,mHand[num],lP1,lP2,mP1,mP2,mP3,mP4,@mBuf);
   if rt<>0 then
   begin
        FmtStr(str,'Check module %x error %x',[mP1,rt]);
        List.Items.Add(str);
        break;
   end;
   if mP1 = 1 then
      if mP2 = 1 then
            FmtStr(str,'Check module %x,valid,can be decrease',[mP1])
      else
            FmtStr(str,'Check module %x,invalid,can not be decrese',[mP1])
   else
     if mP2 = 1 then
            FmtStr(str,'Check module x,invalid,can not be decrese',[mP1])
      else
            FmtStr(str,'Check module %x,invalid,can not be decrese',[mP1]) ;
   List.Items.Add(str);

{Decrease}
 mFun:=RY_DECREASE;
  mP1:=7;
   rt:= Rockey(mFun,mHand[num],lP1,lP2,mP1,mP2,mP3,mP4,@mBuf);
   if rt<>0 then
   begin
        FmtStr(str,'Decrese module %x error %x',[mP1,rt]);
        List.Items.Add(str);
        break;
   end;
   FmtStr(str,'Decrese module %x,is 0X2121',[mP1]);
   List.Items.Add(str);


 {Write Arithmetic 1}
  cstr:='H=H^H, A=A*23, F=B*17, A=A+F, A=A+G, A=A<C, A=A^D, B=B^B, C=C^C, D=D^D';
  for i:=0 to strlen(cstr) do
      mBuf[i]:= Byte(cstr[i]);
  mBuf[i]:=0;
  mFun:=RY_WRITE_ARITHMETIC;
  mP1:=0;
   rt:= Rockey(mFun,mHand[num],lP1,lP2,mP1,mP2,mP3,mP4,@mBuf);
   if rt<>0 then
   begin
        FmtStr(str,'Write Arithmetic 1 error %x',[rt]);
        List.Items.Add(str);
        break;
   end;
  List.Items.Add('Write Arithmetic 1 Success');
{Calculate 1}
  mFun:=RY_CALCULATE1;
  lP1 := 0; lP2 := 7;
  mP1 := 5; mP2 := 3;
  mP3 := 1; mP4 := $ffff;
  rt:= Rockey(mFun,mHand[num],lP1,lP2,mP1,mP2,mP3,mP4,@mBuf);
   if rt<>0 then
   begin
        FmtStr(str,'Calculate Arithmetic 1 error %x',[rt]);
        List.Items.Add(str);
        break;
   end;
  List.Items.Add('Calculate input: p1=5, p2=3, p3=1, p4=0xffff');
  List.Items.Add('Result: = ((5*23 + 3*17 + 0x2121) < 1) ^ 0xffff = BC71');
  FmtStr(str,'Calculate output: p1=%x, p2=%x, p3=%x, p4=%x',[mP1,mP2,mP3,mP4]);
  List.Items.Add(str);

{Write Arithmetic 2}
  cstr:='A=A+B, A=A+C, A=A+D, A=A+E, A=A+F, A=A+G, A=A+H';
  for i:=0 to strlen(cstr) do
      mBuf[i]:= Byte(cstr[i]);
  mBuf[i]:=0;
  mFun:=RY_WRITE_ARITHMETIC;
  mP1:=10;
   rt:= Rockey(mFun,mHand[num],lP1,lP2,mP1,mP2,mP3,mP4,@mBuf);
   if rt<>0 then
   begin
        FmtStr(str,'Write Arithmetic 2 error %x',[rt]);
        List.Items.Add(str);
        break;
   end;
  
  List.Items.Add('Write Arithmetic 2 success');
{Calculate 2}
  mFun:=RY_CALCULATE2;
  lP1 := 10; lP2 := $12345678;
  mP1 := 1; mP2 := 2;
  mP3 := 3; mP4 := 4;
  rt:= Rockey(mFun,mHand[num],lP1,lP2,mP1,mP2,mP3,mP4,@mBuf);
   if rt<>0 then
   begin
        FmtStr(str,'Calculate Arithmetic 2 error %x',[rt]);
        List.Items.Add(str);
        break;
   end;
  List.Items.Add('Calculate input: p1=1, p2=2, p3=3, p4=4');
  FmtStr(str,'Result: %04x + %04x + %04x + %04x + 1 + 2 + 3 + 4 = %04x',[rc[0], rc[1], rc[2], rc[3], Word(rc[0]+rc[1]+rc[2]+rc[3]+10)]);
  List.Items.Add(str);
  FmtStr(str,'Calculate output: p1=%x, p2=%x, p3=%x, p4=%x',[mP1,mP2,mP3,mP4]);
  List.Items.Add(str);
{Write Arithmetic 3}
  cstr:='A=E|E, B=F|F, C=G|G, D=H|H';
  for i:=0 to strlen(cstr) do
      mBuf[i]:= Byte(cstr[i]);
  mBuf[i]:=0;
  mFun:=RY_WRITE_ARITHMETIC;
  mP1:=17;
   rt:= Rockey(mFun,mHand[num],lP1,lP2,mP1,mP2,mP3,mP4,@mBuf);
   if rt<>0 then
   begin
        FmtStr(str,'Write arithmetic 3 error %x',[rt]);
        List.Items.Add(str);
        break;
   end;
  List.Items.Add('Write arithmetic 3 success');
  {Decrease}
 mFun:=RY_DECREASE;
  mP1:=7;
   rt:= Rockey(mFun,mHand[num],lP1,lP2,mP1,mP2,mP3,mP4,@mBuf);
   if rt<>0 then
   begin
        FmtStr(str,'Decrease module %x error %x',[mP1,rt]);
        List.Items.Add(str);
        break;
   end;
   FmtStr(str,'Decrese module %x,is 0X2120',[mP1]);
   List.Items.Add(str);

{Calcualte 3}
  mFun:=RY_CALCULATE3;
  lP1 := 17; lP2 := 6;
  mP1 := 1; mP2 := 2;
  mP3 := 3; mP4 := 4;
  rt:= Rockey(mFun,mHand[num],lP1,lP2,mP1,mP2,mP3,mP4,@mBuf);
   if rt<>0 then
   begin
        FmtStr(str,'Calculate arithmetic 3 error %x',[rt]);
        List.Items.Add(str);
        break;
   end;
  FmtStr(str,'Read the module from 6: %x,%x,%x,%x',[mP1,mP2,mP3,mP4]);
  List.Items.Add(str);

//add 2006/09/23
{Des test}
  mFun:=RY_SET_DES_KEY;
  mp1:=DES_SINGLE_MODE;
  for i:=0 to 15 do
    DesKeyBuf[i] := i;
  rt:= Rockey(mFun,mHand[num],lP1,lP2,mP1,mP2,mP3,mP4,@DesKeyBuf);
  if (rt <> 0) then
  begin
    FmtStr(str, 'Set des key error %d', [rt]);
    List.Items.Add(str);
    break;
  end;
  List.Items.Add('Set des key success');

  ZeroMemory(@mBuf[0], 1024);
  mFun:=RY_DES_ENC;
  mp2:=24;
  for i:=0 to 23 do
    mBuf[i] := byte('A');

  FmtStr(str, 'Source data: %x  ', [mBuf[0]]);
  for i:=1 to 23 do
  begin
    FmtStr(strtemp, '%2x  ', [mBuf[i]]);
    AppendStr(str, strtemp);
  end;
  List.Items.Add(str);

  rt:= Rockey(mFun,mHand[num],lP1,lP2,mP1,mP2,mP3,mP4,@mBuf);
  if (rt <> 0) then
  begin
    FmtStr(str, 'Des Enc error %d', [rt]);
    List.Items.Add(str);
    break;
  end;

  FmtStr(str, 'Des encrypt data: %x  ', [mBuf[0]]);
  for i:=1 to 31 do
  begin
    FmtStr(strtemp, '%02x  ', [mBuf[i]]);
    AppendStr(str, strtemp);
  end;
  List.Items.Add(str);

  mFun:=RY_DES_DEC;
  rt:= Rockey(mFun,mHand[num],lP1,lP2,mP1,mP2,mP3,mP4,@mBuf);
  if (rt <> 0) then
  begin
    FmtStr(str, 'Des Dec error %d', [rt]);
    List.Items.Add(str);
    break;
  end;
  
  FmtStr(str, 'Des decrypt data: %x  ', [mBuf[0]]);
  for i:=1 to 23 do
  begin
    FmtStr(strtemp, '%2x  ', [mBuf[i]]);
    AppendStr(str, strtemp);
  end;
  List.Items.Add(str);

{RSA test}
  ZeroMemory(@mBuf[0],1024);
  mFun:=RY_SET_RSAKEY_N;
  SetN(@char(mBuf[0]));
  rt:= Rockey(mFun,mHand[num],lP1,lP2,mP1,mP2,mP3,mP4,@mBuf);
  if (rt <> 0) then
  begin
    FmtStr(str, 'Set RSA key N error %d', [rt]);
    List.Items.Add(str);
    break;
  end;

  mFun:=RY_SET_RSAKEY_D;
  SetD(@char(mBuf[0]));
  rt:= Rockey(mFun,mHand[num],lP1,lP2,mP1,mP2,mP3,mP4,@mBuf);
  if (rt <> 0) then
  begin
    FmtStr(str, 'Set RSA key D error %d', [rt]);
    List.Items.Add(str);
    break;
  end;

  // set buf for rsa enc
  ZeroMemory(@mBuf[0], 1024);
  FillMemory(@mBuf[0], 120, 65); //'A'

  FmtStr(str, 'RSA source data: %x  ', [mBuf[0]]);
  for i:=1 to 127 do
  begin
    FmtStr(strtemp, '%2x  ', [mBuf[i]]);
    AppendStr(str, strtemp);
  end;
  List.Items.Add(str);

  mFun:=RY_RSA_ENC;
  mp2:=120;
  mp3:=RSA_ROCKEY_PADDING;

  mp1:=RSA_PUBLIC_KEY;
  rt:= Rockey(mFun,mHand[num],lP1,lP2,mP1,mP2,mP3,mP4,@mBuf);
  if (rt <> 0) then
  begin
    FmtStr(str, 'RSA enc error %d', [rt]);
    List.Items.Add(str);
    break;
  end;
  FmtStr(str, 'Data after enc: %x  ', [mBuf[0]]);
  for i:=1 to 127 do
  begin
    FmtStr(strtemp, '%2x  ', [mBuf[i]]);
    AppendStr(str, strtemp);
  end;
  List.Items.Add(str);

  mFun:=RY_RSA_DEC;
  mp1:=RSA_PRIVATE_KEY;
  rt:= Rockey(mFun,mHand[num],lP1,lP2,mP1,mP2,mP3,mP4,@mBuf);
  if (rt <> 0) then
  begin
    FmtStr(str, 'RSA dec error %d', [rt]);
    List.Items.Add(str);
    break;
  end;
  FmtStr(str, 'Data after dec: %x  ', [mBuf[0]]);
  for i:=1 to 127 do
  begin
    FmtStr(strtemp, '%2x  ', [mBuf[i]]);
    AppendStr(str, strtemp);
  end;
  List.Items.Add(str);;

  end;//end of circle

  for num:=0 to mRockeyNumber-1 do
  begin
  mHand[num]  :=10;
   rt:= Rockey(RY_CLOSE,mHand[num],lP1,lP2,mP1,mP2,mP3,mP4,@mBuf);
   begin
        List.Items.Add('Close Rockey');
        break;
   end;
   end;
   R4S_Finish();

end;

//procedure TMainForm.BitBtn1Click(Sender: TObject);
//begin
 //    close()
//end;

procedure TForm1.Button2Click(Sender: TObject);
begin
close()
end;

procedure SetN(buffer:pChar);
begin
        buffer[0]:=char(191);	buffer[1]:=char(25);		buffer[2]:=char(33);		buffer[3]:=char(32);
	buffer[4]:=char(213);	buffer[5]:=char(241);	buffer[6]:=char(45);		buffer[7]:=char(44);
	buffer[8]:=char(252);	buffer[9]:=char(169);	buffer[10]:=char(197);	buffer[11]:=char(176);
	buffer[12]:=char(59);	buffer[13]:=char(209);	buffer[14]:=char(241);	buffer[15]:=char(92);
	buffer[16]:=char(142);	buffer[17]:=char(136);	buffer[18]:=char(144);	buffer[19]:=char(40);
	buffer[20]:=char(170);	buffer[21]:=char(94);	buffer[22]:=char(147);	buffer[23]:=char(97);
	buffer[24]:=char(185);	buffer[25]:=char(135);	buffer[26]:=char(234);	buffer[27]:=char(134);
	buffer[28]:=char(143);	buffer[29]:=char(84);	buffer[30]:=char(152);	buffer[31]:=char(23);
	buffer[32]:=char(27);	buffer[33]:=char(72);	buffer[34]:=char(238);	buffer[35]:=char(245);
	buffer[36]:=char(2);		buffer[37]:=char(144);	buffer[38]:=char(91);	buffer[39]:=char(70);
	buffer[40]:=char(57);	buffer[41]:=char(20);	buffer[42]:=char(192);	buffer[43]:=char(98);
	buffer[44]:=char(77);	buffer[45]:=char(31);	buffer[46]:=char(111);	buffer[47]:=char(227);
	buffer[48]:=char(16);	buffer[49]:=char(115);	buffer[50]:=char(182);	buffer[51]:=char(211);
	buffer[52]:=char(95);	buffer[53]:=char(143);	buffer[54]:=char(108);	buffer[55]:=char(241);
	buffer[56]:=char(115);	buffer[57]:=char(2);		buffer[58]:=char(248);	buffer[59]:=char(62);
	buffer[60]:=char(121);	buffer[61]:=char(134);	buffer[62]:=char(55);	buffer[63]:=char(10);
	buffer[64]:=char(30);	buffer[65]:=char(74);	buffer[66]:=char(145);	buffer[67]:=char(132);
	buffer[68]:=char(79);	buffer[69]:=char(159);	buffer[70]:=char(156);	buffer[71]:=char(18);
	buffer[72]:=char(223);	buffer[73]:=char(175);	buffer[74]:=char(84);	buffer[75]:=char(197);
	buffer[76]:=char(130);	buffer[77]:=char(17);	buffer[78]:=char(95);	buffer[79]:=char(230);
	buffer[80]:=char(200);	buffer[81]:=char(202);	buffer[82]:=char(68);	buffer[83]:=char(202);
	buffer[84]:=char(132);	buffer[85]:=char(22);	buffer[86]:=char(150);	buffer[87]:=char(18);
	buffer[88]:=char(193);	buffer[89]:=char(127);	buffer[90]:=char(10);	buffer[91]:=char(42);
	buffer[92]:=char(158);	buffer[93]:=char(129);	buffer[94]:=char(185);	buffer[95]:=char(153);
	buffer[96]:=char(219);	buffer[97]:=char(226);	buffer[98]:=char(70);	buffer[99]:=char(56);
	buffer[100]:=char(100);	buffer[101]:=char(74);	buffer[102]:=char(149);	buffer[103]:=char(14);
	buffer[104]:=char(92);	buffer[105]:=char(14);	buffer[106]:=char(201);	buffer[107]:=char(106);
	buffer[108]:=char(93);	buffer[109]:=char(221);	buffer[110]:=char(96);	buffer[111]:=char(221);
	buffer[112]:=char(148);	buffer[113]:=char(233);	buffer[114]:=char(46);	buffer[115]:=char(75);
	buffer[116]:=char(195);	buffer[117]:=char(70);	buffer[118]:=char(77);	buffer[119]:=char(157);
	buffer[120]:=char(54);	buffer[121]:=char(99);	buffer[122]:=char(95);	buffer[123]:=char(233);
	buffer[124]:=char(8);	buffer[125]:=char(180);	buffer[126]:=char(4);	buffer[127]:=char(178);
end;

procedure SetD(buffer:pChar);
begin
  buffer[0]:=char(117);	buffer[1]:=char(86);		buffer[2]:=char(160);	buffer[3]:=char(104);
	buffer[4]:=char(137);	buffer[5]:=char(51);		buffer[6]:=char(8);		buffer[7]:=char(4);
	buffer[8]:=char(73);		buffer[9]:=char(178);	buffer[10]:=char(136);	buffer[11]:=char(200);
	buffer[12]:=char(145);	buffer[13]:=char(26);	buffer[14]:=char(66);	buffer[15]:=char(84);
	buffer[16]:=char(48);	buffer[17]:=char(143);	buffer[18]:=char(169);	buffer[19]:=char(74);
	buffer[20]:=char(251);	buffer[21]:=char(86);	buffer[22]:=char(25);	buffer[23]:=char(188);
	buffer[24]:=char(162);	buffer[25]:=char(3);		buffer[26]:=char(162);	buffer[27]:=char(131);
	buffer[28]:=char(107);	buffer[29]:=char(81);	buffer[30]:=char(3);		buffer[31]:=char(49);
	buffer[32]:=char(251);	buffer[33]:=char(133);	buffer[34]:=char(157);	buffer[35]:=char(164);
	buffer[36]:=char(147);	buffer[37]:=char(138);	buffer[38]:=char(72);	buffer[39]:=char(81);
	buffer[40]:=char(111);	buffer[41]:=char(137);	buffer[42]:=char(65);	buffer[43]:=char(48);
	buffer[44]:=char(154);	buffer[45]:=char(11);	buffer[46]:=char(42);	buffer[47]:=char(215);
	buffer[48]:=char(188);	buffer[49]:=char(180);	buffer[50]:=char(175);	buffer[51]:=char(54);
	buffer[52]:=char(79);	buffer[53]:=char(40);	buffer[54]:=char(163);	buffer[55]:=char(60);
	buffer[56]:=char(123);	buffer[57]:=char(20);	buffer[58]:=char(96);	buffer[59]:=char(196);
	buffer[60]:=char(188);	buffer[61]:=char(10);	buffer[62]:=char(254);	buffer[63]:=char(213);
	buffer[64]:=char(17);	buffer[65]:=char(244);	buffer[66]:=char(224);	buffer[67]:=char(61);
	buffer[68]:=char(197);	buffer[69]:=char(77);	buffer[70]:=char(6);		buffer[71]:=char(3);
	buffer[72]:=char(22);	buffer[73]:=char(32);	buffer[74]:=char(157);	buffer[75]:=char(117);
	buffer[76]:=char(92);	buffer[77]:=char(218);	buffer[78]:=char(114);	buffer[79]:=char(245);
	buffer[80]:=char(73);	buffer[81]:=char(10);	buffer[82]:=char(47);	buffer[83]:=char(221);
	buffer[84]:=char(35);	buffer[85]:=char(24);	buffer[86]:=char(123);	buffer[87]:=char(235);
	buffer[88]:=char(79);	buffer[89]:=char(237);	buffer[90]:=char(175);	buffer[91]:=char(153);
	buffer[92]:=char(209);	buffer[93]:=char(193);	buffer[94]:=char(156);	buffer[95]:=char(80);
	buffer[96]:=char(253);	buffer[97]:=char(255);	buffer[98]:=char(54);	buffer[99]:=char(169);
	buffer[100]:=char(111);	buffer[101]:=char(113);	buffer[102]:=char(196);	buffer[103]:=char(202);
	buffer[104]:=char(121);	buffer[105]:=char(77);	buffer[106]:=char(27);	buffer[107]:=char(100);
	buffer[108]:=char(179);	buffer[109]:=char(12);	buffer[110]:=char(211);	buffer[111]:=char(140);
	buffer[112]:=char(217);	buffer[113]:=char(44);	buffer[114]:=char(182);	buffer[115]:=char(71);
	buffer[116]:=char(208);	buffer[117]:=char(247);	buffer[118]:=char(92);	buffer[119]:=char(218);
	buffer[120]:=char(41);	buffer[121]:=char(105);	buffer[122]:=char(234);	buffer[123]:=char(216);
	buffer[124]:=char(85);	buffer[125]:=char(118);	buffer[126]:=char(54);	buffer[127]:=char(8);
end;

end.

