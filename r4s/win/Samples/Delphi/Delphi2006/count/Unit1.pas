unit Unit1;

interface

uses
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs,
  StdCtrls,RY4S;

type
  TForm1 = class(TForm)
    Button2: TButton;
    List: TListBox;
    procedure Button1Click(Sender: TObject);
    procedure Button2Click(Sender: TObject);
  private
    { Private declarations }
  public
    { Public declarations }
  end;



var
  Form1: TForm1;
  mBuf:array[0..1024] of Byte;
  mFun,mP1,mP2,mP3,mP4,rt:Word;
  lP1,lP2:LongWord;
  mHand:array[0..16] of Word;
  mHardID:array[0..16] of LongWord;
  rc:array[0..4] of Word;

implementation

{$R *.DFM}

procedure TForm1.Button1Click(Sender: TObject);
var
    i,num:Integer;
    sMsg:AnsiString;
    str:string;
    strtemp:string;
    mRockeyNumber:Word;
    tmpBuf:array[0..1024] of Byte;
    DesKeyBuf:array[0..15] of byte;
    cStr:array[0..1024] of Char;

    time1: TSystemTime;
    minute1: integer;
    minute2: integer;
    time_mode: integer;
    iCount : integer;
begin
R4S_Init();
    List.Items.Clear();
    mP1:=$c44c;
    mP2:=$c8f8;
    mP3:=$799;
    mP4:=$c43b;
    for i:=0 to 30 do
       mBuf[i]:=0;
    rt:=0;
    mFun:=RY_FIND;

    rt:= Rockey(mFun,mHand[0],lP1,lP2,mP1,mP2,mP3,mP4,@mBuf[0]);
    if rt<>0 then
    begin
        FmtStr(str,'Can not find ROCKEY,error:%x ',[rt]);
        List.Items.Add(str);
        exit;
    end;
    List.Items.Add('Find the first ROCKEY');

    mHardID[0]:=lp1;
    mFun:=RY_OPEN;
    rt:= Rockey(mFun,mHand[0],lP1,lP2,mP1,mP2,mP3,mP4,@mBuf[0]);
    if rt<>0 then
    begin
        FmtStr(str,'Can not open ROCKEY,error:%x ',[rt]);
        List.Items.Add(str);
        exit;
    end;
    List.Items.Add('Open ROCKEY');

    mFun := RY_VERSION;
    rt:= Rockey(mFun,mHand[0],lP1,lP2,mP1,mP2,mP3,mP4,@mBuf[0]);
    if rt<>0 then
    begin
        FmtStr(str,'Can not open ROCKEY,error:%x ',[rt]);
        List.Items.Add(str);
        exit;
    end;

    if lP2 < 259 then
     begin
      FmtStr(str,'Old dongle does not support new TIME and COUNT interfaces,please use V1.03 for test ,version=%x ',[lP2]);
        List.Items.Add(str);
        exit;
     end;


    i:=1;
    while rt = 0 do
    begin
        mFun:=RY_FIND_NEXT;
        rt:= Rockey(mFun,mHand[i],lP1,lP2,mP1,mP2,mP3,mP4,@mBuf[0]);
        if rt<>0 then
           break;

        mHardID[i]:=lP1;
        FmtStr(str,'Find the %xROCKEY',[i+1]);
        List.Items.Add(str);

        mFun:=RY_OPEN;
        rt:= Rockey(mFun,mHand[i],lP1,lP2,mP1,mP2,mP3,mP4,@mBuf[0]);
        if rt<>0 then
        begin
            FmtStr(str,'Can not open ROCKEY%x',[i+1]);
            List.Items.Add(str);
            break;
        end;
        FmtStr(str,'Open ROCKEY%x',[i+1]);
        List.Items.Add(str);
        i:=i+1;
    end;
    mRockeyNumber:=i;
for num:=0 to mRockeyNumber -1 do
begin
  List.Items.Add('*******************************************');
  FmtStr(str,'Start testing ROCKEY: %x',[num+1]);
  List.Items.Add(str);

  mP1 := 1;
  mP3 := 10; 
 //Set authorized count
 iCount := 10;
  mFun := RY_SET_COUNTER_EX;
  List.Items.Add('Set counter...');
  rt:= Rockey(mFun,mHand[i],lP1,lP2,mP1,mP2,mP3,mP4,@iCount);
  if rt <> ERR_SUCCESS then
  begin
    FmtStr(str,'Set counter error,error code: %d',[rt]);
    List.Items.Add(str);
    exit;
  end;
  List.Items.Add('Set counter successfully');
  FmtStr(str,'Set, Unit= %d ',[mP1]);
  List.Items.Add(str);
  FmtStr(str,'Set,count= %d ',[iCount]);
  List.Items.Add(str);
 // Get authorized count
  iCount:=0;
  mFun := RY_GET_COUNTER_EX;
  List.Items.Add('Get counter...');
  rt:= Rockey(mFun,mHand[i],lP1,lP2,mP1,mP2,mP3,mP4,@iCount);
  if rt <> ERR_SUCCESS then
  begin

    FmtStr(str,'Get counter error,error code: %d',[rt]);
    List.Items.Add(str);
    exit;
  end;
  List.Items.Add('Get counter successfully!');
  FmtStr(str,'Get,Unit= %d ',[mP1]);
  List.Items.Add(str);
  FmtStr(str,'Get,count= %d ',[iCount]);
  List.Items.Add(str);
end;
   R4S_Finish();

end;

//procedure TMainForm.BitBtn1Click(Sender: TObject);
//begin
 //    close()
//end;

procedure TForm1.Button2Click(Sender: TObject);
begin
close()
end;
end.

