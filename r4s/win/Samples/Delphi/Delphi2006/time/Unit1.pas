unit Unit1;

interface

uses
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs,
  StdCtrls,RY4S;

type
  TForm1 = class(TForm)
    Button2: TButton;
    List: TListBox;
    procedure Button1Click(Sender: TObject);
    procedure Button2Click(Sender: TObject);
  private
    { Private declarations }
  public
    { Public declarations }
  end;



var
  Form1: TForm1;
  mBuf:array[0..1024] of Byte;
  mFun,mP1,mP2,mP3,mP4,rt:Word;
  lP1,lP2:LongWord;
  mHand:array[0..16] of Word;
  mHardID:array[0..16] of LongWord;
  rc:array[0..4] of Word;

implementation

{$R *.DFM}

procedure TForm1.Button1Click(Sender: TObject);
var
    i,num:Integer;
    str:string;
    strtemp:string;
    mRockeyNumber:Word;
    tmpBuf:array[0..1024] of Byte;
    DesKeyBuf:array[0..15] of byte;
    cStr:array[0..1024] of Char;


    minute1: integer;
    minute2: integer;
    time_mode: integer;
    ihour: integer;
    
    time1: TSystemTime;
    time2: TSystemTime;
    time3: TSystemTime;
begin
R4S_Init();
    List.Items.Clear();
    mP1:=$c44c;
    mP2:=$c8f8;
    mP3:=$799;
    mP4:=$c43b;

    for i:=0 to 30 do
       mBuf[i]:=0;
    rt:=0;
    mFun:=1;
    rt:= Rockey(mFun,mHand[0],lP1,lP2,mP1,mP2,mP3,mP4,@mBuf[0]);
    if rt<>0 then
    begin
        FmtStr(str,'Can not find ROCKEY,error:%x ',[rt]);
        List.Items.Add(str);
        exit;
    end;
    List.Items.Add('Find the first ROCKEY');

    mHardID[0]:=lp1;
    mFun:=RY_OPEN;
    rt:= Rockey(mFun,mHand[0],lP1,lP2,mP1,mP2,mP3,mP4,@mBuf[0]);
    if rt<>0 then
    begin
        FmtStr(str,'Can not open ROCKEY,error:%x ',[rt]);
        List.Items.Add(str);
        exit;
    end;
    List.Items.Add('Open ROCKEY');

    mFun := RY_VERSION;
    rt:= Rockey(mFun,mHand[0],lP1,lP2,mP1,mP2,mP3,mP4,@mBuf[0]);
    if rt<>0 then
    begin
        FmtStr(str,'Can not open ROCKEY,error:%x ',[rt]);
        List.Items.Add(str);
        exit;
    end;

    if lP2 < 259 then
     begin
      FmtStr(str,'Old dongle does not support new TIME and COUNT interfaces,please use V1.03 dongle for test. current version=%x ',[lP2]);
        List.Items.Add(str);
        exit;
     end;

    i:=1;
    while rt = 0 do
    begin
        mFun:=RY_FIND_NEXT;
        rt:= Rockey(mFun,mHand[i],lP1,lP2,mP1,mP2,mP3,mP4,@mBuf[0]);
        if rt<>0 then
           break;

        mHardID[i]:=lP1;
        FmtStr(str,'Find the %xROCKEY',[i+1]);
        List.Items.Add(str);

        mFun:=RY_OPEN;
        rt:= Rockey(mFun,mHand[i],lP1,lP2,mP1,mP2,mP3,mP4,@mBuf[0]);
        if rt<>0 then
        begin
            FmtStr(str,'Can not open ROCKEY%x',[i+1]);
            List.Items.Add(str);
            break;
        end;
        FmtStr(str,'Open ROCKEY%x',[i+1]);
        List.Items.Add(str);
        i:=i+1;
    end;
    mRockeyNumber:=i;

for num:=0 to mRockeyNumber-1 do
begin
  List.Items.Add('*******************************************');
  FmtStr(str,'Begin to test the %x ROCKEY',[num+1]);
  List.Items.Add(str);


  GetSystemTime(time1); //Get system time (UTC)


  FmtStr(str,'Current system time(UTC) %d-%d-%d %d:%d:%d ',[time1.wYear, time1.wMonth, time1.wDay, time1.wHour, time1.wMinute, time1.wSecond]);
  List.Items.Add(str);

  GetSystemTime(time2);
  time2.wDay := time2.wDay + 2;

  //set authorized time (date)
  mP1 := 19;  //unit no
  mP3 := 1;  //1=date, 2=hour, 3=day

  mFun := RY_SET_TIMER_EX;
  List.Items.Add('Set authorized time(date)...');
  rt:= Rockey(mFun,mHand[i],lP1,lP2,mP1,mP2,mP3,mP4,@time2);
  if rt <> ERR_SUCCESS then
  begin
    FmtStr(str,'Set authorized time error,error code: 0x%.2x ',[rt]);
    List.Items.Add(str);
    exit;
  end;
  List.Items.Add('Set authorized time successfully');
  FmtStr(str,'Unit:%d  Time: %d-%d-%d %d:%d:%d ',[mP1,time2.wYear, time2.wMonth, time2.wDay, time2.wHour, time2.wMinute, time2.wSecond]);
  List.Items.Add(str);

  //Adjust time
  mFun := RY_ADJUST_TIMER_EX;
  List.Items.Add('Adjust time...');
  rt:= Rockey(mFun,mHand[i],lP1,lP2,mP1,mP2,mP3,mP4,@time1);

  if rt <> ERR_SUCCESS then
  begin
    FmtStr(str,'Adjust time error,error code: 0x%.2x ',[rt]);
    List.Items.Add(str);
    exit;
  end;
  List.Items.Add('Adjust time successfully!');

  mFun := RY_GET_TIMER_EX;
  List.Items.Add('Get authorized time...');
  rt:= Rockey(mFun,mHand[i],lP1,lP2,mP1,mP2,mP3,mP4,@time3);
  if rt <> ERR_SUCCESS then
  begin
    FmtStr(str,'Get time error,error code: 0x%.2x ',[rt]);
    List.Items.Add(str);
    exit;
  end;

  List.Items.Add('Get time successfully!');
  FmtStr(str,'Unit:%d  Time: %d-%d-%d %d:%d:%d ',[mP1,time3.wYear, time3.wMonth, time3.wDay, time3.wHour, time3.wMinute, time3.wSecond]);
  List.Items.Add(str);



  //Set authorized time(hour)
  mP1 := 10;
  mP3 := 2;
  mFun := RY_SET_TIMER_EX;
  ihour := 10;
  List.Items.Add('Set authorized time(hour)...');
  rt:= Rockey(mFun,mHand[i],lP1,lP2,mP1,mP2,mP3,mP4,@ihour);
  if rt <> ERR_SUCCESS then
  begin
    FmtStr(str,'Set authorized time error,error code: 0x%.2x ',[rt]);
    List.Items.Add(str);
    exit;
  end;
  List.Items.Add('Set authorized time successfully!');
  FmtStr(str,'Unit:%d, Time=%d ',[mP1,ihour]);
  List.Items.Add(str);

  mFun := RY_ADJUST_TIMER_EX;
  List.Items.Add('Adjust time...');
  rt:= Rockey(mFun,mHand[i],lP1,lP2,mP1,mP2,mP3,mP4,@time1);

  if rt <> ERR_SUCCESS then
  begin
    FmtStr(str,'Adjust time error,error code: 0x%.2x ',[rt]);
    List.Items.Add(str);
    exit;
  end;
  List.Items.Add('Adjust time successfully!');
  mFun := RY_GET_TIMER_EX;
  List.Items.Add('Get authorized time...');
  rt:= Rockey(mFun,mHand[i],lP1,lP2,mP1,mP2,mP3,mP4,@ihour);
  if rt <> ERR_SUCCESS then
  begin
   FmtStr(str,'Get time error,error code: 0x%.2x ',[rt]);
   List.Items.Add(str);
   exit;
  end;

  FmtStr(str,'Unit:%d,Time(hour)=%d ',[mP1,ihour]);
  List.Items.Add(str);

  end;
   R4S_Finish();

end;

procedure TForm1.Button2Click(Sender: TObject);
begin
close()
end;
end.

