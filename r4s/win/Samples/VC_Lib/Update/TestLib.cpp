// TestLib.cpp : Defines the entry point for the console application.
//

#include <Windows.h>
#include "ry4s.h"
#include <malloc.h>
#include <stdio.h>

//#pragma comment(lib,"Ry4s.lib")
#define BUF_SIZE 2048

void ShowERR(WORD retcode)
{
	if (retcode == 0) return;
	printf("Error Code: %d\n", retcode);
}

void SetN(BYTE* buffer)
{
	buffer[0]=(char)191;	buffer[1]=(char)25;		buffer[2]=(char)33;		buffer[3]=(char)32;
	buffer[4]=(char)213;	buffer[5]=(char)241;	buffer[6]=(char)45;		buffer[7]=(char)44;
	buffer[8]=(char)252;	buffer[9]=(char)169;	buffer[10]=(char)197;	buffer[11]=(char)176;
	buffer[12]=(char)59;	buffer[13]=(char)209;	buffer[14]=(char)241;	buffer[15]=(char)92;
	buffer[16]=(char)142;	buffer[17]=(char)136;	buffer[18]=(char)144;	buffer[19]=(char)40;
	buffer[20]=(char)170;	buffer[21]=(char)94;	buffer[22]=(char)147;	buffer[23]=(char)97;
	buffer[24]=(char)185;	buffer[25]=(char)135;	buffer[26]=(char)234;	buffer[27]=(char)134;
	buffer[28]=(char)143;	buffer[29]=(char)84;	buffer[30]=(char)152;	buffer[31]=(char)23;
	buffer[32]=(char)27;	buffer[33]=(char)72;	buffer[34]=(char)238;	buffer[35]=(char)245;
	buffer[36]=(char)2;		buffer[37]=(char)144;	buffer[38]=(char)91;	buffer[39]=(char)70;
	buffer[40]=(char)57;	buffer[41]=(char)20;	buffer[42]=(char)192;	buffer[43]=(char)98;
	buffer[44]=(char)77;	buffer[45]=(char)31;	buffer[46]=(char)111;	buffer[47]=(char)227;
	buffer[48]=(char)16;	buffer[49]=(char)115;	buffer[50]=(char)182;	buffer[51]=(char)211;
	buffer[52]=(char)95;	buffer[53]=(char)143;	buffer[54]=(char)108;	buffer[55]=(char)241;
	buffer[56]=(char)115;	buffer[57]=(char)2;		buffer[58]=(char)248;	buffer[59]=(char)62;
	buffer[60]=(char)121;	buffer[61]=(char)134;	buffer[62]=(char)55;	buffer[63]=(char)10;
	buffer[64]=(char)30;	buffer[65]=(char)74;	buffer[66]=(char)145;	buffer[67]=(char)132;
	buffer[68]=(char)79;	buffer[69]=(char)159;	buffer[70]=(char)156;	buffer[71]=(char)18;
	buffer[72]=(char)223;	buffer[73]=(char)175;	buffer[74]=(char)84;	buffer[75]=(char)197;
	buffer[76]=(char)130;	buffer[77]=(char)17;	buffer[78]=(char)95;	buffer[79]=(char)230;
	buffer[80]=(char)200;	buffer[81]=(char)202;	buffer[82]=(char)68;	buffer[83]=(char)202;
	buffer[84]=(char)132;	buffer[85]=(char)22;	buffer[86]=(char)150;	buffer[87]=(char)18;
	buffer[88]=(char)193;	buffer[89]=(char)127;	buffer[90]=(char)10;	buffer[91]=(char)42;
	buffer[92]=(char)158;	buffer[93]=(char)129;	buffer[94]=(char)185;	buffer[95]=(char)153;
	buffer[96]=(char)219;	buffer[97]=(char)226;	buffer[98]=(char)70;	buffer[99]=(char)56;
	buffer[100]=(char)100;	buffer[101]=(char)74;	buffer[102]=(char)149;	buffer[103]=(char)14;
	buffer[104]=(char)92;	buffer[105]=(char)14;	buffer[106]=(char)201;	buffer[107]=(char)106;
	buffer[108]=(char)93;	buffer[109]=(char)221;	buffer[110]=(char)96;	buffer[111]=(char)221;
	buffer[112]=(char)148;	buffer[113]=(char)233;	buffer[114]=(char)46;	buffer[115]=(char)75;
	buffer[116]=(char)195;	buffer[117]=(char)70;	buffer[118]=(char)77;	buffer[119]=(char)157;
	buffer[120]=(char)54;	buffer[121]=(char)99;	buffer[122]=(char)95;	buffer[123]=(char)233;
	buffer[124]=(char)8;	buffer[125]=(char)180;	buffer[126]=(char)4;	buffer[127]=(char)178;
}
void SetD(BYTE* buffer)
{
	buffer[0]=(char)117;	buffer[1]=(char)86;		buffer[2]=(char)160;	buffer[3]=(char)104;
	buffer[4]=(char)137;	buffer[5]=(char)51;		buffer[6]=(char)8;		buffer[7]=(char)4;
	buffer[8]=(char)73;		buffer[9]=(char)178;	buffer[10]=(char)136;	buffer[11]=(char)200;
	buffer[12]=(char)145;	buffer[13]=(char)26;	buffer[14]=(char)66;	buffer[15]=(char)84;
	buffer[16]=(char)48;	buffer[17]=(char)143;	buffer[18]=(char)169;	buffer[19]=(char)74;
	buffer[20]=(char)251;	buffer[21]=(char)86;	buffer[22]=(char)25;	buffer[23]=(char)188;
	buffer[24]=(char)162;	buffer[25]=(char)3;		buffer[26]=(char)162;	buffer[27]=(char)131;
	buffer[28]=(char)107;	buffer[29]=(char)81;	buffer[30]=(char)3;		buffer[31]=(char)49;
	buffer[32]=(char)251;	buffer[33]=(char)133;	buffer[34]=(char)157;	buffer[35]=(char)164;
	buffer[36]=(char)147;	buffer[37]=(char)138;	buffer[38]=(char)72;	buffer[39]=(char)81;
	buffer[40]=(char)111;	buffer[41]=(char)137;	buffer[42]=(char)65;	buffer[43]=(char)48;
	buffer[44]=(char)154;	buffer[45]=(char)11;	buffer[46]=(char)42;	buffer[47]=(char)215;
	buffer[48]=(char)188;	buffer[49]=(char)180;	buffer[50]=(char)175;	buffer[51]=(char)54;
	buffer[52]=(char)79;	buffer[53]=(char)40;	buffer[54]=(char)163;	buffer[55]=(char)60;
	buffer[56]=(char)123;	buffer[57]=(char)20;	buffer[58]=(char)96;	buffer[59]=(char)196;
	buffer[60]=(char)188;	buffer[61]=(char)10;	buffer[62]=(char)254;	buffer[63]=(char)213;
	buffer[64]=(char)17;	buffer[65]=(char)244;	buffer[66]=(char)224;	buffer[67]=(char)61;
	buffer[68]=(char)197;	buffer[69]=(char)77;	buffer[70]=(char)6;		buffer[71]=(char)3;
	buffer[72]=(char)22;	buffer[73]=(char)32;	buffer[74]=(char)157;	buffer[75]=(char)117;
	buffer[76]=(char)92;	buffer[77]=(char)218;	buffer[78]=(char)114;	buffer[79]=(char)245;
	buffer[80]=(char)73;	buffer[81]=(char)10;	buffer[82]=(char)47;	buffer[83]=(char)221;
	buffer[84]=(char)35;	buffer[85]=(char)24;	buffer[86]=(char)123;	buffer[87]=(char)235;
	buffer[88]=(char)79;	buffer[89]=(char)237;	buffer[90]=(char)175;	buffer[91]=(char)153;
	buffer[92]=(char)209;	buffer[93]=(char)193;	buffer[94]=(char)156;	buffer[95]=(char)80;
	buffer[96]=(char)253;	buffer[97]=(char)255;	buffer[98]=(char)54;	buffer[99]=(char)169;
	buffer[100]=(char)111;	buffer[101]=(char)113;	buffer[102]=(char)196;	buffer[103]=(char)202;
	buffer[104]=(char)121;	buffer[105]=(char)77;	buffer[106]=(char)27;	buffer[107]=(char)100;
	buffer[108]=(char)179;	buffer[109]=(char)12;	buffer[110]=(char)211;	buffer[111]=(char)140;
	buffer[112]=(char)217;	buffer[113]=(char)44;	buffer[114]=(char)182;	buffer[115]=(char)71;
	buffer[116]=(char)208;	buffer[117]=(char)247;	buffer[118]=(char)92;	buffer[119]=(char)218;
	buffer[120]=(char)41;	buffer[121]=(char)105;	buffer[122]=(char)234;	buffer[123]=(char)216;
	buffer[124]=(char)85;	buffer[125]=(char)118;	buffer[126]=(char)54;	buffer[127]=(char)8;
}

void HexBufferToString(char* str,BYTE* buf,int len)
{
	int k;
	sprintf_s(str, 3, "0x");
	for( k=0;k<len;k++)
	{
		char strTemp[20];
		sprintf_s(strTemp, 3, "%02x",buf[k]);
		strcat_s(str, strlen(str) + 3, strTemp);
	}
}

void main(int argc, char* argv[])
{
	WORD handle[16], p1, p2, p3, p4, retcode;
	DWORD lp1, lp2;
	BYTE buffer[BUF_SIZE];
	BYTE *pbyRetBuf = NULL;
	int i = 0, j = 0;
	SYSTEMTIME systime;

	p1 = 0xc44c;
	p2 = 0xc8f8;
	p3 = 0x0799;
	p4 = 0xc43b;

	//Find Rockey4Smart with specified password
	retcode = Rockey(RY_FIND, &handle[0], &lp1, &lp2, &p1, &p2, &p3, &p4, buffer);
	if (retcode)
	{
		ShowERR(retcode);
		return;
	}
	printf("Find Rock: %08X\n", lp1);
	//Open Rockey4Smart
	retcode = Rockey(RY_OPEN, &handle[0], &lp1, &lp2, &p1, &p2, &p3, &p4, buffer);
	if (retcode)
	{
		ShowERR(retcode);
		return;
	}

	SetN(buffer);
	SetD(buffer + 128);
	//Set update key pair
	retcode = Rockey(RY_SET_UPDATE_KEY, &handle[0], &lp1, &lp2, &p1, &p2, &p3, &p4, buffer);
	if (retcode)
	{
		ShowERR(retcode);
		return;
	}

	//Update memory
	memset(buffer, 0, BUF_SIZE);
	lp2 = 2012;
	p1 = 11;
	p2 = 11;
	p3 = 11;
	p4 = 11;
	retcode = Rockey(RY_GET_TIME_DWORD, &handle[0], (DWORD *)&buffer[3 * 4], &lp2, &p1, &p2, &p3, &p4, NULL);
	lp1 = 0x00000008;
	retcode = Rockey(RY_ADD_UPDATE_HEADER, &handle[0], &lp1, &lp2, &p1, &p2, &p3, &p4, buffer);
	for (i = 0; i < 128; i++)
	{
		buffer[i] = 5;// * (BYTE)i;
	}
	p1 = 0;   //flag,0:update memory,1:upate time unit,2:update count unit,3:update arithmetic,4:update UID,5:update module word
	p2 = 0;   //offset of user memory
	p3 = 119; //length
	retcode = Rockey(RY_ADD_UPDATE_CONTENT, &handle[0], &lp1, &lp2, &p1, &p2, &p3, &p4, buffer);
	//Generate update file, that is encrypting file header and file content
	retcode = Rockey(RY_UPDATE_GEN_EX, &handle[0], &lp1, &lp2, &p1, &p2, &p3, &p4, NULL);
	pbyRetBuf = (BYTE *)malloc(p1 * sizeof(BYTE));
	p1 = 0xc44c;
	p2 = 0xc8f8;
	retcode = Rockey(RY_UPDATE_GEN_EX, &handle[0], &lp1, &lp2, &p1, &p2, &p3, &p4, pbyRetBuf);
	if (retcode)
	{
		ShowERR(retcode);
		return;
	}
	//Update dongle
	retcode = Rockey(RY_UPDATE_EX, &handle[0], &lp1, &lp2, &p1, &p2, &p3, &p4, pbyRetBuf);
	if (retcode)
	{
		ShowERR(retcode);
		return;
	}
	if (pbyRetBuf)
	{
		free(pbyRetBuf);
		pbyRetBuf = NULL;
	}


	Sleep(1000*60*1.5);
	//Update time
	memset(buffer, 0, BUF_SIZE);
	lp2 = 2012;
	p1 = 11;
	p2 = 11;
	p3 = 11;
	p4 = 11;
	Rockey(RY_GET_TIME_DWORD, &handle[0], (DWORD *)&buffer[3 * 4], &lp2, &p1, &p2, &p3, &p4, NULL);
	lp1 = 0x00000008;
	Rockey(RY_ADD_UPDATE_HEADER, &handle[0], &lp1, &lp2, &p1, &p2, &p3, &p4, buffer);

	lp2 = 2012;
	p1 = 11;
	p2 = 11;
	p3 = 11;
	p4 = 11;
	Rockey(RY_GET_TIME_DWORD, &handle[0], (DWORD *)buffer, &lp2, &p1, &p2, &p3, &p4, NULL);
	p1 = 1;
	p2 = 0;
	p3 = 1;		//date
	Rockey(RY_ADD_UPDATE_CONTENT, &handle[0], &lp1, &lp2, &p1, &p2, &p3, &p4, buffer);

	p1 = 1;
	p2 = 1;
	p3 = 2;		//hour
	*(DWORD *)buffer = 24;
	Rockey(RY_ADD_UPDATE_CONTENT, &handle[0], &lp1, &lp2, &p1, &p2, &p3, &p4, buffer);
	p1 = 1;
	p2 = 2;
	p3 = 3;		//day
	*(DWORD *)buffer = 30;
	Rockey(RY_ADD_UPDATE_CONTENT, &handle[0], &lp1, &lp2, &p1, &p2, &p3, &p4, buffer);


	retcode = Rockey(RY_UPDATE_GEN_EX, &handle[0], &lp1, &lp2, &p1, &p2, &p3, &p4, NULL);
	pbyRetBuf = (BYTE *)malloc(p1 * sizeof(BYTE ));
	p1 = 0xc44c;
	p2 = 0xc8f8;
	retcode = Rockey(RY_UPDATE_GEN_EX, &handle[0], &lp1, &lp2, &p1, &p2, &p3, &p4, pbyRetBuf);
	if (retcode)
	{
		ShowERR(retcode);
		return;
	}
	retcode = Rockey(RY_UPDATE_EX, &handle[0], &lp1, &lp2, &p1, &p2, &p3, &p4, pbyRetBuf);
	if (retcode)
	{
		ShowERR(retcode);
		return;
	}
	p1 = 0;
	retcode = Rockey(RY_GET_TIMER_EX, &handle[0], &lp1, &lp2, &p1, &p2, &p3, &p4, buffer);
	if (retcode)
	{
		ShowERR(retcode);
		return;
	}
	p1 = 1;
	retcode = Rockey(RY_GET_TIMER_EX, &handle[0], &lp1, &lp2, &p1, &p2, &p3, &p4, buffer);
	if (retcode)
	{
		ShowERR(retcode);
		return;
	}
	p1 = 2;
	retcode = Rockey(RY_GET_TIMER_EX, &handle[0], &lp1, &lp2, &p1, &p2, &p3, &p4, buffer);
	if (retcode)
	{
		ShowERR(retcode);
		return;
	}
	if (pbyRetBuf)
	{
		free(pbyRetBuf);
		pbyRetBuf = NULL;
	}
	//Update count
	Sleep(1000*60*1.5);
	memset(buffer, 0, BUF_SIZE);
	lp2 = 2012;
	p1 = 11;
	p2 = 11;
	p3 = 11;
	p4 = 11;
	Rockey(RY_GET_TIME_DWORD, &handle[0], (DWORD *)&buffer[3 * 4], &lp2, &p1, &p2, &p3, &p4, NULL);
	lp1 = 0x00000008;
	Rockey(RY_ADD_UPDATE_HEADER, &handle[0], &lp1, &lp2, &p1, &p2, &p3, &p4, buffer);

	p1 = 2;
	p2 = 0;
	p3 = 1;
	*(DWORD *)buffer = 1;
	Rockey(RY_ADD_UPDATE_CONTENT, &handle[0], &lp1, &lp2, &p1, &p2, &p3, &p4, buffer);
	p1 = 2;
	p2 = 1;
	p3 = 1;
	*(DWORD *)buffer = 1;
	Rockey(RY_ADD_UPDATE_CONTENT, &handle[0], &lp1, &lp2, &p1, &p2, &p3, &p4, buffer);
	p1 = 2;
	p2 = 2;
	p3 = 1;
	*(DWORD *)buffer = 1;
	Rockey(RY_ADD_UPDATE_CONTENT, &handle[0], &lp1, &lp2, &p1, &p2, &p3, &p4, buffer);

	retcode = Rockey(RY_UPDATE_GEN_EX, &handle[0], &lp1, &lp2, &p1, &p2, &p3, &p4, NULL);
	pbyRetBuf = (BYTE *)malloc(p1 * sizeof(BYTE));
	p1 = 0xc44c;
	p2 = 0xc8f8;
	retcode = Rockey(RY_UPDATE_GEN_EX, &handle[0], &lp1, &lp2, &p1, &p2, &p3, &p4, pbyRetBuf);
	if (retcode)
	{
		ShowERR(retcode);
		return;
	}
	retcode = Rockey(RY_UPDATE_EX, &handle[0], &lp1, &lp2, &p1, &p2, &p3, &p4, pbyRetBuf);
	if (retcode)
	{
		ShowERR(retcode);
		return;
	}
	p1 = 1;
	retcode = Rockey(RY_GET_COUNTER_EX, &handle[0], &lp1, &lp2, &p1, &p2, &p3, &p4, buffer);
	if (retcode)
	{
		ShowERR(retcode);
		return;
	}
	p1 = 2;
	retcode = Rockey(RY_GET_COUNTER_EX, &handle[0], &lp1, &lp2, &p1, &p2, &p3, &p4, buffer);
	if (retcode)
	{
		ShowERR(retcode);
		return;
	}
	if (pbyRetBuf)
	{
		free(pbyRetBuf);
		pbyRetBuf = NULL;
	}
	//Update arithmetic
	Sleep(1000*60*1.5);
	memset(buffer, 0, BUF_SIZE);
	lp2 = 2012;
	p1 = 11;
	p2 = 11;
	p3 = 11;
	p4 = 11;
	//Converse time format from yyyy-mm-dd hh:mm to minute value from 2006-1-1 0:0
	Rockey(RY_GET_TIME_DWORD, &handle[0], (DWORD *)&buffer[3 * 4], &lp2, &p1, &p2, &p3, &p4, NULL);
	lp1 = 0x00000008;
	//Fill update file header
	Rockey(RY_ADD_UPDATE_HEADER, &handle[0], &lp1, &lp2, &p1, &p2, &p3, &p4, buffer);
	p1 = 3;
	p2 = 0;
	strcpy((char *)buffer, "H=H^H, A=A*23, F=B*17, A=A+F, A=A+G, A=A<C, A=A^D, B=B^B, C=C^C, D=D^D");
	//Fill update file content
	Rockey(RY_ADD_UPDATE_CONTENT, &handle[0], &lp1, &lp2, &p1, &p2, &p3, &p4, buffer);

	retcode = Rockey(RY_UPDATE_GEN_EX, &handle[0], &lp1, &lp2, &p1, &p2, &p3, &p4, NULL);
	pbyRetBuf = (BYTE *)malloc(p1 * sizeof(BYTE));
	p1 = 0xc44c;
	p2 = 0xc8f8;
	retcode = Rockey(RY_UPDATE_GEN_EX, &handle[0], &lp1, &lp2, &p1, &p2, &p3, &p4, pbyRetBuf);
	if (retcode)
	{
		ShowERR(retcode);
		return;
	}
	retcode = Rockey(RY_UPDATE_EX, &handle[0], &lp1, &lp2, &p1, &p2, &p3, &p4, pbyRetBuf);
	if (retcode)
	{
		ShowERR(retcode);
		return;
	}
	lp1 = 0;
	lp2 = 7;
	p1 = 5;
	p2 = 3;
	p3 = 1;
	p4 = 0xffff;
	retcode = Rockey(RY_CALCULATE1, &handle[0], &lp1, &lp2, &p1, &p2, &p3, &p4, buffer);
	if (retcode)
	{
		ShowERR(retcode);
		return;
	}
	if (pbyRetBuf)
	{
		free(pbyRetBuf);
		pbyRetBuf = NULL;
	}
	//Update User ID
	Sleep(1000*60*1.5);
	memset(buffer, 0, BUF_SIZE);
	lp2 = 2012;
	p1 = 11;
	p2 = 11;
	p3 = 11;
	p4 = 11;
	Rockey(RY_GET_TIME_DWORD, &handle[0], (DWORD *)&buffer[3 * 4], &lp2, &p1, &p2, &p3, &p4, NULL);
	lp1 = 0x00000008;
	Rockey(RY_ADD_UPDATE_HEADER, &handle[0], &lp1, &lp2, &p1, &p2, &p3, &p4, buffer);
	p1 = 4;
	*(DWORD *)buffer = 0x12345678;
	Rockey(RY_ADD_UPDATE_CONTENT, &handle[0], &lp1, &lp2, &p1, &p2, &p3, &p4, buffer);

	retcode = Rockey(RY_UPDATE_GEN_EX, &handle[0], &lp1, &lp2, &p1, &p2, &p3, &p4, NULL);
	pbyRetBuf = (BYTE *)malloc(p1 * sizeof(BYTE));
	p1 = 0xc44c;
	p2 = 0xc8f8;
	retcode = Rockey(RY_UPDATE_GEN_EX, &handle[0], &lp1, &lp2, &p1, &p2, &p3, &p4, pbyRetBuf);
	if (retcode)
	{
		ShowERR(retcode);
		return;
	}
	retcode = Rockey(RY_UPDATE_EX, &handle[0], &lp1, &lp2, &p1, &p2, &p3, &p4, pbyRetBuf);
	if (retcode)
	{
		ShowERR(retcode);
		return;
	}
	if (pbyRetBuf)
	{
		free(pbyRetBuf);
		pbyRetBuf = NULL;
	}
	//Update module word
	Sleep(1000*60*1.5);
	memset(buffer, 0, BUF_SIZE);
	lp2 = 2012;
	p1 = 11;
	p2 = 11;
	p3 = 11;
	p4 = 11;
	Rockey(RY_GET_TIME_DWORD, &handle[0], (DWORD *)&buffer[3 * 4], &lp2, &p1, &p2, &p3, &p4, NULL);
	lp1 = 0x00000008;
	Rockey(RY_ADD_UPDATE_HEADER, &handle[0], &lp1, &lp2, &p1, &p2, &p3, &p4, buffer);

	p1 = 5;
	p2 = 0;
	p3 = 1;
	*(WORD *)buffer = 3;
	Rockey(RY_ADD_UPDATE_CONTENT, &handle[0], &lp1, &lp2, &p1, &p2, &p3, &p4, buffer);
	p1 = 5;
	p2 = 1;
	p3 = 1;
	*(WORD *)buffer = 4;
	Rockey(RY_ADD_UPDATE_CONTENT, &handle[0], &lp1, &lp2, &p1, &p2, &p3, &p4, buffer);
	p1 = 5;
	p2 = 2;
	p3 = 1;
	*(WORD *)buffer = 1;
	Rockey(RY_ADD_UPDATE_CONTENT, &handle[0], &lp1, &lp2, &p1, &p2, &p3, &p4, buffer);

	retcode = Rockey(RY_UPDATE_GEN_EX, &handle[0], &lp1, &lp2, &p1, &p2, &p3, &p4, NULL);
	pbyRetBuf = (BYTE *)malloc(p1 * sizeof(BYTE));
	p1 = 0xc44c;
	p2 = 0xc8f8;
	retcode = Rockey(RY_UPDATE_GEN_EX, &handle[0], &lp1, &lp2, &p1, &p2, &p3, &p4, pbyRetBuf);
	if (retcode)
	{
		ShowERR(retcode);
		return;
	}
	retcode = Rockey(RY_UPDATE_EX, &handle[0], &lp1, &lp2, &p1, &p2, &p3, &p4, pbyRetBuf);
	if (retcode)
	{
		ShowERR(retcode);
		return;
	}
	if (pbyRetBuf)
	{
		free(pbyRetBuf);
		pbyRetBuf = NULL;
	}

	retcode = Rockey(RY_CLOSE, &handle[0], &lp1, &lp2, &p1, &p2, &p3, &p4, buffer);
	if (retcode)
	{
		ShowERR(retcode);
		return;
	}
	printf("success");
	getchar();
	return;
}

