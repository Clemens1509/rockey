// Count.cpp : Defines the entry point for the console application.
//
#include <stdio.h>
#include <windows.h>
#include "Ry4S.h"
#pragma comment(lib,"Ry4S.lib")
int main(int argc, char* argv[])
{

	DWORD dwLP1 = 0;
	DWORD dwLP2 = 0;
	WORD  wP1   = 0xc44c;
	WORD  wP2   = 0xc8f8;
	WORD  wP3   = 0x0799;
	WORD  wP4   = 0xc43b;
	BYTE bBuffer[1024] ={0};
	int i = 0;
	int j = 0;
	int k = 0;

	DWORD dwRet = 0;

	WORD dwHandel[16];
	//Find Dongle
	dwRet = Rockey(RY_FIND,&dwHandel[0],&dwLP1,&dwLP2,&wP1,&wP2,&wP3,&wP4,bBuffer);
	if (dwRet != ERR_SUCCESS)
	{
		printf(" Find Dongle Faild, Error Code = %d\n",dwRet);
		Rockey(RY_CLOSE,&dwHandel[0],NULL,NULL,NULL,NULL,NULL,NULL,NULL);
		return 0;

	}
	//Open Dongle
	dwRet = Rockey(RY_OPEN,&dwHandel[0],&dwLP1,&dwLP2,&wP1,&wP2,&wP3,&wP4,bBuffer);
	if (dwRet != ERR_SUCCESS)
	{
		printf("Open Dongle Faild, Error Code = %d\n",dwRet);
		Rockey(RY_CLOSE,&dwHandel[0],NULL,NULL,NULL,NULL,NULL,NULL,NULL);
		//return 0;
	}
	dwRet = Rockey(RY_VERSION,&dwHandel[0],&dwLP1,&dwLP2,&wP1,&wP2,&wP3,&wP4,bBuffer);
	if (dwRet)
	{
		printf("Get cos version Faild, Error Code = %d\n",dwRet);
		Rockey(RY_CLOSE,&dwHandel[0],NULL,NULL,NULL,NULL,NULL,NULL,NULL);
		return 0;
	}
	if (dwLP2 < 259)
	{
		printf("Dongle is Old Version, Cannot support new Function, Program End !\n");
		Rockey(RY_CLOSE,&dwHandel[0],NULL,NULL,NULL,NULL,NULL,NULL,NULL);
		return 0;
	}

	i = 1;
	//Find and Open more Dongle
	while (dwRet == ERR_SUCCESS)
	{
		dwRet = Rockey(RY_FIND_NEXT,&dwHandel[i],&dwLP1,&dwLP2,&wP1,&wP2,&wP3,&wP4,bBuffer);
		if (dwRet == ERR_NOMORE)
		{
			break;
		}
		if (dwRet != ERR_SUCCESS)
		{
			printf("Find Next Dongle Faild, Error Code = %d\n",dwRet);
			Rockey(RY_CLOSE,&dwHandel[0],NULL,NULL,NULL,NULL,NULL,NULL,NULL);

		}
		dwRet = Rockey(RY_OPEN,&dwHandel[i],&dwLP1,&dwLP2,&wP1,&wP2,&wP3,&wP4,bBuffer);
		if (dwRet != ERR_SUCCESS)
		{
			printf("Open Next Dongle Faild, Error Code = %d\n",dwRet);
			Rockey(RY_CLOSE,&dwHandel[0],NULL,NULL,NULL,NULL,NULL,NULL,NULL);
		}
		dwRet = Rockey(RY_VERSION,&dwHandel[0],&dwLP1,&dwLP2,&wP1,&wP2,&wP3,&wP4,bBuffer);
		if (dwRet)
		{
			printf("Get Next cos version Faild, Error Code = %d\n",dwRet);
			Rockey(RY_CLOSE,&dwHandel[0],NULL,NULL,NULL,NULL,NULL,NULL,NULL);
			return 0;
		}
		if (dwLP2 < 259)
		{
			printf("Dongle is Old Version, Cannot support new Function, Program End !\n");
			Rockey(RY_CLOSE,&dwHandel[0],NULL,NULL,NULL,NULL,NULL,NULL,NULL);
			return 0;
		}
		i++;
		printf("Find Dongle: %08X\n", dwLP1);
	}

	//Set and Get dongle's count
	for (j = 0; j < i; j++ )
	{
		wP1 = 1;//the count nuit number, base 0
		wP3 = 1;//1: decreasing is allowed,  0: Not allowed

		DWORD dwVaule = 10;

		dwRet = Rockey(RY_SET_COUNTER_EX,&dwHandel[j],&dwLP1,&dwLP2,&wP1,&wP2,&wP3,&wP4,(BYTE*)(&dwVaule));
		if (dwRet != ERR_SUCCESS)
		{
			printf("Set count faild, Error Code = %d\n",dwRet);
			Rockey(RY_CLOSE,&dwHandel[0],NULL,NULL,NULL,NULL,NULL,NULL,NULL);
			//return 0;
		}

		printf("set unit %d = count %d \n",wP1,dwVaule);

		wP3 = 999;
		wP4 = 0;

		DWORD   dwCount;  

		//test - call three times
		dwRet = Rockey(RY_GET_COUNTER_EX,&dwHandel[j],&dwLP1,&dwLP2,&wP1,&wP2,&wP3,NULL,bBuffer);
		dwRet = Rockey(RY_GET_COUNTER_EX,&dwHandel[j],&dwLP1,&dwLP2,&wP1,&wP2,&wP3,NULL,bBuffer);
		dwRet = Rockey(RY_GET_COUNTER_EX,&dwHandel[j],&dwLP1,&dwLP2,&wP1,&wP2,&wP3,NULL,bBuffer);

		if (dwRet != ERR_SUCCESS)
		{
			printf("Get count faild, Error Code = %d\n",dwRet);
			Rockey(RY_CLOSE,&dwHandel[0],NULL,NULL,NULL,NULL,NULL,NULL,NULL);
			//return 0;
		}
		memset((void*)(&dwCount),0,sizeof(DWORD));   
		memcpy((void*)(&dwCount),bBuffer,2);   
		printf("get unit %d = count %d\n",wP1,dwCount);
		
	}
	getchar();
	return 0;
}

