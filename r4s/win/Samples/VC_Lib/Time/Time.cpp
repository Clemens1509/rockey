// Time.cpp : Defines the entry point for the console application.
//

#include <stdio.h>
#include <windows.h>
#include "Ry4S.h"
#include <time.h>


#pragma comment(lib,"Ry4S.lib")

int main(int argc, char* argv[])
{

	DWORD dwLP1 = 0;
	DWORD dwLP2 = 0;
	WORD  wP1   = 0xc44c;
	WORD  wP2   = 0xc8f8;
	WORD  wP3   = 0x0799;
	WORD  wP4   = 0xc43b;

	BYTE bBuffer[1024] ={"0"};
	int i = 0;
	int j = 0;
	int k = 0;

	DWORD dwRet = 0;

	WORD dwHandel[16];
	//Find Rockey4Smart
	dwRet = Rockey(RY_FIND,&dwHandel[0],&dwLP1,&dwLP2,&wP1,&wP2,&wP3,&wP4,bBuffer);
	if (dwRet != ERR_SUCCESS)
	{
		printf("Failed to find dongle��error code = %d\n",dwRet);
		Rockey(RY_CLOSE,&dwHandel[0],NULL,NULL,NULL,NULL,NULL,NULL,NULL);
		return 0;

	}
	//Open Rockey4Smart
	dwRet = Rockey(RY_OPEN,&dwHandel[0],&dwLP1,&dwLP2,&wP1,&wP2,&wP3,&wP4,bBuffer);
	if (dwRet != ERR_SUCCESS)
	{
		printf("Failed to open dongle,error code = %d\n",dwRet);
		Rockey(RY_CLOSE,&dwHandel[0],NULL,NULL,NULL,NULL,NULL,NULL,NULL);
		return 0;
	}
	dwRet = Rockey(RY_VERSION,&dwHandel[0],&dwLP1,&dwLP2,&wP1,&wP2,&wP3,&wP4,bBuffer);
	if (dwRet)
	{
		printf("Failed to get version,error code = %d\n",dwRet);
		Rockey(RY_CLOSE,&dwHandel[0],NULL,NULL,NULL,NULL,NULL,NULL,NULL);
		return 0;
	}
	if (dwLP2 < 259)
	{
		printf("Old dongle doesn't support new TIME and COUNT interface,please use v1.03 dongle for test!\n");
		Rockey(RY_CLOSE,&dwHandel[0],NULL,NULL,NULL,NULL,NULL,NULL,NULL);
		return 0;
	}
	
	i = 1;
	//Find all other Rockey4Smart with the same password 
	while (dwRet == ERR_SUCCESS)
	{
		dwRet = Rockey(RY_FIND_NEXT,&dwHandel[i],&dwLP1,&dwLP2,&wP1,&wP2,&wP3,&wP4,bBuffer);
		if (dwRet == ERR_NOMORE)
		{
			break;
		}
		if (dwRet != ERR_SUCCESS)
		{
			printf("Failed to find next Rockey4Smart, error code = %d\n",dwRet);
			Rockey(RY_CLOSE,&dwHandel[0],NULL,NULL,NULL,NULL,NULL,NULL,NULL);
			return 0;
		}
		dwRet = Rockey(RY_OPEN,&dwHandel[i],&dwLP1,&dwLP2,&wP1,&wP2,&wP3,&wP4,bBuffer);
		if (dwRet != ERR_SUCCESS)
		{
			printf("Failed to open next dongle,error code = %d\n",dwRet);
			Rockey(RY_CLOSE,&dwHandel[0],NULL,NULL,NULL,NULL,NULL,NULL,NULL);
			return 0;
		}
		dwRet = Rockey(RY_VERSION,&dwHandel[0],&dwLP1,&dwLP2,&wP1,&wP2,&wP3,&wP4,bBuffer);
		if (dwRet)
		{
			printf("Failed to get version, error code = %d\n",dwRet);
			Rockey(RY_CLOSE,&dwHandel[0],NULL,NULL,NULL,NULL,NULL,NULL,NULL);
			return 0;
		}
		if (dwLP2 < 259)
		{
			printf("Old dongle doesn't support new TIME and COUNT interface,please use v1.03 dongle for test!\n");
			Rockey(RY_CLOSE,&dwHandel[0],NULL,NULL,NULL,NULL,NULL,NULL,NULL);
			return 0;
		}
		i++;

		printf("Found Rockey4Smart: %08X\n", dwLP1);
	}


	//Set time,get time on opened dongles
	for (j = 0; j < i; j++ )
	{
		//set time authorization
 		wP1 = 0;//Time unit number based on 0
		wP3 = 2;//Time flag. 1 means date,2 means hour

		DWORD dwHour = 10;
		memcpy(bBuffer,&dwHour,sizeof(DWORD));

		dwRet = Rockey(RY_SET_TIMER_EX,&dwHandel[j],&dwLP1,&dwLP2,&wP1,&wP2,&wP3,&wP4,bBuffer);
		if (dwRet != ERR_SUCCESS)
		{
			printf("Failed to set time(hour),error code = %d\n",dwRet);
			Rockey(RY_CLOSE,&dwHandel[j],NULL,NULL,NULL,NULL,NULL,NULL,NULL);
		}
		printf("Set time successfully. Unit %d, %d hours\n",wP1,dwHour);

		memset(bBuffer,0,sizeof(bBuffer));

		SYSTEMTIME sysTime;
		GetSystemTime(&sysTime);
		memcpy(bBuffer, &sysTime, sizeof(SYSTEMTIME));

		//Adjust time,if the dongle time is earlier than pc, then use the pc time to synchronize dongle's
		//otherwize keep the dongle time unchanged.
		dwRet = Rockey(RY_ADJUST_TIMER_EX,&dwHandel[j],&dwLP1,&dwLP2,&wP1,&wP2,&wP3,&wP4,bBuffer);

		if (dwRet != ERR_SUCCESS)
		{
			printf("Failed to adjust time, error code = %d\n",dwRet);
			Rockey(RY_CLOSE,&dwHandel[j],NULL,NULL,NULL,NULL,NULL,NULL,NULL);
		}
		dwHour = 0;
		dwRet = Rockey(RY_GET_TIMER_EX,&dwHandel[j],&dwLP1,&dwLP2,&wP1,&wP2,&wP3,&wP4,bBuffer);
		if (dwRet != ERR_SUCCESS)
		{
			printf("Failed to get time, error code = %d\n",dwRet);
			Rockey(RY_CLOSE,&dwHandel[j],NULL,NULL,NULL,NULL,NULL,NULL,NULL);
		}

		if (wP3 == 2 && dwRet ==ERR_SUCCESS)
		{
			DWORD dwHour;
			memset((void*)(&dwHour),0,sizeof(DWORD));   
			memcpy((void*)(&dwHour),bBuffer,2);   
			printf("Get time successfully. Unit %d, %d hours\n",wP1,dwHour);
		}


		SYSTEMTIME time1;
		//current local date and time
		GetLocalTime(&time1);	
		printf("PC System time(Local): %d-%d-%d %d:%d:%d \n", time1.wYear, time1.wMonth, time1.wDay, time1.wHour, time1.wMinute, time1.wSecond);
        //current system date and time. The system time is expressed in Coordinated Universal Time (UTC).
		GetSystemTime(&time1); 
		printf("PC System time(UTC): %d-%d-%d %d:%d:%d \n", time1.wYear, time1.wMonth, time1.wDay, time1.wHour, time1.wMinute, time1.wSecond);
	
		//set authorization date
		wP1 = 1;//Time unit number based on 0
		wP3 = 1;//Time flag. 1 means date, 2 means hour
		
		memset(bBuffer,0,sizeof(bBuffer));

		SYSTEMTIME sysTimeLisense;
		GetSystemTime(&sysTimeLisense);
		sysTimeLisense.wDay = sysTimeLisense.wDay + 2;

		printf("Set time. Unit %d, Time(UTC): %d-%d-%d %d:%d:%d\n",wP1,sysTimeLisense.wYear,sysTimeLisense.wMonth,
			   sysTimeLisense.wDay,sysTimeLisense.wHour,
			   sysTimeLisense.wMinute,sysTimeLisense.wSecond);
		memcpy(bBuffer, &sysTimeLisense, sizeof(SYSTEMTIME));

		dwRet = Rockey(RY_SET_TIMER_EX,&dwHandel[j],&dwLP1,&dwLP2,&wP1,&wP2,&wP3,&wP4,(BYTE*)bBuffer);
		if (dwRet != ERR_SUCCESS)
		{
			printf("Failed to set time,error code = %d\n",dwRet);
			Rockey(RY_CLOSE,&dwHandel[j],NULL,NULL,NULL,NULL,NULL,NULL,NULL);
			//return 0;
		} 

		memset(bBuffer,0,sizeof(bBuffer));
		SYSTEMTIME licTimeDay;
		GetSystemTime(&licTimeDay);
		memcpy(bBuffer, &licTimeDay, sizeof(SYSTEMTIME));

		dwRet = Rockey(RY_ADJUST_TIMER_EX,&dwHandel[j],&dwLP1,&dwLP2,&wP1,&wP2,&wP3,&wP4,(BYTE*)bBuffer);
		if (dwRet != ERR_SUCCESS)
		{
			printf("Failed to adjust time,error code = %d\n",dwRet);
			Rockey(RY_CLOSE,&dwHandel[j],NULL,NULL,NULL,NULL,NULL,NULL,NULL);
			//return 0;
		}

		SYSTEMTIME licST;
		dwRet = Rockey(RY_GET_TIMER_EX,&dwHandel[j],&dwLP1,&dwLP2,&wP1,&wP2,&wP3,&wP4,(BYTE*)(&licST));

		if (dwRet != ERR_SUCCESS)
		{
			printf("Failed to get time,error code = %d\n",dwRet);
			Rockey(RY_CLOSE,&dwHandel[j],NULL,NULL,NULL,NULL,NULL,NULL,NULL);
			//return 0;
		}

		//cut-off time
		if (wP3 == 1)
		{
			printf("Get time successfully. Unit %d, Time(UTC): %d-%d-%d %d:%d:%d \n",wP1,licST.wYear,licST.wMonth,licST.wDay,
				licST.wHour,licST.wMinute,licST.wSecond);

			FILETIME ft;
			SystemTimeToFileTime(&licST,&ft);
			FileTimeToLocalFileTime(&ft,&ft);
			FileTimeToSystemTime(&ft,&licST);

			printf("Get time successfully. Unit %d, Time(Local): %d-%d-%d %d:%d:%d \n",wP1,licST.wYear,licST.wMonth,licST.wDay,
				licST.wHour,licST.wMinute,licST.wSecond);
 		}
 	}
	getchar();
	return 0;
}

