#include <windows.h>
#include <stdio.h>
#include <conio.h>
#include "time.h"
#include "ry4s.h"
#pragma comment(lib,"Ry4S.lib")

#define  ADDRESSLEN 500
#define  MAXLEN 1500
void ShowERR(WORD retcode)
{
	if (retcode == 0) return;
	printf("Error Code: %d\n", retcode);
}
void main()
{
	WORD handle[16], p1, p2, p3, p4, retcode;
	DWORD lp1, lp2;
	BYTE buffer[1000*2];
	int i, j,k;
	int nKeyLen[2]={8,16} ;


 	p1 = 0xc44c;
 	p2 = 0xc8f8;
 	p3 = 0x0799;
	p4 = 0xc43b;

	//Find Rockey4Smart with specified password
	retcode = Rockey(RY_FIND, &handle[0], &lp1, &lp2, &p1, &p2, &p3, &p4, buffer);
	if (retcode)
	{
		ShowERR(retcode);
		return;
	}

	printf("Found Rockey4Smart: %08X\n", lp1);
	//Open Rockey4Smart
	retcode = Rockey(RY_OPEN, &handle[0], &lp1, &lp2, &p1, &p2, &p3, &p4, buffer);
	if (retcode)
	{
		ShowERR(retcode);
		return;
	}

	//Get version
	retcode = Rockey(RY_VERSION,&handle[0],&lp1,&lp2,&p1,&p2,&p3,&p4,buffer);
	if (retcode)
	{
		printf("Failed to get version,error code = %d\n",retcode);
		Rockey(RY_CLOSE,&handle[0],NULL,NULL,NULL,NULL,NULL,NULL,NULL);
		return ;
	}
	if (lp2 < 259)
	{
		printf("Old dongle doesn't support new memory read/write interface,please use v1.03 dongle for test!\n");
		Rockey(RY_CLOSE,&handle[0],NULL,NULL,NULL,NULL,NULL,NULL,NULL);
		return ;
	}



	i = 1;
	while (retcode == 0)
	{
		retcode = Rockey(RY_FIND_NEXT, &handle[i], &lp1, &lp2, &p1, &p2, &p3, &p4, buffer);
		if (retcode == ERR_NOMORE) break;
		if (retcode)
		{
			ShowERR(retcode);
			return;
		}

		retcode = Rockey(RY_OPEN, &handle[i], &lp1, &lp2, &p1, &p2, &p3, &p4, buffer);
		if (retcode)
		{
			ShowERR(retcode);
			return;
		}

		retcode = Rockey(RY_VERSION,&handle[0],&lp1,&lp2,&p1,&p2,&p3,&p4,buffer);
		if (retcode)
		{
			printf("Failed to get version��error code = %d\n",retcode);
			Rockey(RY_CLOSE,&handle[0],NULL,NULL,NULL,NULL,NULL,NULL,NULL);
			return ;
		}
		if (lp2 < 259)
		{
			printf("Old dongle doesn't support new memory read/write interface,please use v1.03 dongle for test!\n");
			Rockey(RY_CLOSE,&handle[0],NULL,NULL,NULL,NULL,NULL,NULL,NULL);
			return ;
		}

		i++;
		printf("Found Rockey4Smart: %08X\n", lp1);
	}
	for (j=0; j<MAXLEN; j++)
	{

		if (j<16)
		{
			buffer[j]=8;
		}
		if (j<32&&j>=16)
		{
			buffer[j]=1;
		}
		if (j<64&&j>=32)
		{
			buffer[j]=3;
		}
		if(j>=64)
		{
			buffer[j]=9;
		}
	}

	for (j=0;j<i;j++)
	{
		p1 = ADDRESSLEN;
		p2 = MAXLEN;
		p3 = 1;

		//Write data to user memory
		retcode = Rockey(RY_WRITE_EX, &handle[j], &lp1, &lp2, &p1, &p2, &p3, &p4, buffer);
		if (retcode)
		{
			ShowERR(retcode);
			return;
		}
		for (k=0;k<MAXLEN;k++)
		{
			if (k%16 ==0)
			{
				printf("\n");
			}
			printf("%d", buffer[k]);
		}
		printf("\nWrite: successfully\n");


		p1 = ADDRESSLEN;
		p2 = MAXLEN;
		p3 = 1;
		//Read data from user memory
		retcode = Rockey(RY_READ_EX, &handle[j], &lp1, &lp2, &p1, &p2, &p3, &p4, buffer);
		if (retcode)
		{
			printf("Read fail\n");
			ShowERR(retcode);
			return;
		}
		for (k=0;k<MAXLEN;k++)
		{
			if (k%16 ==0)
			{
				printf("\n");
			}
			printf("%d", buffer[k]);
		}
		printf("\nRead successfully\n");

		//Close Rockey4Smart handle
		retcode = Rockey(RY_CLOSE, &handle[j], &lp1, &lp2, &p1, &p2, &p3, &p4, buffer);
		if (retcode)
		{
			ShowERR(retcode);
			return;
		}		
	}
	system("PAUSE");
}
