// sample.cpp : main project file.

#include "stdafx.h"
#include "stdio.h"
using namespace System;
using namespace Rockey4NDClass;

int main(array<System::String ^> ^args)
{
     unsigned short handle, p1, p2, p3, p4, retcode;
	unsigned int lp1, lp2;
	array<unsigned char> ^ buffer=gcnew array<unsigned char>(100);

	Rockey4ND r4nd;	
	p1 = 0xc44c;
	p2 = 0xc8f8;
	p3 = 0x0799;
	p4 = 0xc43b;
	//find rockey4nd
   retcode= r4nd.Rockey(1,handle, lp1, lp2, p1, p2, p3, p4, buffer);
   if(0!=retcode)
   {
	   printf("Find rockey4nd error code is %d\n",retcode); 
	   return 1;
   }
   //open rockey4nd
   retcode=r4nd.Rockey(3,handle,lp1,lp2, p1, p2, p3, p4, buffer);
   if(0!=retcode)
   {
	   printf("open rockey4nd error code is %d\n",retcode); 
	   return 1;
   }
   //read rockey4nd
   p1=0;
   p2=10;
   retcode=r4nd.Rockey(5,handle,lp1,lp2, p1, p2, p3, p4, buffer);
   if(0!=retcode)
   {
	   printf("read rockey4nd error code is %d\n",retcode); 
	   return 1;
   }
    //close rockey4nd
   retcode=r4nd.Rockey(4,handle,lp1,lp2, p1, p2, p3, p4, buffer);
   if(0!=retcode)
   {
	   printf("close rockey4nd error code is %d\n",retcode); 
	   return 1;
   }
   printf("success!!!");
    return 0;
}
