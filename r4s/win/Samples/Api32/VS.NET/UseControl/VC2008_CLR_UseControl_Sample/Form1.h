#pragma once


namespace VC2008_CLR_UseControl_Sample {

	using namespace System;
	using namespace System::ComponentModel;
	using namespace System::Collections;
	using namespace System::Windows::Forms;
	using namespace System::Data;
	using namespace System::Drawing;
	using namespace Rockey4NDControl;

	/// <summary>
	/// Summary for Form1
	///
	/// WARNING: If you change the name of this class, you will need to change the
	///          'Resource File Name' property for the managed resource compiler tool
	///          associated with all .resx files this class depends on.  Otherwise,
	///          the designers will not be able to interact properly with localized
	///          resources associated with this form.
	/// </summary>
	public ref class Form1 : public System::Windows::Forms::Form
	{
	public:
		Form1(void)
		{
			InitializeComponent();
			//
			//TODO: Add the constructor code here
			//
		}

	protected:
		/// <summary>
		/// Clean up any resources being used.
		/// </summary>
		~Form1()
		{
			if (components)
			{
				delete components;
			}
		}
	private: System::Windows::Forms::TextBox^  textBox1;
	protected: 
	private: System::Windows::Forms::Button^  button1;

	private:
		/// <summary>
		/// Required designer variable.
		/// </summary>
		System::ComponentModel::Container ^components;

#pragma region Windows Form Designer generated code
		/// <summary>
		/// Required method for Designer support - do not modify
		/// the contents of this method with the code editor.
		/// </summary>
		void InitializeComponent(void)
		{
			this->textBox1 = (gcnew System::Windows::Forms::TextBox());
			this->button1 = (gcnew System::Windows::Forms::Button());
			this->SuspendLayout();
			// 
			// textBox1
			// 
			this->textBox1->Location = System::Drawing::Point(12, 24);
			this->textBox1->Multiline = true;
			this->textBox1->Name = L"textBox1";
			this->textBox1->Size = System::Drawing::Size(266, 195);
			this->textBox1->TabIndex = 0;
			// 
			// button1
			// 
			this->button1->Location = System::Drawing::Point(12, 237);
			this->button1->Name = L"button1";
			this->button1->Size = System::Drawing::Size(265, 20);
			this->button1->TabIndex = 1;
			this->button1->Text = L"Test";
			this->button1->UseVisualStyleBackColor = true;
			this->button1->Click += gcnew System::EventHandler(this, &Form1::button1_Click);
			// 
			// Form1
			// 
			this->AutoScaleDimensions = System::Drawing::SizeF(6, 13);
			this->AutoScaleMode = System::Windows::Forms::AutoScaleMode::Font;
			this->ClientSize = System::Drawing::Size(292, 266);
			this->Controls->Add(this->button1);
			this->Controls->Add(this->textBox1);
			this->Name = L"Form1";
			this->Text = L"Form1";
			this->ResumeLayout(false);
			this->PerformLayout();

		}
#pragma endregion
	private: System::Void button1_Click(System::Object^  sender, System::EventArgs^  e) {
	unsigned short handle, p1, p2, p3, p4, retcode;
	unsigned int lp1, lp2;
	array<unsigned char> ^ buffer=gcnew array<unsigned char>(100);

	Rockey4ND r4nd;	
	p1 = 0xc44c;
	p2 = 0xc8f8;
	p3 = 0x0799;
	p4 = 0xc43b;
	//find rockey4nd
	retcode=r4nd.Rockey(1,handle,lp1,lp2, p1, p2, p3, p4, buffer);
	 if(0!=retcode)
   {
	   textBox1->Text="Find rockey4nd error\r\n"; 
	   return;
   }
   //open rockey4nd
   retcode=r4nd.Rockey(3,handle,lp1,lp2, p1, p2, p3, p4, buffer);
   if(0!=retcode)
   {
	  textBox1->Text="open rockey4nd error\r\n"; 
	   return;
   }
   //read rockey4nd
   p1=0;
   p2=10;
   retcode=r4nd.Rockey(5,handle,lp1,lp2, p1, p2, p3, p4, buffer);
   if(0!=retcode)
   {
	   textBox1->Text="read rockey4nd error\r\n";
	   return;
   }
    //close rockey4nd
   retcode=r4nd.Rockey(4,handle,lp1,lp2, p1, p2, p3, p4, buffer);
   if(0!=retcode)
   {
	   textBox1->Text="close rockey4nd error\r\n";
	   return;
   }
   textBox1->Text="Success!";
   retcode= r4nd.Rockey(1,handle, lp1, lp2, p1, p2, p3, p4, buffer);

			 }
	};
}

