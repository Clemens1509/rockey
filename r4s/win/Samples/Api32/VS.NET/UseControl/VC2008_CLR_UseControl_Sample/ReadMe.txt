========================================================================
    APPLICATION : VC2008_CLR_UseControl_Sample Project Overview
========================================================================

AppWizard has created this VC2008_CLR_UseControl_Sample Application for you.  

This file contains a summary of what you will find in each of the files that
make up your VC2008_CLR_UseControl_Sample application.

VC2008_CLR_UseControl_Sample.vcproj
    This is the main project file for VC++ projects generated using an Application Wizard. 
    It contains information about the version of Visual C++ that generated the file, and 
    information about the platforms, configurations, and project features selected with the
    Application Wizard.

VC2008_CLR_UseControl_Sample.cpp
    This is the main application source file.
    Contains the code to display the form.

Form1.h
    Contains the implementation of your form class and InitializeComponent() function.

AssemblyInfo.cpp
    Contains custom attributes for modifying assembly metadata.

/////////////////////////////////////////////////////////////////////////////
Other standard files:

StdAfx.h, StdAfx.cpp
    These files are used to build a precompiled header (PCH) file
    named VC2008_CLR_UseControl_Sample.pch and a precompiled types file named StdAfx.obj.

/////////////////////////////////////////////////////////////////////////////
