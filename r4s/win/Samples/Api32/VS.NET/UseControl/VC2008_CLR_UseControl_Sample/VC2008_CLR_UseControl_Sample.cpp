// VC2008_CLR_UseControl_Sample.cpp : main project file.

#include "stdafx.h"
#include "Form1.h"

using namespace VC2008_CLR_UseControl_Sample;

[STAThreadAttribute]
int main(array<System::String ^> ^args)
{
	// Enabling Windows XP visual effects before any controls are created
	Application::EnableVisualStyles();
	Application::SetCompatibleTextRenderingDefault(false); 

	// Create the main window and run it
	Application::Run(gcnew Form1());
	return 0;
}
