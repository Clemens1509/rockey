using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Windows.Forms;
using Ry4SCom;
namespace Com_Count
{

    enum Ry4Cmd : ushort
    {
        RY_FIND = 1,			        //Find Ry4S
        RY_FIND_NEXT,		            //Find next
        RY_OPEN,			            //Open Ry4S
        RY_CLOSE,			            //Close Ry4S
        RY_READ,			            //Read Ry4S
        RY_WRITE,			            //Write Ry4S
        RY_RANDOM,			            //Generate random
        RY_SEED,			            //Generate seed
        RY_READ_USERID = 10,	//Read UID
        RY_CHECK_MODULE = 12,	//Check Module
        RY_CALCULATE1 = 14,	//Calculate1
        RY_CALCULATE2,		            //Calculate1
        RY_CALCULATE3,		            //Calculate1

        RY_SET_COUNTER = 20,	//set counter
        RY_GET_COUNTER = 21,	//get counter
        RY_DEC_COUNTER = 22,
        RY_SET_TIMER = 23,	//set clock
        RY_GET_TIMER = 24,	//get clock
        RY_ADJUST_TIMER = 25,   //adjust clock

        RY_SET_TIMER_ITV = 26,
        RY_GET_TIMER_ITV = 27,
        RY_DEC_TIMER = 28,
        RY_SET_RSAKEY_N = 29,   //write N and D of RSA
        RY_SET_RSAKEY_D = 30,	//write N and D of RSA
        RY_UPDATE_GEN_HEADER = 31,	//generate cipher of file header
        RY_UPDATE_GEN = 32,	//generate cipher of file content
        RY_UPDATE_CHECK = 33,	//update cipher file
        RY_UPDATE = 34,	//update cipher file
        RY_UNPACK = 35,

        RY_SET_DES_KEY = 41,	//set DES key
        RY_DES_ENC = 42,	//DES encryption
        RY_DES_DEC = 43,	//DES decryption
        RY_RSA_ENC = 44,	//RSA encryption
        RY_RSA_DEC = 45,	//RSA decryption

        RY_SET_COUNTER_EX = 0xA0,	//set counter,value type changed from WORD to DWORD
        RY_GET_COUNTER_EX = 0xA1,	//get counter,value type changed from WORD to DWORD

        RY_SET_TIMER_EX = 0xA2,	//set timer of time unit,time type changed from WORD to DWORD
        RY_GET_TIMER_EX = 0xA3,	//get timer of time unit,time type changed from WORD to DWORD
        RY_ADJUST_TIMER_EX = 0xA4,	//adjust system time,time type changed from WORD to DWORD

        RY_UPDATE_GEN_HEADER_EX = 0xA5,	//generate update file header just for updating RSA key pair
        RY_UPDATE_GEN_EX = 0xA6,	//generate update file content just for updating RSA key pair
        RY_UPDATE_CHECK_EX = 0xA7,	//update file check just for updating RSA key pair
        RY_RY_UPDATE_EX = 0xA8,	//update cipher file just for updating RSA key pair
        RY_VERSION = 100,//version

        DES_SINGLE_MODE = 0,		//DES
        DES_TRIPLE_MODE = 1,		//3DES

        RSA_PRIVATE_KEY = 0,		//private key
        RSA_PUBLIC_KEY = 1,		//public key

        RSA_ROCKEY_PADDING = 0,	//Rockey filling mode
        RSA_USER_PADDING = 1,		//User filling mode


    };

    enum Ry4ErrCode : uint
    {
        ERR_SUCCESS = 0,						//No error
        ERR_NO_PARALLEL_PORT = 0x80300001,		//(0x80300001)No parallel port
        ERR_NO_DRIVER,							//(0x80300002)No drive
        ERR_NO_ROCKEY,							//(0x80300003)No Ry4S
        ERR_INVALID_PASSWORD,					//(0x80300004)Invalid password
        ERR_INVALID_PASSWORD_OR_ID,				//(0x80300005)Invalid password or ID
        ERR_SETID,								//(0x80300006)Set id error
        ERR_INVALID_ADDR_OR_SIZE,				//(0x80300007)Invalid address or size
        ERR_UNKNOWN_COMMAND,					//(0x80300008)Unkown command
        ERR_NOTBELEVEL3,						//(0x80300009)Inner error
        ERR_READ,								//(0x8030000A)Read error
        ERR_WRITE,								//(0x8030000B)Write error
        ERR_RANDOM,								//(0x8030000C)Generate random error
        ERR_SEED,								//(0x8030000D)Generate seed error
        ERR_CALCULATE,							//(0x8030000E)Calculate error
        ERR_NO_OPEN,							//(0x8030000F)The Ry4S is not opened
        ERR_OPEN_OVERFLOW,						//(0x80300010)Open Ry4S too more(>16)
        ERR_NOMORE,								//(0x80300011)No more Ry4S
        ERR_NEED_FIND,							//(0x80300012)Need Find before FindNext
        ERR_DECREASE,							//(0x80300013)Dcrease error
        ERR_AR_BADCOMMAND,						//(0x80300014)Band command
        ERR_AR_UNKNOWN_OPCODE,					//(0x80300015)Unkown op code
        ERR_AR_WRONGBEGIN,						//(0x80300016)There could not be constant in first instruction in arithmetic 
        ERR_AR_WRONG_END,						//(0x80300017)There could not be constant in last instruction in arithmetic 
        ERR_AR_VALUEOVERFLOW,					//(0x80300018)The constant in arithmetic overflow
        ERR_UNKNOWN = 0x8030ffff,				//(0x8030FFFF)Unkown error

        ERR_RECEIVE_NULL = 0x80300100,			//(0x80300100)Receive null
        ERR_PRNPORT_BUSY = 0x80300101			//(0x80300101)Parallel port busy

    };


    public partial class Form1 : Form
    {
        public Form1()
        {
            InitializeComponent();
        }

        private void Exit_Click(object sender, EventArgs e)
        {
            Dispose(true);
        }

        private void Test_Click(object sender, EventArgs e)
        {

            byte[] BUFFER = new byte[1024];

            object obbuffer = new object();

            uint uLp1 = 0;
            uint uLp2 = 0;
            ushort p1 = 0;
            ushort p2 = 0;
            ushort p3 = 0;
            ushort p4 = 0;
            ushort uHandle = 0;


            int iMaxRockey = 0;
            uint[] uiarrRy4ID = new uint[32];
            uint iCurrID;

            CRy4S Ry4S = new CRy4S();

            obbuffer = (object)BUFFER;

            p1 = 0xC44C;
            p2 = 0xC8F8;
            p3 = 0x0799;
            p4 = 0xC43B;
            //Find Rockey4Smart with specified password
            ulong ret = Ry4S.Rockey((ushort)Ry4Cmd.RY_FIND, ref uHandle, ref uLp1, ref uLp2, ref p1, ref p2, ref p3, ref p4, ref obbuffer);//Find Ry4S

            string strRet;
            listBox1.Items.Clear();
            if (0 != ret)
            {
                strRet = string.Format("Failed to find Rockey4Smart! Error code:{0:G}", ret);
                listBox1.Items.Add(strRet);
                return;
            }
            uiarrRy4ID[iMaxRockey] = uLp1;
            iMaxRockey++;

            ulong flag = 0;
            while (flag == 0)
            {//Find next Ry4S
                flag = Ry4S.Rockey((ushort)Ry4Cmd.RY_FIND_NEXT, ref uHandle, ref uLp1, ref uLp2, ref p1, ref p2, ref p3, ref p4, ref obbuffer);

                if (flag == 0)
                {
                    uiarrRy4ID[iMaxRockey] = uLp1;
                    iMaxRockey++;
                }

            }

            strRet = "Found " + iMaxRockey + " Ry4S";
            listBox1.Items.Add(strRet);

            for (int i = 0; i < iMaxRockey; i++)
            {

                strRet = string.Format("({0}): {1:X8}", i + 1, uiarrRy4ID[i]);
                listBox1.Items.Add(strRet);

            }

            iCurrID = uiarrRy4ID[0];
            for (int n = 0; n < iMaxRockey; n++)
            {
                uLp1 = uiarrRy4ID[n];

                p1 = 0xC44C;
                p2 = 0xC8F8;
                p3 = 0x0799;
                p4 = 0xC43B;
                //Open Rockey4Smart
                ret = Ry4S.Rockey((ushort)Ry4Cmd.RY_OPEN, ref uHandle, ref uLp1, ref uLp2, ref p1, ref p2, ref p3, ref p4, ref obbuffer);

                if (0 != ret)
                {
                    strRet = string.Format("Failed to open Rockey4Smart! Error code:", ret);
                    listBox1.Items.Add(strRet);
                    return;
                }
                strRet = string.Format("Open Rockey4Smart successfully!");
                listBox1.Items.Add(strRet);

                //Get version
                ret = Ry4S.Rockey((ushort)Ry4Cmd.RY_VERSION, ref uHandle, ref uLp1, ref uLp2, ref p1, ref p2, ref p3, ref p4, ref obbuffer);

                if (0 != ret)
                {
                    strRet = string.Format("Failed to get version!Error code: ", ret);
                    listBox1.Items.Add(strRet);
                    return;
                }

                if (uLp2 < 259)
                {
                    strRet = string.Format("Old dongle doesn't support new TIME and COUNT interface,please use V1.03 for test!", ret);
                    listBox1.Items.Add(strRet);
                    return;
                }

                //set count authorization
                p1 = 0; //unit 0
                p2 = 10; //10 hours
                p3 = 1; //allow decrease or not.0 is not allowed,1 is allowed
                p4 = 0; //0 means hour, 1 means date 

                Int32 uCount = 10;
                obbuffer = uCount;
                ret = Ry4S.Rockey((ushort)Ry4Cmd.RY_SET_COUNTER_EX, ref uHandle, ref uLp1, ref uLp2, ref p1, ref p2, ref p3, ref p4, ref obbuffer);

                if (0 != ret)
                {
                    strRet = string.Format("Failed to set count authorization! Error code:{0:G}", ret);
                    listBox1.Items.Add(strRet);
                    return;
                }
                strRet = string.Format("Set count successfully!");
                listBox1.Items.Add(strRet);

                strRet = string.Format("Set��Unit:{0:G} ", p1);
                listBox1.Items.Add(strRet);
                strRet = string.Format("Set��Value:{0:G} ", p2);
                listBox1.Items.Add(strRet);

                //get count authorization
                p2 = 0;
                obbuffer = new object();
                ret = Ry4S.Rockey((ushort)Ry4Cmd.RY_GET_COUNTER_EX, ref uHandle, ref uLp1, ref uLp2, ref p1, ref p2, ref p3, ref p4, ref obbuffer);
                uCount = (Int32)obbuffer;

                if (0 != ret)
                {
                    strRet = string.Format("Failed to get count authorization!Error code:{0:G}", ret);
                    listBox1.Items.Add(strRet);
                    return;
                }
                strRet = string.Format("Get count successfully!");
                listBox1.Items.Add(strRet);

                strRet = string.Format("Get count��Unit:{0:G} ", p1);
                listBox1.Items.Add(strRet);
                strRet = string.Format("Get count��Value:{0:G} ", uCount);
                listBox1.Items.Add(strRet);

            }

        }
    }
}