------------------ ROCKEY4SMART Software Developer's Kit --------------------

This folder comprises the COM samples for C#

Notes:
    Before use the sample, please register the COM components in DOS 
	commandline console.
    Register:
        regsvr32 Ry4SCom.dll
    Unregister:
        regsvr32 /u Ry4SCom.dll

------------------ Folder Content List --------------------------------------

COM             Common function sample
Com_Count       Sample to show how to use timer unit in C#
Com_Time        Sample to show how to use counter unit in C#

 
