#include <windows.h>
#include <stdio.h>
#include <conio.h>
#include "time.h"
#include "ry4s.h"



void ShowERR(WORD retcode)
{
	if (retcode == 0) return;
	printf("Error Code: %d\n", retcode);
}
void HexBufferToString(char* str,BYTE* buf,int len)
{
	int k;
	sprintf(str,"0x");
	for( k=0;k<len;k++)
	{
		char strTemp[20];
		sprintf(strTemp,"%02x",buf[k]);
		strcat(str,strTemp);
	}
}
void SetN(BYTE* buffer)
{
	buffer[0]=(char)191;	buffer[1]=(char)25;		buffer[2]=(char)33;		buffer[3]=(char)32;
	buffer[4]=(char)213;	buffer[5]=(char)241;	buffer[6]=(char)45;		buffer[7]=(char)44;
	buffer[8]=(char)252;	buffer[9]=(char)169;	buffer[10]=(char)197;	buffer[11]=(char)176;
	buffer[12]=(char)59;	buffer[13]=(char)209;	buffer[14]=(char)241;	buffer[15]=(char)92;
	buffer[16]=(char)142;	buffer[17]=(char)136;	buffer[18]=(char)144;	buffer[19]=(char)40;
	buffer[20]=(char)170;	buffer[21]=(char)94;	buffer[22]=(char)147;	buffer[23]=(char)97;
	buffer[24]=(char)185;	buffer[25]=(char)135;	buffer[26]=(char)234;	buffer[27]=(char)134;
	buffer[28]=(char)143;	buffer[29]=(char)84;	buffer[30]=(char)152;	buffer[31]=(char)23;
	buffer[32]=(char)27;	buffer[33]=(char)72;	buffer[34]=(char)238;	buffer[35]=(char)245;
	buffer[36]=(char)2;		buffer[37]=(char)144;	buffer[38]=(char)91;	buffer[39]=(char)70;
	buffer[40]=(char)57;	buffer[41]=(char)20;	buffer[42]=(char)192;	buffer[43]=(char)98;
	buffer[44]=(char)77;	buffer[45]=(char)31;	buffer[46]=(char)111;	buffer[47]=(char)227;
	buffer[48]=(char)16;	buffer[49]=(char)115;	buffer[50]=(char)182;	buffer[51]=(char)211;
	buffer[52]=(char)95;	buffer[53]=(char)143;	buffer[54]=(char)108;	buffer[55]=(char)241;
	buffer[56]=(char)115;	buffer[57]=(char)2;		buffer[58]=(char)248;	buffer[59]=(char)62;
	buffer[60]=(char)121;	buffer[61]=(char)134;	buffer[62]=(char)55;	buffer[63]=(char)10;
	buffer[64]=(char)30;	buffer[65]=(char)74;	buffer[66]=(char)145;	buffer[67]=(char)132;
	buffer[68]=(char)79;	buffer[69]=(char)159;	buffer[70]=(char)156;	buffer[71]=(char)18;
	buffer[72]=(char)223;	buffer[73]=(char)175;	buffer[74]=(char)84;	buffer[75]=(char)197;
	buffer[76]=(char)130;	buffer[77]=(char)17;	buffer[78]=(char)95;	buffer[79]=(char)230;
	buffer[80]=(char)200;	buffer[81]=(char)202;	buffer[82]=(char)68;	buffer[83]=(char)202;
	buffer[84]=(char)132;	buffer[85]=(char)22;	buffer[86]=(char)150;	buffer[87]=(char)18;
	buffer[88]=(char)193;	buffer[89]=(char)127;	buffer[90]=(char)10;	buffer[91]=(char)42;
	buffer[92]=(char)158;	buffer[93]=(char)129;	buffer[94]=(char)185;	buffer[95]=(char)153;
	buffer[96]=(char)219;	buffer[97]=(char)226;	buffer[98]=(char)70;	buffer[99]=(char)56;
	buffer[100]=(char)100;	buffer[101]=(char)74;	buffer[102]=(char)149;	buffer[103]=(char)14;
	buffer[104]=(char)92;	buffer[105]=(char)14;	buffer[106]=(char)201;	buffer[107]=(char)106;
	buffer[108]=(char)93;	buffer[109]=(char)221;	buffer[110]=(char)96;	buffer[111]=(char)221;
	buffer[112]=(char)148;	buffer[113]=(char)233;	buffer[114]=(char)46;	buffer[115]=(char)75;
	buffer[116]=(char)195;	buffer[117]=(char)70;	buffer[118]=(char)77;	buffer[119]=(char)157;
	buffer[120]=(char)54;	buffer[121]=(char)99;	buffer[122]=(char)95;	buffer[123]=(char)233;
	buffer[124]=(char)8;	buffer[125]=(char)180;	buffer[126]=(char)4;	buffer[127]=(char)178;
}
void SetD(BYTE* buffer)
{
	buffer[0]=(char)117;	buffer[1]=(char)86;		buffer[2]=(char)160;	buffer[3]=(char)104;
	buffer[4]=(char)137;	buffer[5]=(char)51;		buffer[6]=(char)8;		buffer[7]=(char)4;
	buffer[8]=(char)73;		buffer[9]=(char)178;	buffer[10]=(char)136;	buffer[11]=(char)200;
	buffer[12]=(char)145;	buffer[13]=(char)26;	buffer[14]=(char)66;	buffer[15]=(char)84;
	buffer[16]=(char)48;	buffer[17]=(char)143;	buffer[18]=(char)169;	buffer[19]=(char)74;
	buffer[20]=(char)251;	buffer[21]=(char)86;	buffer[22]=(char)25;	buffer[23]=(char)188;
	buffer[24]=(char)162;	buffer[25]=(char)3;		buffer[26]=(char)162;	buffer[27]=(char)131;
	buffer[28]=(char)107;	buffer[29]=(char)81;	buffer[30]=(char)3;		buffer[31]=(char)49;
	buffer[32]=(char)251;	buffer[33]=(char)133;	buffer[34]=(char)157;	buffer[35]=(char)164;
	buffer[36]=(char)147;	buffer[37]=(char)138;	buffer[38]=(char)72;	buffer[39]=(char)81;
	buffer[40]=(char)111;	buffer[41]=(char)137;	buffer[42]=(char)65;	buffer[43]=(char)48;
	buffer[44]=(char)154;	buffer[45]=(char)11;	buffer[46]=(char)42;	buffer[47]=(char)215;
	buffer[48]=(char)188;	buffer[49]=(char)180;	buffer[50]=(char)175;	buffer[51]=(char)54;
	buffer[52]=(char)79;	buffer[53]=(char)40;	buffer[54]=(char)163;	buffer[55]=(char)60;
	buffer[56]=(char)123;	buffer[57]=(char)20;	buffer[58]=(char)96;	buffer[59]=(char)196;
	buffer[60]=(char)188;	buffer[61]=(char)10;	buffer[62]=(char)254;	buffer[63]=(char)213;
	buffer[64]=(char)17;	buffer[65]=(char)244;	buffer[66]=(char)224;	buffer[67]=(char)61;
	buffer[68]=(char)197;	buffer[69]=(char)77;	buffer[70]=(char)6;		buffer[71]=(char)3;
	buffer[72]=(char)22;	buffer[73]=(char)32;	buffer[74]=(char)157;	buffer[75]=(char)117;
	buffer[76]=(char)92;	buffer[77]=(char)218;	buffer[78]=(char)114;	buffer[79]=(char)245;
	buffer[80]=(char)73;	buffer[81]=(char)10;	buffer[82]=(char)47;	buffer[83]=(char)221;
	buffer[84]=(char)35;	buffer[85]=(char)24;	buffer[86]=(char)123;	buffer[87]=(char)235;
	buffer[88]=(char)79;	buffer[89]=(char)237;	buffer[90]=(char)175;	buffer[91]=(char)153;
	buffer[92]=(char)209;	buffer[93]=(char)193;	buffer[94]=(char)156;	buffer[95]=(char)80;
	buffer[96]=(char)253;	buffer[97]=(char)255;	buffer[98]=(char)54;	buffer[99]=(char)169;
	buffer[100]=(char)111;	buffer[101]=(char)113;	buffer[102]=(char)196;	buffer[103]=(char)202;
	buffer[104]=(char)121;	buffer[105]=(char)77;	buffer[106]=(char)27;	buffer[107]=(char)100;
	buffer[108]=(char)179;	buffer[109]=(char)12;	buffer[110]=(char)211;	buffer[111]=(char)140;
	buffer[112]=(char)217;	buffer[113]=(char)44;	buffer[114]=(char)182;	buffer[115]=(char)71;
	buffer[116]=(char)208;	buffer[117]=(char)247;	buffer[118]=(char)92;	buffer[119]=(char)218;
	buffer[120]=(char)41;	buffer[121]=(char)105;	buffer[122]=(char)234;	buffer[123]=(char)216;
	buffer[124]=(char)85;	buffer[125]=(char)118;	buffer[126]=(char)54;	buffer[127]=(char)8;
}

void main()
{
	WORD handle[16], p1, p2, p3, p4, retcode;
	DWORD lp1, lp2;
	BYTE buffer[1024];
	WORD rc[4];
	int i, j,k;
	int nKeyLen[2]={8,16} ;
	unsigned char keybuf[16];
	char str[1024];

	
	char cmd[] = "H=H^H, A=A*23, F=B*17, A=A+F, A=A+G, A=A<C, A=A^D, B=B^B, C=C^C, D=D^D";
	char cmd1[] = "A=A+B, A=A+C, A=A+D, A=A+E, A=A+F, A=A+G, A=A+H";
	char cmd2[] = "A=E|E, B=F|F, C=G|G, D=H|H";

	p1 = 0xc44c;
	p2 = 0xc8f8;
	p3 = 0x0799;
	p4 = 0xc43b;


	//Find Rockey4Smart with specified password
	retcode = Rockey(RY_FIND, &handle[0], &lp1, &lp2, &p1, &p2, &p3, &p4, buffer);
	if (retcode)
	{
		ShowERR(retcode);
		return;
	}
	printf("Find Rock: %08X\n", lp1);

	//Open Rockey4Smart
	retcode = Rockey(RY_OPEN, &handle[0], &lp1, &lp2, &p1, &p2, &p3, &p4, buffer);
	if (retcode)
	{
		ShowERR(retcode);
		return;
	}

	i = 1;
	while (retcode == 0)
	{
		//Find next Rockey4Smart
		retcode = Rockey(RY_FIND_NEXT, &handle[i], &lp1, &lp2, &p1, &p2, &p3, &p4, buffer);
		if (retcode == ERR_NOMORE) break;
		if (retcode)
		{
			ShowERR(retcode);
			return;
		}

		retcode = Rockey(RY_OPEN, &handle[i], &lp1, &lp2, &p1, &p2, &p3, &p4, buffer);
		if (retcode)
		{
			ShowERR(retcode);
			return;
		}

		i++;
		printf("Find Rock: %08X\n", lp1);
	}
	printf("\n");

	//Operate on each dongle
	for (j=0;j<i;j++)
	{
		p1 = 3;
		p2 = 6;
		strcpy((char*)buffer, "Hello1");
		//Write data to user memory
		retcode = Rockey(RY_WRITE, &handle[j], &lp1, &lp2, &p1, &p2, &p3, &p4, buffer);
		if (retcode)
		{
			ShowERR(retcode);
			return;
		}
		printf("Write: Hello1\n");

		p1 = 3;
		p2 = 6;
		memset(buffer, 0, 64);
		//Read data from user memory
		retcode = Rockey(RY_READ, &handle[j], &lp1, &lp2, &p1, &p2, &p3, &p4, buffer);
		if (retcode)
		{
			ShowERR(retcode);
			return;
		}
		printf("Read: %s\n", buffer);

	
        //Generate random number
		retcode = Rockey(RY_RANDOM, &handle[j], &lp1, &lp2, &p1, &p2, &p3, &p4, buffer);
		if (retcode)
		{
			ShowERR(retcode);
			return;
		}
		printf("Random: %04X,%04X,%04X,%04X\n", p1,p2,p3,p4);

		lp2 = 0x12345678;
		//Get return code of seed
		retcode = Rockey(RY_SEED, &handle[j], &lp1, &lp2, &p1, &p2, &p3, &p4, buffer);
		if (retcode)
		{
			ShowERR(retcode);
			return;
		}
		printf("Seed: %04X %04X %04X %04X\n", p1, p2, p3, p4);
		rc[0] = p1;
		rc[1] = p2;
		rc[2] = p3;
		rc[3] = p4;

		lp1 = 0x88888888;
		//Write User ID
		retcode = Rockey(RY_WRITE_USERID, &handle[j], &lp1, &lp2, &p1, &p2, &p3, &p4, buffer);
		if (retcode)
		{
			ShowERR(retcode);
			return;
		}
		printf("Write User ID: %08X\n", lp1);

		lp1 = 0;
		//Read User ID
		retcode = Rockey(RY_READ_USERID, &handle[j], &lp1, &lp2, &p1, &p2, &p3, &p4, buffer);
		if (retcode)
		{
			ShowERR(retcode);
			return;
		}
		printf("Read User ID: %08X\n", lp1);

		p1 = 7;     //module number
		p2 = 0x2121;//module word
		p3 = 0;     //not allow decrease

		//Set module
		retcode = Rockey(RY_SET_MODULE, &handle[j], &lp1, &lp2, &p1, &p2, &p3, &p4, buffer);
		if (retcode)
		{
			ShowERR(retcode);
			return;
		}
		printf("Set Moudle 7: Module word= %04X, Decrease not allow\n", p2);

		p1 = 7;
		//Check module
		retcode = Rockey(RY_CHECK_MODULE, &handle[j], &lp1, &lp2, &p1, &p2, &p3, &p4, buffer);
		if (retcode)
		{
			ShowERR(retcode);
			return;
		}
		printf("Check Module 7: \n");
		if (p2) 
			printf("The module is valid\n");
		else 
			printf("The module is not valid\n");
		if (p3) 
			printf("Allow Decrease\n");
		else 
			printf("Not Allow Decrease\n");

		p1 = 0;
		strcpy((char*)buffer, cmd);
		//Write user defined arithmetic to dongle
		retcode = Rockey(RY_WRITE_ARITHMETIC, &handle[j], &lp1, &lp2, &p1, &p2, &p3, &p4, buffer);
		if (retcode)
		{
			ShowERR(retcode);
			return;
		}
		printf("Write Arithmetic 1\n");

		//lp1 = 127;
		lp1 = 0;
		lp2 = 7;
		p1 = 5;
		p2 = 3;
		p3 = 1;
		p4 = 0xffff;
		//Calculate,the result is decided by user's arithmetic wrote to dongle
		retcode = Rockey(RY_CALCULATE1, &handle[j], &lp1, &lp2, &p1, &p2, &p3, &p4, buffer);
		if (retcode)
		{
			ShowERR(retcode);
			return;
		}
		printf("Calculate Input: p1=5, p2=3, p3=1, p4=0xffff\n");
		printf("Result = ((5*23 + 3*17 + 0x2121) < 1) ^ 0xffff = BC71\n");
		printf("Calculate Output: p1=%x, p2=%x, p3=%x, p4=%x\n", p1, p2, p3, p4);

		p1 = 10;
		strcpy((char*)buffer, cmd1);
		retcode = Rockey(RY_WRITE_ARITHMETIC, &handle[j], &lp1, &lp2, &p1, &p2, &p3, &p4, buffer);
		if (retcode)
		{
			ShowERR(retcode);
			return;
		}
		printf("Write Arithmetic 2\n");

		lp1 = 10;
		lp2 = 0x12345678;
		p1 = 1;
		p2 = 2;
		p3 = 3;
		p4 = 4;
		retcode = Rockey(RY_CALCULATE2, &handle[j], &lp1, &lp2, &p1, &p2, &p3, &p4, buffer);
		if (retcode)
		{
			ShowERR(retcode);
			return;
		}
		printf("Calculate Input: p1=1, p2=2, p3=3, p4=4\n");
		printf("Result = %04x + %04x + %04x + %04x + 1 + 2 + 3 + 4 = %04x\n", rc[0], rc[1], rc[2], rc[3], (WORD)(rc[0]+rc[1]+rc[2]+rc[3]+10));
		printf("Calculate Output: p1=%x, p2=%x, p3=%x, p4=%x\n", p1, p2, p3, p4);

		// Set Decrease
		p1 = 9;
		p2 = 0x5;
		p3 = 1;   //allow decrease
		retcode = Rockey(RY_SET_MODULE, &handle[j], &lp1, &lp2, &p1, &p2, &p3, &p4, buffer);
		if (retcode)
		{
			ShowERR(retcode);
			return;
		}

		p1 = 17;
		strcpy((char*)buffer, cmd2);
		retcode = Rockey(RY_WRITE_ARITHMETIC, &handle[j], &lp1, &lp2, &p1, &p2, &p3, &p4, buffer);
		if (retcode)
		{
			ShowERR(retcode);
			return;
		}
		printf("Write Arithmetic 3\n");

		lp1 = 17;
		lp2 = 6;
		p1 = 1;
		p2 = 2;
		p3 = 3;
		p4 = 4;
		retcode = Rockey(RY_CALCULATE3, &handle[j], &lp1, &lp2, &p1, &p2, &p3, &p4, buffer);
		if (retcode)
		{
			ShowERR(retcode);
			return;
		}
		printf("Show Module from 6: p1=%x, p2=%x, p3=%x, p4=%x\n", p1, p2, p3, p4);

		// Decrease module
		p1 = 9;
		retcode = Rockey(RY_DECREASE, &handle[j], &lp1, &lp2, &p1, &p2, &p3, &p4, buffer);
		if (retcode)
		{
			ShowERR(retcode);
			return;
		}
		printf("Decrease module 9\n");

		lp1 = 17;
		lp2 = 6;
		p1 = 1;
		p2 = 2;
		p3 = 3;
		p4 = 4;
		retcode = Rockey(RY_CALCULATE3, &handle[j], &lp1, &lp2, &p1, &p2, &p3, &p4, buffer);
		if (retcode)
		{
			ShowERR(retcode);
			return;
		}
		printf("Show Module from 6: p1=%x, p2=%x, p3=%x, p4=%x\n", p1, p2, p3, p4);

		//DES test
		printf("des test start!\n");

		//set DES key buffer
		p1=DES_SINGLE_MODE;          //single DES
		for(k=0;k<nKeyLen[p1];k++)
		{
			keybuf[k]=k; //des key
		}

		//set DES key
		retcode = Rockey(RY_SET_DES_KEY, &handle[j], &lp1, &lp2, &p1, &p2, &p3, &p4, keybuf);
		if (retcode)
		{
			ShowERR(retcode);
			return;
		}

		//Length of data to be encrypted,must be multiple of 8
		p2 = 24;
		
		for(k=0;k<p2;k++)
		{
			//Data to be encrypted
			buffer[k]='A';
		}		
		printf("des plane data:\n");
		HexBufferToString(str,buffer,p2);
		printf(str);
		printf("\n");

		//DES encryption
		retcode = Rockey(RY_DES_ENC, &handle[j], &lp1, &lp2, &p1, &p2, &p3, &p4, buffer);

		if (retcode)
		{
			ShowERR(retcode);
			return;
		}
		printf("des cipher data:\n");
		HexBufferToString(str,buffer,p2);
		printf(str);
		printf("\n");
		//DES decryption
		retcode = Rockey(RY_DES_DEC, &handle[j], &lp1, &lp2, &p1, &p2, &p3, &p4,buffer);
		if (retcode)
		{
			ShowERR(retcode);
			return;
		}
		printf("des decryption result:\n");
		HexBufferToString(str,buffer,p2);
		printf(str);
		printf("\n");

		printf("des test over!\n");


		//RSA test
		printf("rsa test start!\n");
		
		//set key N
		SetN(buffer);
		retcode = Rockey(RY_SET_RSAKEY_N, &handle[j], &lp1, &lp2, &p1, &p2, &p3, &p4,buffer);
		if (retcode)
		{
			ShowERR(retcode);
			return;
		}

		//set key D
		SetD(buffer);
		retcode = Rockey(RY_SET_RSAKEY_D, &handle[j], &lp1, &lp2, &p1, &p2, &p3, &p4,buffer);
		if (retcode)
		{
			ShowERR(retcode);
			return;
		}
		//set rude data to be encrypted
		memset(buffer,0,8);

		//set pad type
		p2 = 120;
		for(k=0;k<p2-1;k++)
		{
			buffer[k] = 'A';
		}	
        buffer[119] = '\0';
		printf("rsa plane data:\n");
		HexBufferToString(str,buffer,p2);
		printf(str);
		printf("\n");
		p3 = RSA_ROCKEY_PADDING;

		//rsa public key encrypt
		p1 = RSA_PUBLIC_KEY;
		retcode = Rockey(RY_RSA_ENC, &handle[j], &lp1, &lp2, &p1, &p2, &p3, &p4,buffer);
		if (retcode)
		{
			ShowERR(retcode);
			return;
		}
		
		printf("rsa cipher data:\n");
		HexBufferToString(str,buffer,128);
		printf(str);
		printf("\n");
		//rsa private key decrypt
		p1 = RSA_PRIVATE_KEY;
		retcode = Rockey(RY_RSA_DEC, &handle[j], &lp1, &lp2, &p1, &p2, &p3, &p4,buffer);
		if (retcode)
		{
			ShowERR(retcode);
			return;
		}
		printf("rsa decryption result:\n");
		HexBufferToString(str,buffer,p2);
		printf(str);
		printf("\n");

		printf("rsa test over!\n");
		
		//Close Rockey4Smart handle
		retcode = Rockey(RY_CLOSE, &handle[j], &lp1, &lp2, &p1, &p2, &p3, &p4, buffer);
		if (retcode)
		{
			ShowERR(retcode);
			return;
		}

		getch();
	}
}
