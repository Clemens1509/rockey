// 高级命令字
#ifndef _ROCKEY4_SM_32_
#define _ROCKEY4_SM_32_
//函数id
/* 基本格式:
(1) 查找锁
    输入参数:
    function = 1
    *p1 = pass1
    *p2 = pass2
    *p3 = pass3
    *p4 = pass4
    返回:
    *lp1 为锁的硬件 ID
    返回为 0 表示成功, 其它为错误码

(2) 查找下一锁
    输入参数:
    function = 2
    *p1 = pass1
    *p2 = pass2
    *p3 = pass3
    *p4 = pass4
    返回:
    *lp1 为锁的硬件 ID
    返回为 0 表示成功, 其它为错误码

(3) 打开锁
    输入参数:
    function = 3
    *p1 = pass1
    *p2 = pass2
    *p3 = pass3
    *p4 = pass4
    *lp1 = 硬件 ID
    返回:
    *handle 为锁的句柄
    返回为 0 表示成功, 其它为错误码

(4) 关闭锁
    输入参数:
    function = 4
    *handle = 锁的句柄
    返回:
    返回为 0 表示成功, 其它为错误码

(5) 读锁
    输入参数:
    function = 5
    *handle = 锁的句柄
    *p1 = pos
    *p2 = length
    buffer = 缓冲区的指针
    返回:
    buffer 中添入读入的内容
    返回为 0 表示成功, 其它为错误码

(6) 写锁
    function = 6
    *handle = 锁的句柄
    *p1 = pos
    *p2 = length
    buffer = 缓冲区的指针
    返回:
    返回为 0 表示成功, 其它为错误码

(7) 随机数
    function = 7
    *handle = 锁的句柄
    返回:
    *p1,*p2,*p3,*p4 = 随机数
    返回为 0 表示成功, 其它为错误码
    
(8) 种子码
    function = 8
    *handle = 锁的句柄
    *lp2 = 种子码
    返回:
    *p1 = 返回码1
    *p2 = 返回码2
    *p3 = 返回码3
    *p4 = 返回码4
    返回为 0 表示成功, 其它为错误码

(9) 写用户 ID [*]
    function = 9
    *handle = 锁的句柄
    *lp1 = 用户 ID
    返回:
    返回为 0 表示成功, 其它为错误码

(10) 读用户 ID
     function = 10
     *handle = 锁的句柄
     返回:
     *lp1 = 用户 ID
     返回为 0 表示成功, 其它为错误码

(11) 设置模块 [*]
     function = 11
     *handle = 锁的句柄
     *p1 = 模块号
     *p2 = 用户模块密码
     *p3 = 是否允许递减 (1 = 允许, 0 = 不允许)
     返回:
     返回为 0 表示成功, 其它为错误码

(12) 检查模块是否有效
     function = 12
     *handle = 锁的句柄
     *p1 = 模块号
     返回:
     *p2 = 1 表示此模块有效
     *p3 = 1 表示此模块可以递减
     返回为 0 表示成功, 其它为错误码

(13) 写算法 [*]
     function = 13
     *handle = 锁的句柄
     *p1 = pos
     buffer = 算法指令串
     返回:
     返回为 0 表示成功, 其它为错误码
     
(14) 计算1 (模块字, ID 高位, ID 低位, 随机数)
     function = 14
     *handle = 锁的句柄
     *lp1 = 计算起始点
     *lp2 = 模块号
     *p1 = 输入值1
     *p2 = 输入值2
     *p3 = 输入值3
     *p4 = 输入值4
     返回:
     *p1 = 返回值1
     *p2 = 返回值2
     *p3 = 返回值3
     *p4 = 返回值4
     返回为 0 表示成功, 其它为错误码

(15) 计算2
     function = 15
     *handle = 锁的句柄
     *lp1 = 计算起始点
     *lp2 = 种子码
     *p1 = 输入值1
     *p2 = 输入值2
     *p3 = 输入值3
     *p4 = 输入值4
     返回:
     *p1 = 返回值1
     *p2 = 返回值2
     *p3 = 返回值3
     *p4 = 返回值4
     返回为 0 表示成功, 其它为错误码

(16) 计算3
     function = 16
     *handle = 锁的句柄
     *lp1 = 计算起始点
     *lp2 = 密码起始地址
     *p1 = 输入值1
     *p2 = 输入值2
     *p3 = 输入值3
     *p4 = 输入值4
     返回:
     *p1 = 返回值1
     *p2 = 返回值2
     *p3 = 返回值3
     *p4 = 返回值4
     返回为 0 表示成功, 其它为错误码

(17) 递减
     function = 17
     *handle = 锁的句柄
     *p1 = 模块号
     返回为 0 表示成功, 其它为错误码
*/

#define  RY_FIND                        1       //找锁
#define  RY_FIND_NEXT                   2       //找下一锁
#define  RY_OPEN                        3       //打开锁
#define  RY_CLOSE                       4       //关闭锁
#define  RY_READ                        5       //读锁
#define  RY_WRITE                       6       //写锁
#define  RY_RANDOM                      7       //产生随机数
#define  RY_SEED                        8       //产生种子码
#define  RY_WRITE_USERID                9       //写用户 ID
#define  RY_READ_USERID                 10      //读用户 ID
#define  RY_SET_MODULE                  11      //设置模块字
#define  RY_CHECK_MODULE                12      //检查模块状态
#define  RY_WRITE_ARITHMETIC            13      //写算法
#define  RY_CALCULATE1                  14      //计算 1
#define  RY_CALCULATE2                  15      //计算 2
#define  RY_CALCULATE3                  16      //计算 3
#define  RY_DECREASE                    17      //递减模块单元

#define  RY_SET_COUNTER                 20      //设置计数器
#define  RY_GET_COUNTER                 21      //取计数器
#define  RY_DEC_COUNTER                 22

//授权类型定义 
#define  FT_LICENSE_HOUR				0				//小时授权，超过可使用的小时后授权无效
#define  FT_LICENSE_DATE				2				//日期授权，超过截止日期后授权无效
#define  FT_LICENSE_DAY					4				//天数授权，超过可使用的天数后授权无效

#define  RY_SET_TIMER                   23      //设置时钟
#define  RY_GET_TIMER                   24      //取时钟
#define  RY_ADJUST_TIMER                25      //调整系统时钟
#define  RY_SET_TIMER_ITV               26
#define  RY_GET_TIMER_ITV               27
#define  RY_DEC_TIMER                   28
#define  RY_SET_RSAKEY_N                29      //写入RSA N和D
#define  RY_SET_RSAKEY_D                30      //写入RSA N和D
#define  RY_UPDATE_GEN_HEADER           31      //生成文件头密文
#define  RY_UPDATE_GEN                  32      //生成文件内容密文
#define  RY_UPDATE_CHECK                33      //升级密文文件
#define  RY_UPDATE                      34      //升级密文文件
#define  RY_UNPACK                      35

#define  RY_SET_DES_KEY                 41      //设置DES密钥
#define  RY_DES_ENC                     42      //DES加密
#define  RY_DES_DEC                     43      //DES解密
#define  RY_RSA_ENC                     44      //RSA加密       
#define  RY_RSA_DEC                     45      //RSA解密

//以下为新增接口
#define RY_READ_EX                      46      //新锁新接口读内存
#define RY_WRITE_EX                     47      //新锁新接口写内存

#define RY_SET_COUNTER_EX               0xA0    //设置计数器，值类型由WORD改为DWORD
#define RY_GET_COUNTER_EX               0xA1    //获取计数器，值类型由WORD改为DWORD
#define RY_SET_TIMER_EX                 0xA2    //设置计时单元时钟，时间类型由WORD改为DWORD
#define RY_GET_TIMER_EX                 0xA3    //获取计时单元时钟，时间类型由WORD改为DWORD
#define RY_ADJUST_TIMER_EX              0xA4    //调整系统时间，时间类型由WORD改为DWORD
#define RY_UPDATE_GEN_HEADER_EX         0xA5    //生成升级文件头，专用升级RSA密钥对
#define RY_UPDATE_GEN_EX                0xA6    //生成升级文件内容，专用升级RSA密钥对
#define RY_UPDATE_CHECK_EX              0xA7    //升级文件检测，专用升级RSA密钥对
#define RY_UPDATE_EX                    0xA8    //升级密文文件，专用升级RSA密钥对
#define RY_SET_UPDATE_KEY               0xA9    //设置升级RSA密钥对
#define RY_ADD_UPDATE_HEADER            0xAA    //添充授权文件头
#define RY_ADD_UPDATE_CONTENT           0xAB    //添充授权文件内容
#define RY_GET_TIME_DWORD               0xAC    //获取基于2006.1.1.0.0.0的DWORD值

#define  RY_VERSION                     100     //取得cos版本号

#define DES_SINGLE_MODE                 0
#define DES_TRIPLE_MODE                 1
#define RSA_PRIVATE_KEY                 0
#define RSA_PUBLIC_KEY                  1
#define RSA_ROCKEY_PADDING              0
#define RSA_USER_PADDING                1


// 错误码
#define ERR_SUCCESS                     0  //没有错误
#define ERR_NO_ROCKEY                   3  //没有ROCKEY
#define ERR_INVALID_PASSWORD            4  //有ROCKEY锁，但基本密码错
#define ERR_INVALID_PASSWORD_OR_ID      5  //错误的密码或硬件 ID
#define ERR_SETID                       6  //设置硬件 ID 错
#define ERR_INVALID_ADDR_OR_SIZE        7  //读写地址或长度有误
#define ERR_UNKNOWN_COMMAND             8  //没有此命令
#define ERR_NOTBELEVEL3                 9  //内部错误
#define ERR_READ                        10 //读数据错
#define ERR_WRITE                       11 //写数据错
#define ERR_RANDOM                      12 //随机数错
#define ERR_SEED                        13 //种子码错
#define ERR_CALCULATE                   14 //计算错
#define ERR_NO_OPEN                     15 //在操作前没有打开锁
#define ERR_OPEN_OVERFLOW               16 //打开的锁太多(>32)
#define ERR_NOMORE                      17 //找不到更多的锁
#define ERR_NEED_FIND                   18 //没有 Find 直接用了 FindNext
#define ERR_DECREASE                    19 //递减错
#define ERR_AR_BADCOMMAND               20 //算法指令错
#define ERR_AR_UNKNOWN_OPCODE           21 //算法运算符错
#define ERR_AR_WRONGBEGIN               22 //算法第一条指令含有常数
#define ERR_AR_WRONG_END                23 //算法最后一条指令含有常数
#define ERR_AR_VALUEOVERFLOW            24 //算法中常数值 > 63
#define ERR_TOOMUCHTHREAD               25 //同一个进程中打开锁的线程数 > 100
#define ERR_INVALID_RY4S                30 //试图对非Rockey4Smart的锁进行操作
#define ERR_INVALID_PARAMETER           31 //不合法的参数
#define ERR_INVALID_TIMEVALUE           32 //不合法的时间值

#define ERR_SET_DES_KEY                 40 //设置DES密钥错误
#define ERR_DES_ENCRYPT                 41 //DES 加密错误
#define ERR_DES_DECRYPT                 42 //DES 解密错误
#define ERR_SET_RSAKEY_N                43 //设置RSA密钥N错误
#define ERR_SET_RSAKEY_D                44 //设置RSA密钥D错误
#define ERR_RSA_ENCRYPT                 45 //RSA加密错误   
#define ERR_RSA_DECRYPT                 46 //RSA解密错误 
#define ERR_INVALID_LENGTH              47 //非法的数据长度

#define R4SERR_JUST_TIMER											 0x32 //校验时间出错
#define R4SERR_NO_SUCH_DEVICE							             0xA1 //没有找到给定要求的设备(参数错误)
#define R4SERR_NOT_OPENED_DEVICE						             0xA2 //在调用此功能前需要先调用 RY_Open 打开设备(操作错误)
#define R4SERR_WRONG_UID								             0xA3 //给出的 UID 错误(参数错误)
#define R4SERR_INDEX									             0xA4 //读写操作给出的块索引错误(参数错误)
#define R4SERR_TOO_LONG_SEED							             0xA5 //调用 GenUID 功能的时候，给出的 seed 字符串长度超过了 64 个字节(参数错误)
#define R4SERR_WRITE_PROTECT							             0xA6 //试图改写已经写保护的硬件(操作错误)
#define R4SERR_OPEN_DEVICE								             0xA7 //打开设备错(Windows 错误)
#define R4SERR_READ_REPORT								             0xA8 //读记录错(Windows 错误)
#define R4SERR_WRITE_REPORT											 0xA9 //写记录错(Windows 错误)
#define R4SERR_SETUP_DI_GET_DEVICE_INTERFACE_DETAIL	                 0xAA //内部错误(Windows 错误)
#define R4SERR_GET_ATTRIBUTES							             0xAB //内部错误(Windows 错误)
#define R4SERR_GET_PREPARSED_DATA						             0xAC //内部错误(Windows 错误)
#define R4SERR_GETCAPS									             0xAD //内部错误(Windows 错误)
#define R4SERR_FREE_PREPARSED_DATA						             0xAE //内部错误(Windows 错误)
#define R4SERR_FLUSH_QUEUE								             0xAF //内部错误(Windows 错误)
#define R4SERR_SETUP_DI_CLASS_DEVS						             0xB0 //内部错误(Windows 错误)
#define R4SERR_GET_SERIAL								             0xB1 //内部错误(Windows 错误)
#define R4SERR_GET_PRODUCT_STRING						             0xB2 //内部错误(Windows 错误)
#define R4SERR_TOO_LONG_DEVICE_DETAIL					             0xB3 //内部错误
#define R4SERR_UNKNOWN_DEVICE							             0xB4 //未知的设备(硬件错误)
#define R4SERR_VERIFY									             0xB5 //操作验证错误(硬件错误)
#define R4SERR_ERASE									             0xB6 //硬件内部擦除错误
#define	RY4SERR_INVALID_RY4S							             0xB7 //不是Rockey4Smart
#define R4SERR_UNKNOWN_ERROR							             0xFF //未知错误(硬件错误)

#define ERR_UPFILE_CRC	                                             0xC0 //升级文件CRC校验失败
#define	ERR_UPFILE_UID	                                             0xC1 //升级文件UID校验失败
#define ERR_UPFILE_HID	                                             0xC2 //升级文件HID校验失败
#define ERR_UPFILE_MOD	                                             0xC3 //Mod无效
#define	ERR_UPFILE_DATE                                              0xC4 //超过授权日期
#define ERR_UPDATE		                                             0xC5 //升级失败
#define ERR_UPDATE_OVERFLOW		                                     0xC6 //边界溢出
#define ERR_UPDATE_UNKNOWN_CMD	                                     0xC7 //未知升级项
#define ERR_UPDATE_INVALID	                        	             0xC8 //升级包无效

#define ERR_ACTIVATION                                               0xE0 //尚未激活或者激活失败
#define ERROR_PASSWORD	                                             0xE1 //验证密码错误
#define ERROR_DESCENDING_IS_NOT_ALLOWED                              0xE2 //不允许递减啦
#define ERROR_WITHOUT_DUE_AUTHORITY                                  0xE3 //没有响应的权限，也就是相应的密钥用户没有登录或者登录没有成功
#define ERROR_COUNT_MODULE_ISZERO                                    0xE4 //模块已经为零啦
#define ERROR_CHANGE_PWD                                             0xE5 //登录密码错误
#define ERR_INDEX		                                             0xE6 //单元索引错误
#define ERR_AUTH_TYPE	                                             0xE7 //错误授权类型
#define ERR_AUTH_VIRGIN                                              0xE8 //单元未初始化
#define ERR_AUTH_EXPIRED                                             0xE9 //授权已到期
#define	ERR_RSA			                                             0xEA //RSA类型错
#define R4S_ERR_UNKNOWN                                              0xEB //锁内未知错误
#define R4S_ERR_ADDR                                                 0xEC //地址错误

#define ERR_RECEIVE_NULL                                             0x100 //接收不到
#define ERR_INVALID_BUFFER                                           0x101 //空指针
#define ERR_UNKNOWN_SYSTEM                                           0x102 //未知的操作系统
#define ERROR_UNINIT_TIME_UNIT			                             0x103 //计时单元未初始化
#define ERR_UNKNOWN                                                  0xFFFF//未知错误
//其他为未知错误

EXTERN_C __declspec(dllexport) WORD WINAPI Rockey(WORD function, WORD* handle, DWORD* lp1,  DWORD* lp2, WORD* p1, WORD* p2, WORD* p3, WORD* p4, BYTE* buffer);

#endif























