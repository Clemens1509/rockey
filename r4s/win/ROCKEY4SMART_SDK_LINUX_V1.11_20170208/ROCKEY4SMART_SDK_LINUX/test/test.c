//#include <windows.h>
#include <stdio.h>
#include <string.h>
#include "rockey.h"
#include <time.h>

typedef unsigned short WORD;
typedef unsigned int DWORD;
typedef unsigned char BYTE;

typedef struct {
  WORD wYear;
  WORD wMonth;
  WORD wDayOfWeek;
  WORD wDay;
  WORD wHour;
  WORD wMinute;
  WORD wSecond;
  WORD wMilliseconds;
} SYSTEMTIME;

#define BUF_SIZE 2048

void ShowERR(WORD retcode)
{
	if (retcode == 0) return;
	printf("Error Code: %d\n", retcode);
}

void HexBufferToString(char* str,BYTE* buf,int len)
{
	int k;
	sprintf(str,"0x");
	for( k=0;k<len;k++)
	{
		char strTemp[1024];
		sprintf(strTemp,"%02x",buf[k]);
		strcat(str,strTemp);
	}
}

void SetN(BYTE* buffer)
{
	buffer[0]=(char)191;	buffer[1]=(char)25;	buffer[2]=(char)33;	buffer[3]=(char)32;
	buffer[4]=(char)213;	buffer[5]=(char)241;	buffer[6]=(char)45;	buffer[7]=(char)44;
	buffer[8]=(char)252;	buffer[9]=(char)169;	buffer[10]=(char)197;	buffer[11]=(char)176;
	buffer[12]=(char)59;	buffer[13]=(char)209;	buffer[14]=(char)241;	buffer[15]=(char)92;
	buffer[16]=(char)142;	buffer[17]=(char)136;	buffer[18]=(char)144;	buffer[19]=(char)40;
	buffer[20]=(char)170;	buffer[21]=(char)94;	buffer[22]=(char)147;	buffer[23]=(char)97;
	buffer[24]=(char)185;	buffer[25]=(char)135;	buffer[26]=(char)234;	buffer[27]=(char)134;
	buffer[28]=(char)143;	buffer[29]=(char)84;	buffer[30]=(char)152;	buffer[31]=(char)23;
	buffer[32]=(char)27;	buffer[33]=(char)72;	buffer[34]=(char)238;	buffer[35]=(char)245;
	buffer[36]=(char)2;	buffer[37]=(char)144;	buffer[38]=(char)91;	buffer[39]=(char)70;
	buffer[40]=(char)57;	buffer[41]=(char)20;	buffer[42]=(char)192;	buffer[43]=(char)98;
	buffer[44]=(char)77;	buffer[45]=(char)31;	buffer[46]=(char)111;	buffer[47]=(char)227;
	buffer[48]=(char)16;	buffer[49]=(char)115;	buffer[50]=(char)182;	buffer[51]=(char)211;
	buffer[52]=(char)95;	buffer[53]=(char)143;	buffer[54]=(char)108;	buffer[55]=(char)241;
	buffer[56]=(char)115;	buffer[57]=(char)2;	buffer[58]=(char)248;	buffer[59]=(char)62;
	buffer[60]=(char)121;	buffer[61]=(char)134;	buffer[62]=(char)55;	buffer[63]=(char)10;
	buffer[64]=(char)30;	buffer[65]=(char)74;	buffer[66]=(char)145;	buffer[67]=(char)132;
	buffer[68]=(char)79;	buffer[69]=(char)159;	buffer[70]=(char)156;	buffer[71]=(char)18;
	buffer[72]=(char)223;	buffer[73]=(char)175;	buffer[74]=(char)84;	buffer[75]=(char)197;
	buffer[76]=(char)130;	buffer[77]=(char)17;	buffer[78]=(char)95;	buffer[79]=(char)230;
	buffer[80]=(char)200;	buffer[81]=(char)202;	buffer[82]=(char)68;	buffer[83]=(char)202;
	buffer[84]=(char)132;	buffer[85]=(char)22;	buffer[86]=(char)150;	buffer[87]=(char)18;
	buffer[88]=(char)193;	buffer[89]=(char)127;	buffer[90]=(char)10;	buffer[91]=(char)42;
	buffer[92]=(char)158;	buffer[93]=(char)129;	buffer[94]=(char)185;	buffer[95]=(char)153;
	buffer[96]=(char)219;	buffer[97]=(char)226;	buffer[98]=(char)70;	buffer[99]=(char)56;
	buffer[100]=(char)100;	buffer[101]=(char)74;	buffer[102]=(char)149;	buffer[103]=(char)14;
	buffer[104]=(char)92;	buffer[105]=(char)14;	buffer[106]=(char)201;	buffer[107]=(char)106;
	buffer[108]=(char)93;	buffer[109]=(char)221;	buffer[110]=(char)96;	buffer[111]=(char)221;
	buffer[112]=(char)148;	buffer[113]=(char)233;	buffer[114]=(char)46;	buffer[115]=(char)75;
	buffer[116]=(char)195;	buffer[117]=(char)70;	buffer[118]=(char)77;	buffer[119]=(char)157;
	buffer[120]=(char)54;	buffer[121]=(char)99;	buffer[122]=(char)95;	buffer[123]=(char)233;
	buffer[124]=(char)8;	buffer[125]=(char)180;	buffer[126]=(char)4;	buffer[127]=(char)178;
}

void SetD(BYTE* buffer)
{
	buffer[0]=(char)117;	buffer[1]=(char)86;	buffer[2]=(char)160;	buffer[3]=(char)104;
	buffer[4]=(char)137;	buffer[5]=(char)51;	buffer[6]=(char)8;		buffer[7]=(char)4;
	buffer[8]=(char)73;	buffer[9]=(char)178;	buffer[10]=(char)136;	buffer[11]=(char)200;
	buffer[12]=(char)145;	buffer[13]=(char)26;	buffer[14]=(char)66;	buffer[15]=(char)84;
	buffer[16]=(char)48;	buffer[17]=(char)143;	buffer[18]=(char)169;	buffer[19]=(char)74;
	buffer[20]=(char)251;	buffer[21]=(char)86;	buffer[22]=(char)25;	buffer[23]=(char)188;
	buffer[24]=(char)162;	buffer[25]=(char)3;	buffer[26]=(char)162;	buffer[27]=(char)131;
	buffer[28]=(char)107;	buffer[29]=(char)81;	buffer[30]=(char)3;	buffer[31]=(char)49;
	buffer[32]=(char)251;	buffer[33]=(char)133;	buffer[34]=(char)157;	buffer[35]=(char)164;
	buffer[36]=(char)147;	buffer[37]=(char)138;	buffer[38]=(char)72;	buffer[39]=(char)81;
	buffer[40]=(char)111;	buffer[41]=(char)137;	buffer[42]=(char)65;	buffer[43]=(char)48;
	buffer[44]=(char)154;	buffer[45]=(char)11;	buffer[46]=(char)42;	buffer[47]=(char)215;
	buffer[48]=(char)188;	buffer[49]=(char)180;	buffer[50]=(char)175;	buffer[51]=(char)54;
	buffer[52]=(char)79;	buffer[53]=(char)40;	buffer[54]=(char)163;	buffer[55]=(char)60;
	buffer[56]=(char)123;	buffer[57]=(char)20;	buffer[58]=(char)96;	buffer[59]=(char)196;
	buffer[60]=(char)188;	buffer[61]=(char)10;	buffer[62]=(char)254;	buffer[63]=(char)213;
	buffer[64]=(char)17;	buffer[65]=(char)244;	buffer[66]=(char)224;	buffer[67]=(char)61;
	buffer[68]=(char)197;	buffer[69]=(char)77;	buffer[70]=(char)6;	buffer[71]=(char)3;
	buffer[72]=(char)22;	buffer[73]=(char)32;	buffer[74]=(char)157;	buffer[75]=(char)117;
	buffer[76]=(char)92;	buffer[77]=(char)218;	buffer[78]=(char)114;	buffer[79]=(char)245;
	buffer[80]=(char)73;	buffer[81]=(char)10;	buffer[82]=(char)47;	buffer[83]=(char)221;
	buffer[84]=(char)35;	buffer[85]=(char)24;	buffer[86]=(char)123;	buffer[87]=(char)235;
	buffer[88]=(char)79;	buffer[89]=(char)237;	buffer[90]=(char)175;	buffer[91]=(char)153;
	buffer[92]=(char)209;	buffer[93]=(char)193;	buffer[94]=(char)156;	buffer[95]=(char)80;
	buffer[96]=(char)253;	buffer[97]=(char)255;	buffer[98]=(char)54;	buffer[99]=(char)169;
	buffer[100]=(char)111;	buffer[101]=(char)113;	buffer[102]=(char)196;	buffer[103]=(char)202;
	buffer[104]=(char)121;	buffer[105]=(char)77;	buffer[106]=(char)27;	buffer[107]=(char)100;
	buffer[108]=(char)179;	buffer[109]=(char)12;	buffer[110]=(char)211;	buffer[111]=(char)140;
	buffer[112]=(char)217;	buffer[113]=(char)44;	buffer[114]=(char)182;	buffer[115]=(char)71;
	buffer[116]=(char)208;	buffer[117]=(char)247;	buffer[118]=(char)92;	buffer[119]=(char)218;
	buffer[120]=(char)41;	buffer[121]=(char)105;	buffer[122]=(char)234;	buffer[123]=(char)216;
	buffer[124]=(char)85;	buffer[125]=(char)118;	buffer[126]=(char)54;	buffer[127]=(char)8;
}

short r4s_update(unsigned short * handle)
{
	WORD retcode,p1, p2, p3, p4;
	DWORD lp1=0, lp2=0;
	BYTE buffer[BUF_SIZE]={0};
	BYTE RetBuf[BUF_SIZE]={0};
	int i =0;
	struct tm time;

	p1 = 0xc44c;
	p2 = 0xc8f8;
	p3 = 0x0799;
	p4 = 0xc43b;
    
	//Set update key pair
    SetN(buffer);
	SetD(buffer + 128);
	retcode = Rockey(RY_SET_UPDATE_KEY, &handle[0], &lp1, &lp2, &p1, &p2, &p3, &p4, buffer);
	if (retcode)
	{
		ShowERR(retcode);
		return retcode;
	}

    //Update time
	memset(buffer, 0, BUF_SIZE);
	lp2 = 2011;
	p1 = 11;
	p2 = 11;
	p3 = 11;
	p4 = 11;
	Rockey(RY_GET_TIME_DWORD, &handle[0], (DWORD *)&buffer[3 * 4], &lp2, &p1, &p2, &p3, &p4, NULL);

	lp1 = 0x00000008;
	Rockey(RY_ADD_UPDATE_HEADER, &handle[0], &lp1, &lp2, &p1, &p2, &p3, &p4, buffer);

	lp2 = 2012;  
	p1 = 10;
	p2 = 25;
	p3 = 10;
	p4 = 15;
	Rockey(RY_GET_TIME_DWORD, &handle[0], (DWORD *)buffer, &lp2, &p1, &p2, &p3, &p4, NULL);
	
	p1 = 1;
	p2 = 0;
	p3 = 1;		//date 2011-10-20 10:15 UTC
	Rockey(RY_ADD_UPDATE_CONTENT, &handle[0], &lp1, &lp2, &p1, &p2, &p3, &p4, buffer);

	p1 = 1;
	p2 = 1;
	p3 = 2;		//hour
	*(DWORD *)buffer = 24;
	Rockey(RY_ADD_UPDATE_CONTENT, &handle[0], &lp1, &lp2, &p1, &p2, &p3, &p4, buffer);
	p1 = 1;
	p2 = 2;
	p3 = 3;		//day
	*(DWORD *)buffer = 100;
	Rockey(RY_ADD_UPDATE_CONTENT, &handle[0], &lp1, &lp2, &p1, &p2, &p3, &p4, buffer);


	retcode = Rockey(RY_UPDATE_GEN_EX, &handle[0], &lp1, &lp2, &p1, &p2, &p3, &p4, NULL);
	p1 = 0xc44c;
	p2 = 0xc8f8;
	retcode = Rockey(RY_UPDATE_GEN_EX, &handle[0], &lp1, &lp2, &p1, &p2, &p3, &p4, RetBuf);
	if (retcode)
	{
		ShowERR(retcode);
		return retcode;
	}

	retcode = Rockey(RY_UPDATE_EX, &handle[0], &lp1, &lp2, &p1, &p2, &p3, &p4, RetBuf);
	if (retcode)
	{
		ShowERR(retcode);
		return retcode;
	}

	printf("Update time over\n");


	p1 = 0;
	retcode = Rockey(RY_GET_TIMER_EX, &handle[0], &lp1, &lp2, &p1, &p2, &p3, &p4, (BYTE*)(&time));
	if (retcode)
	{
		ShowERR(retcode);
		return retcode;
	}
	printf("UTC time:%d-%d-%d %d:%d\n",time.tm_year+1900,time.tm_mon-1,time.tm_mday,time.tm_hour,time.tm_min);

	p1 = 1;
	retcode = Rockey(RY_GET_TIMER_EX, &handle[0], &lp1, &lp2, &p1, &p2, &p3, &p4, buffer);
	if (retcode)
	{
		ShowERR(retcode);
		return retcode;
	}
	printf("time:%d,%d\n",p3,buffer[0]);

	p1 = 2;
	retcode = Rockey(RY_GET_TIMER_EX, &handle[0], &lp1, &lp2, &p1, &p2, &p3, &p4, (BYTE*)(&time));
	if (retcode)
	{
		ShowERR(retcode);
		return retcode;
	}
	printf("UTC time:%d-%d-%d %d:%d\n",time.tm_year+1900,time.tm_mon-1,time.tm_mday,time.tm_hour,time.tm_min);


/*
  //Update memory
	memset(buffer, 0, BUF_SIZE);
	lp2 = 2011;
	p1 = 11;
	p2 = 11;
	p3 = 17;
	p4 = 11;
	retcode = Rockey(RY_GET_TIME_DWORD, &handle[0], (DWORD *)&buffer[3 * 4], &lp2, &p1, &p2, &p3, &p4, NULL);

	lp1 = 0x00000008;
	retcode = Rockey(RY_ADD_UPDATE_HEADER, &handle[0], &lp1, &lp2, &p1, &p2, &p3, &p4, buffer);
	for (i = 0; i < 128; i++)
	{
		buffer[i] = 3;// * (BYTE)i;
	}
	p1 = 0;   //flag,0:update memory,1:upate time unit,2:update count unit,3:update arithmetic,4:update UID,5:update module word
	p2 = 0;   //offset of user memory
	p3 = 128; //length
	retcode = Rockey(RY_ADD_UPDATE_CONTENT, &handle[0], &lp1, &lp2, &p1, &p2, &p3, &p4, buffer);
	//Generate update file, that is encrypting file header and file content
	retcode = Rockey(RY_UPDATE_GEN_EX, &handle[0], &lp1, &lp2, &p1, &p2, &p3, &p4, NULL);

	p1 = 0xc44c;
	p2 = 0xc8f8;
	retcode = Rockey(RY_UPDATE_GEN_EX, &handle[0], &lp1, &lp2, &p1, &p2, &p3, &p4, RetBuf);
	if (retcode)
	{
		ShowERR(retcode);
		return retcode;
	}

	printf("Generate update file over.\n");

	//Update memory
	retcode = Rockey(RY_UPDATE_EX, &handle[0], &lp1, &lp2, &p1, &p2, &p3, &p4, RetBuf);
	if (retcode)
	{
		ShowERR(retcode);
		return retcode;
	}

	printf("Update memory over\n");

    //Update module word
	memset(buffer, 0, BUF_SIZE);
	lp2 = 2011;
	p1 = 11;
	p2 = 11;
	p3 = 11;
	p4 = 11;
	Rockey(RY_GET_TIME_DWORD, &handle[0], (DWORD *)&buffer[3 * 4], &lp2, &p1, &p2, &p3, &p4, NULL);
	lp1 = 0x00000008;
	Rockey(RY_ADD_UPDATE_HEADER, &handle[0], &lp1, &lp2, &p1, &p2, &p3, &p4, buffer);

	p1 = 5;
	p2 = 0;
	p3 = 1;
	*(WORD *)buffer = 3;
	Rockey(RY_ADD_UPDATE_CONTENT, &handle[0], &lp1, &lp2, &p1, &p2, &p3, &p4, buffer);
	p1 = 5;
	p2 = 1;
	p3 = 1;
	*(WORD *)buffer = 4;
	Rockey(RY_ADD_UPDATE_CONTENT, &handle[0], &lp1, &lp2, &p1, &p2, &p3, &p4, buffer);
	p1 = 5;
	p2 = 2;
	p3 = 1;
	*(WORD *)buffer = 6;
	Rockey(RY_ADD_UPDATE_CONTENT, &handle[0], &lp1, &lp2, &p1, &p2, &p3, &p4, buffer);

	retcode = Rockey(RY_UPDATE_GEN_EX, &handle[0], &lp1, &lp2, &p1, &p2, &p3, &p4, NULL);
	p1 = 0xc44c;
	p2 = 0xc8f8;
	retcode = Rockey(RY_UPDATE_GEN_EX, &handle[0], &lp1, &lp2, &p1, &p2, &p3, &p4, RetBuf);
	if (retcode)
	{
		ShowERR(retcode);
		return retcode;
	}
	retcode = Rockey(RY_UPDATE_EX, &handle[0], &lp1, &lp2, &p1, &p2, &p3, &p4, RetBuf);
	if (retcode)
	{
		ShowERR(retcode);
		return retcode;
	}

	printf("Update moudle over\n");


	//Update User ID
	memset(buffer, 0, BUF_SIZE);
	lp2 = 2011;
	p1 = 11;
	p2 = 11;
	p3 = 11;
	p4 = 11;
	Rockey(RY_GET_TIME_DWORD, &handle[0], (DWORD *)&buffer[3 * 4], &lp2, &p1, &p2, &p3, &p4, NULL);
	lp1 = 0x00000008;
	Rockey(RY_ADD_UPDATE_HEADER, &handle[0], &lp1, &lp2, &p1, &p2, &p3, &p4, buffer);
	p1 = 4;
	*(DWORD *)buffer = 0x12345678;
	Rockey(RY_ADD_UPDATE_CONTENT, &handle[0], &lp1, &lp2, &p1, &p2, &p3, &p4, buffer);

	retcode = Rockey(RY_UPDATE_GEN_EX, &handle[0], &lp1, &lp2, &p1, &p2, &p3, &p4, NULL);
	p1 = 0xc44c;
	p2 = 0xc8f8;
	retcode = Rockey(RY_UPDATE_GEN_EX, &handle[0], &lp1, &lp2, &p1, &p2, &p3, &p4, RetBuf);
	if (retcode)
	{
		ShowERR(retcode);
		return retcode;
	}
	retcode = Rockey(RY_UPDATE_EX, &handle[0], &lp1, &lp2, &p1, &p2, &p3, &p4, RetBuf);
	if (retcode)
	{
		ShowERR(retcode);
		return retcode;
	}
	
	printf("Update User ID over\n");


    //Update arithmetic
	memset(buffer, 0, BUF_SIZE);
	lp2 = 2011;
	p1 = 11;
	p2 = 11;
	p3 = 11;
	p4 = 11;
	//Converse time format from yyyy-mm-dd hh:mm to minute value from 2006-1-1 0:0
	Rockey(RY_GET_TIME_DWORD, &handle[0], (DWORD *)&buffer[3 * 4], &lp2, &p1, &p2, &p3, &p4, NULL);
	lp1 = 0x00000008;
	//Fill update file header
	Rockey(RY_ADD_UPDATE_HEADER, &handle[0], &lp1, &lp2, &p1, &p2, &p3, &p4, buffer);
	p1 = 3;
	p2 = 0;
	strcpy((char *)buffer, "H=H^H, A=A*23, F=B*17, A=A+F, A=A+G, A=A<C, A=A^D, B=B^B, C=C^C, D=D^D");
	//Fill update file content
	Rockey(RY_ADD_UPDATE_CONTENT, &handle[0], &lp1, &lp2, &p1, &p2, &p3, &p4, buffer);

	retcode = Rockey(RY_UPDATE_GEN_EX, &handle[0], &lp1, &lp2, &p1, &p2, &p3, &p4, NULL);
	p1 = 0xc44c;
	p2 = 0xc8f8;
	retcode = Rockey(RY_UPDATE_GEN_EX, &handle[0], &lp1, &lp2, &p1, &p2, &p3, &p4, RetBuf);
	if (retcode)
	{
		ShowERR(retcode);
		return retcode;
	}
	retcode = Rockey(RY_UPDATE_EX, &handle[0], &lp1, &lp2, &p1, &p2, &p3, &p4, RetBuf);
	if (retcode)
	{
		ShowERR(retcode);
		return retcode;
	}

	printf("Update arithmetic over\n");

	lp1 = 0;
	lp2 = 7;
	p1 = 5;
	p2 = 3;
	p3 = 1;
	p4 = 0xffff;
	retcode = Rockey(RY_CALCULATE1, &handle[0], &lp1, &lp2, &p1, &p2, &p3, &p4, buffer);
	if (retcode)
	{
		ShowERR(retcode);
		return retcode;
	}

	printf("Calculate result:p1=%d,p2=%d,p3=%d,p4=%d\n",p1,p2,p3,p4);

  
*/
	return retcode;
}

int main()
{
	WORD handle[16], p1, p2, p3, p4, retcode;
	DWORD lp1, lp2;
	BYTE buffer[1024];
	WORD rc[4];
	int i, j,k;
	int nKeyLen[2]={8,16} ;
	unsigned char keybuf[16];
	char str[1024];
	DWORD dwHour;
	char cmd[] = "H=H^H, A=A*23, F=B*17, A=A+F, A=A+G, A=A<C, A=A^D, B=B^B, C=C^C, D=D^D";
	char cmd1[] = "A=A+B, A=A+C, A=A+D, A=A+E, A=A+F, A=A+G, A=A+H";
	char cmd2[] = "A=E|E, B=F|F, C=G|G, D=H|H";
	BYTE buffer2[16];
	p1 = 0xc44c;
	p2 = 0xc8f8;
	p3 = 0x0799;
	p4 = 0xc43b;
	SYSTEMTIME st={0};	

	retcode = Rockey(RY_FIND, &handle[0], &lp1, &lp2, &p1, &p2, &p3, &p4, buffer);
	if (retcode)
	{
		ShowERR(retcode);
		return retcode;
	}
	
	printf("Find Rockey: %08X\n", lp1);
	retcode = Rockey(RY_OPEN, &handle[0], &lp1, &lp2, &p1, &p2, &p3, &p4, buffer);
	if (retcode)
	{
		ShowERR(retcode);
		return retcode;
	}

	i = 1;
	while (retcode == 0)
	{
		retcode = Rockey(RY_FIND_NEXT, &handle[i], &lp1, &lp2, &p1, &p2, &p3, &p4, buffer);
		if (retcode == ERR_NOMORE) break;
		if (retcode)
		{
			ShowERR(retcode);
			return retcode;
		}

		retcode = Rockey(RY_OPEN, &handle[i], &lp1, &lp2, &p1, &p2, &p3, &p4, buffer);
		if (retcode)
		{
			ShowERR(retcode);
			return retcode;
		}

		i++;
		printf("Find Rock: %08X\n", lp1);
	}
	printf("\n");

	for (j=0;j<i;j++)
	{

		//update package
		//retcode = r4s_update(&handle[j]);
//		if (retcode)
//		{
//			ShowERR(retcode);
//			return;
//		}
//		printf("update package success\n");
		
        //
		p1 = 3;
		p2 = 6;
		
		strcpy((char*)buffer, "Hello1");
		retcode = Rockey(RY_WRITE, &handle[j], &lp1, &lp2, &p1, &p2, &p3, &p4, buffer);
		if (retcode)
		{
			ShowERR(retcode);
			return retcode;
		}
		printf("Write: Hello1\n");

		p1 = 3;
		p2 = 6;
		memset(buffer, 0, 64);
		retcode = Rockey(RY_READ, &handle[j], &lp1, &lp2, &p1, &p2, &p3, &p4, buffer);
		if (retcode)
		{
			ShowERR(retcode);
			return retcode;
		}
		printf("Read: %s\n", buffer);


		retcode = Rockey(RY_RANDOM, &handle[j], &lp1, &lp2, &p1, &p2, &p3, &p4, buffer);
		if (retcode)
		{
			ShowERR(retcode);
			return retcode;
		}
		printf("Random: %04X,%04X,%04X,%04X\n", p1,p2,p3,p4);

		lp2 = 0x12345678;
		retcode = Rockey(RY_SEED, &handle[j], &lp1, &lp2, &p1, &p2, &p3, &p4, buffer);
		if (retcode)
		{
			ShowERR(retcode);
			return retcode;
		}
		printf("Seed: %04X %04X %04X %04X\n", p1, p2, p3, p4);
		rc[0] = p1;
		rc[1] = p2;
		rc[2] = p3;
		rc[3] = p4;

		lp1 = 0x88888888;
		retcode = Rockey(RY_WRITE_USERID, &handle[j], &lp1, &lp2, &p1, &p2, &p3, &p4, buffer);
		if (retcode)
		{
			ShowERR(retcode);
			return;
		}
		printf("Write User ID: %08X\n", lp1);

		lp1 = 0;
		retcode = Rockey(RY_READ_USERID, &handle[j], &lp1, &lp2, &p1, &p2, &p3, &p4, buffer);
		if (retcode)
		{
			ShowERR(retcode);
			return;
		}
		printf("Read User ID: %08X\n", lp1);

		p1 = 7;
		p2 = 0x2121;
		p3 = 0;
		retcode = Rockey(RY_SET_MODULE, &handle[j], &lp1, &lp2, &p1, &p2, &p3, &p4, buffer);
		if (retcode)
		{
			ShowERR(retcode);
			return;
		}
		printf("Set Moudle 7: Pass = %04X Decrease no allow\n", p2);

		p1 = 7;
		retcode = Rockey(RY_CHECK_MODULE, &handle[j], &lp1, &lp2, &p1, &p2, &p3, &p4, buffer);
		if (retcode)
		{
			ShowERR(retcode);
			return;
		}
		printf("Check Module 7: \n");
		if (p2) printf("The module is valid\n");
		else printf("The module is not valid\n");
		if (p3) printf("Allow Decrease\n");
		else printf("Not Allow Decrease\n");

		p1 = 0;
		strcpy((char*)buffer, cmd);
		retcode = Rockey(RY_WRITE_ARITHMETIC, &handle[j], &lp1, &lp2, &p1, &p2, &p3, &p4, buffer);
		if (retcode)
		{
			ShowERR(retcode);
			return;
		}
		printf("Write Arithmetic 1\n");

		lp1 = 0;
		lp2 = 7;
		p1 = 5;
		p2 = 3;
		p3 = 1;
		p4 = 0xffff;
		
		retcode = Rockey(RY_CALCULATE1, &handle[j], &lp1, &lp2, &p1, &p2, &p3, &p4, buffer);
		if (retcode)
		{
			ShowERR(retcode);
			return;
		}
		printf("Calculate Input: p1=5, p2=3, p3=1, p4=0xffff\n");
		printf("Result = ((5*23 + 3*17 + 0x2121) < 1) ^ 0xffff = BC71\n");
		printf("Calculate Output: p1=%x, p2=%x, p3=%x, p4=%x\n", p1, p2, p3, p4);

		p1 = 10;
		strcpy((char*)buffer, cmd1);
		retcode = Rockey(RY_WRITE_ARITHMETIC, &handle[j], &lp1, &lp2, &p1, &p2, &p3, &p4, buffer);
		if (retcode)
		{
			ShowERR(retcode);
			return;
		}
		printf("Write Arithmetic 2\n");

		lp1 = 10;
		lp2 = 0x12345678;
		p1 = 1;
		p2 = 2;
		p3 = 3;
		p4 = 4;
		retcode = Rockey(RY_CALCULATE2, &handle[j], &lp1, &lp2, &p1, &p2, &p3, &p4, buffer);
		if (retcode)
		{
			ShowERR(retcode);
			return;
		}
		printf("Calculate Input: p1=1, p2=2, p3=3, p4=4\n");
		printf("Result = %04x + %04x + %04x + %04x + 1 + 2 + 3 + 4 = %04x\n", rc[0], rc[1], rc[2], rc[3], (WORD)(rc[0]+rc[1]+rc[2]+rc[3]+10));
		printf("Calculate Output: p1=%x, p2=%x, p3=%x, p4=%x\n", p1, p2, p3, p4);

		// Set Decrease
		p1 = 9;
		p2 = 0x5;
		p3 = 1;
		retcode = Rockey(RY_SET_MODULE, &handle[j], &lp1, &lp2, &p1, &p2, &p3, &p4, buffer);
		if (retcode)
		{
			ShowERR(retcode);
			return;
		}

		p1 = 17;
		strcpy((char*)buffer, cmd2);
		retcode = Rockey(RY_WRITE_ARITHMETIC, &handle[j], &lp1, &lp2, &p1, &p2, &p3, &p4, buffer);
		if (retcode)
		{
			ShowERR(retcode);
			return;
		}
		printf("Write Arithmetic 3\n");

		lp1 = 17;
		lp2 = 6;
		p1 = 1;
		p2 = 2;
		p3 = 3;
		p4 = 4;
		retcode = Rockey(RY_CALCULATE3, &handle[j], &lp1, &lp2, &p1, &p2, &p3, &p4, buffer);
		if (retcode)
		{
			ShowERR(retcode);
			return;
		}
		printf("Show Module from 6: p1=%x, p2=%x, p3=%x, p4=%x\n", p1, p2, p3, p4);

		// Decrease
		p1 = 9;
		retcode = Rockey(RY_DECREASE, &handle[j], &lp1, &lp2, &p1, &p2, &p3, &p4, buffer);
		if (retcode)
		{
			ShowERR(retcode);
			return;
		}
		printf("Decrease module 9\n");

		lp1 = 17;
		lp2 = 6;
		p1 = 1;
		p2 = 2;
		p3 = 3;
		p4 = 4;
		retcode = Rockey(RY_CALCULATE3, &handle[j], &lp1, &lp2, &p1, &p2, &p3, &p4, buffer);
		if (retcode)
		{
			ShowERR(retcode);
			return;
		}
		printf("Show Module from 6: p1=%x, p2=%x, p3=%x, p4=%x\n", p1, p2, p3, p4);

		//DES test
		printf("des test start!\n");

		//set DES key buffer
		p1=DES_SINGLE_MODE;
		for(k=0;k<nKeyLen[p1];k++)
		{
			keybuf[k]=k; //des key
		}

		//set DES key
		retcode = Rockey(RY_SET_DES_KEY, &handle[j], &lp1, &lp2, &p1, &p2, &p3, &p4, keybuf);
		if (retcode)
		{
			ShowERR(retcode);
			return;
		}

		//set rude data to be encrypt
		p2 = 24;
		//p2 must be multiple of 8
		for(k=0;k<p2;k++)
		{
			buffer[k]='A';
		}		
		printf("des plain data:\n");
		HexBufferToString(str,buffer,p2);
		printf(str);
		printf("\n");
		//DES encrypt
		retcode = Rockey(RY_DES_ENC, &handle[j], &lp1, &lp2, &p1, &p2, &p3, &p4, buffer);

		if (retcode)
		{
			ShowERR(retcode);
			return;
		}
		printf("des cipher data:\n");
		HexBufferToString(str,buffer,p2);
		printf(str);
		printf("\n");
		//DES decrypt
		retcode = Rockey(RY_DES_DEC, &handle[j], &lp1, &lp2, &p1, &p2, &p3, &p4,buffer);
		if (retcode)
		{
			ShowERR(retcode);
			return;
		}
		printf("des decrypt data:\n");
		HexBufferToString(str,buffer,p2);
		printf(str);
		printf("\n");

		printf("des test over!\n");


		//RSA test
		printf("rsa test start!\n");

		//set key N
		memset(buffer, 0 , 1024);
		SetN(buffer);
		retcode = Rockey(RY_SET_RSAKEY_N, &handle[j], &lp1, &lp2, &p1, &p2, &p3, &p4, buffer);
		if (retcode)
		{
			ShowERR(retcode);
			return;
		}

		//set key D
		memset(buffer, 0 , 1024);
		SetD(buffer);
		retcode = Rockey(RY_SET_RSAKEY_D, &handle[j], &lp1, &lp2, &p1, &p2, &p3, &p4, buffer);
		if (retcode)
		{
			ShowERR(retcode);
			return;
		}
		//set rude data to be encrypt
		memset(buffer,0,1024);

		//set pad type
		p2 = 120;
		for(k=0;k<p2;k++)
		{
			buffer[k] = 'A';
		}	
		printf("rsa plain data:\n");
		HexBufferToString(str,buffer,p2);
		printf(str);
		printf("\n");
		
		p3 = RSA_ROCKEY_PADDING;

		//rsa public key encrypt
		p1 = RSA_PUBLIC_KEY;
		retcode = Rockey(RY_RSA_ENC, &handle[j], &lp1, &lp2, &p1, &p2, &p3, &p4,buffer);
		if (retcode)
		{
			ShowERR(retcode);
			return;
		}
		
		printf("rsa cipher data:\n");
		HexBufferToString(str,buffer,128);
		printf(str);
		printf("\n");
		//rsa private key decrypt
		p1 = RSA_PRIVATE_KEY;

		retcode = Rockey(RY_RSA_DEC, &handle[j], &lp1, &lp2, &p1, &p2, &p3, &p4,buffer);
		
		if (retcode)
		{
			ShowERR(retcode);
			return;
		}
		printf("rsa decrypt data:\n");
		HexBufferToString(str,buffer,p2);
		printf(str);
		printf("\n");

		printf("rsa test over!\n");
		

		// Timer test
		// Get Timer 0 - hour
		p1 = 0;
		retcode = Rockey(RY_GET_TIMER_EX, &handle[j], &lp1, &lp2, &p1, &p2, &p3, &p4, buffer);
		if (retcode)
		{
			ShowERR(retcode);
			return;
		}		
		memset((void*)(&dwHour),0,sizeof(DWORD));   
		memcpy((void*)(&dwHour),buffer,2);
		printf("Get timer %d: %d\n", p1, dwHour);
		
		// Set Timer 2 to 20 hours
		dwHour = 20;
		memset(buffer,0,sizeof(buffer));   
		memcpy(buffer,(void*)(&dwHour),2);
		p1 = 2;
		p3 = 2;
		retcode = Rockey(RY_SET_TIMER_EX, &handle[j], &lp1, &lp2, &p1, &p2, &p3, &p4, buffer);
		if (retcode)
		{
			ShowERR(retcode);
			return;
		}
		printf("Set timer %d: %d\n", p1, dwHour);
		p1 = 2;
		memset(buffer,0,sizeof(buffer));
		retcode = Rockey(RY_GET_TIMER_EX, &handle[j], &lp1, &lp2, &p1, &p2, &p3, &p4, buffer);
		if (retcode)
		{
			ShowERR(retcode);
			return;
		}		
		memset((void*)(&dwHour),0,sizeof(DWORD));   
		memcpy((void*)(&dwHour),buffer,2);
		printf("Get timer %d: %d\n", p1, dwHour);

		
		// Get timer 1 - date
		struct tm licST;
		p1 = 1;
		p3 = 1;
		retcode = Rockey(RY_GET_TIMER_EX, &handle[j], &lp1, &lp2, &p1, &p2, &p3, &p4, (BYTE*)(&licST));
		if (retcode)
		{
			ShowERR(retcode);
			return;
		}
		printf("Get time successfully. Unit %d, Time(UTC): %d-%d-%d %d:%d:%d %d\n",p1,licST.tm_year+1900,
			licST.tm_mon+1,licST.tm_mday,licST.tm_hour,licST.tm_min,licST.tm_sec,licST.tm_wday);
		
		// Set timer 3 to 2018/02/10
		struct tm licST2 = {0};
		licST2.tm_year = 2018 - 1900;
		licST2.tm_mon = 2 - 1;
		licST2.tm_mday = 10;
		p1 = 3;
		p3 = 1;
		retcode = Rockey(RY_SET_TIMER_EX, &handle[j], &lp1, &lp2, &p1, &p2, &p3, &p4, (BYTE*)(&licST2));
		if (retcode)
		{
			ShowERR(retcode);
			return;
		}
		printf("Set time successfully. Unit %d, Time(UTC): %d-%d-%d %d:%d:%d \n",p1,licST2.tm_year+1900,
			licST2.tm_mon+1,licST2.tm_mday,licST2.tm_hour,licST2.tm_min,licST2.tm_sec);

		p1 = 3;
		p3 = 1;
		retcode = Rockey(RY_GET_TIMER_EX, &handle[j], &lp1, &lp2, &p1, &p2, &p3, &p4, (BYTE*)(&licST));
		if (retcode)
		{
			ShowERR(retcode);
			return;
		}
		printf("Get time successfully. Unit %d, Time(UTC): %d-%d-%d %d:%d:%d %d\n",p1,licST.tm_year+1900,
			licST.tm_mon+1,licST.tm_mday,licST.tm_hour,licST.tm_min,licST.tm_sec,licST.tm_wday);

		
/*		time_t t = time(NULL);
    		struct tm licST2 = *gmtime(&t);
*/		
		retcode = Rockey(RY_CLOSE, &handle[j], &lp1, &lp2, &p1, &p2, &p3, &p4, buffer);
		if (retcode)
		{
			ShowERR(retcode);
			return;
		}

		
	}
}

