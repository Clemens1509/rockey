# -*- coding: cp936 -*-
import sys
from ctypes import *
hinst=cdll.LoadLibrary("Ry4S_X64.dll")  
Rockey=hinst.Rockey
#
#Define some constants
RY_FIND                  =              1               #find rockey
RY_FIND_NEXT             =              2		#find next rockey
RY_OPEN                  =              3		#open rockey
RY_CLOSE                 =              4		#close rockey
RY_READ                  =              5		#read memory
RY_WRITE                 =              6		#write memory
RY_RANDOM                =              7		#generate random number
RY_SEED                  =              8		#generate seed code
RY_WRITE_USERID	         =	        9		#write user ID
RY_READ_USERID	         =	        10		#read user ID
RY_SET_MODULE	         =	        11		#set module
RY_CHECK_MODULE	         =              12		#check module status
RY_WRITE_ARITHMETIC      =              13		#write arithmetic
RY_CALCULATE1		 =	        14		#calculate 1
RY_CALCULATE2	    	 =	        15		#calculate 2
RY_CALCULATE3		 =	        16		#calculate 3
RY_DECREASE		 =	        17		#decrease module unit
RY_CALLNET               =              18              #NETROCKEY
#
#

ERR_SUCCESS		    =		0               
ERR_NO_ROCKEY		    =	        3               
ERR_INVALID_PASSWORD        =	        4               
ERR_INVALID_PASSWORD_OR_ID  =	        5               
ERR_SETID		    =		6               
ERR_INVALID_ADDR_OR_SIZE    =	        7               
ERR_UNKNOWN_COMMAND         =		8               
ERR_NOTBELEVEL3		    =	        9               
ERR_READ	            =	        10              
ERR_WRITE	            =	        11              
ERR_RANDOM		    =		12              
ERR_SEED	    	    =		13              
ERR_CALCULATE		    =	        14              
ERR_NO_OPEN		    =		15              
ERR_OPEN_OVERFLOW	    =	        16              
ERR_NOMORE		    =		17              
ERR_NEED_FIND		    =	        18              
ERR_DECREASE		    =	        19              
ERR_AR_BADCOMMAND	    =	        20              
ERR_AR_UNKNOWN_OPCODE       =	        21              
ERR_AR_WRONGBEGIN	    =	        22              
ERR_AR_WRONG_END	    =	        23              
ERR_AR_VALUEOVERFLOW	    =           24              
ERR_TOOMUCHTHREAD	    =	        25              
ERR_RECEIVE_NULL	    =	        0x100           
ERR_UNKNOWN_SYSTEM	    =	        0x102           
ERR_UNKNOWN		    =	        0xffff          
#
#Define variables
handle=c_int()
lp1=c_int()
lp2=c_int()
p1=c_int(0xc44c)                                #Demo passwords
p2=c_int(0xc8f8)
p3=c_int(0x0799)
p4=c_int(0xc43b)
arr=c_byte*1024
buf=arr()
#
#

retval=Rockey(RY_FIND,byref(handle),byref(lp1),byref(lp2),\
              byref(p1),byref(p2),byref(p3),byref(p4),byref(buf))
if retval!=ERR_SUCCESS:
    print ("No Rockey found!")
    sys.exit(0)

print ("Find Rockey: 0x%x"%lp1.value)
print ("")
#
retval=Rockey(RY_OPEN,byref(handle),byref(lp1),byref(lp2),\
              byref(p1),byref(p2),byref(p3),byref(p4),byref(buf))
if retval!=ERR_SUCCESS:
    print ("Open Rockey failed!")
    sys.exit(0)
count=1
while retval==ERR_SUCCESS:
    retval=Rockey(RY_FIND_NEXT,byref(handle),byref(lp1),byref(lp2),\
              byref(p1),byref(p2),byref(p3),byref(p4),byref(buf))
    if retval==ERR_NOMORE:
        break
    if retval!=ERR_SUCCESS:
        print ("Find next Rockey failed!")
        sys.exit(0)
    retval=Rockey(RY_OPEN,byref(handle),byref(lp1),byref(lp2),\
              byref(p1),byref(p2),byref(p3),byref(p4),byref(buf))
    if retval!=ERR_SUCCESS:
        print ("Open Rockey failed!")
        sys.exit(0)
    count=count+1
    print ("Find another Rockey: 0x%x"%lp1.value)
    
    print ("")
print ("Find %d rockey"%count)
print ("")
i=0
while i<count:

    handle=c_int()
    lp1=c_int()
    lp2=c_int()
    p1=c_int(0xc44c)                                #Demo passwords
    p2=c_int(0xc8f8)
    p3=c_int(0x0799)
    p4=c_int(0xc43b)
    arr=c_byte*1024
    buf=arr()
    
    retval=Rockey(RY_FIND,byref(handle),byref(lp1),byref(lp2),\
              byref(p1),byref(p2),byref(p3),byref(p4),byref(buf))
    if retval==ERR_NOMORE:
        break
    if retval!=ERR_SUCCESS:
        print ("Error: %d"%retval)
        print ("Find next Rockey failed!")
        sys.exit(0)

    print ("Handle is: 0x%x"%handle.value)
    retval=Rockey(RY_OPEN,byref(handle),byref(lp1),byref(lp2),\
              byref(p1),byref(p2),byref(p3),byref(p4),byref(buf))
    if retval!=ERR_SUCCESS:
        print ("Error: %d"%retval)
        print ("Open Rockey failed!")
        sys.exit(0)
   

    



    i = i+1
