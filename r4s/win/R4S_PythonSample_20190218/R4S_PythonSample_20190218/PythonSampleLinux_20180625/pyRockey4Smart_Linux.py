# -*- coding: cp936 -*-
import sys
from ctypes import *
hinst=cdll.LoadLibrary("libRockey4Smart.so")  
Rockey=hinst.Rockey
#
#Define some constants
RY_FIND                  =              1               #find rockey
RY_FIND_NEXT             =              2		#find next rockey
RY_OPEN                  =              3		#open rockey
RY_CLOSE                 =              4		#close rockey
RY_READ                  =              5		#read memory
RY_WRITE                 =              6		#write memory
RY_RANDOM                =              7		#generate random number
RY_SEED                  =              8		#generate seed code
RY_WRITE_USERID	         =	        9		#write user ID
RY_READ_USERID	         =	        10		#read user ID
RY_SET_MODULE	         =	        11		#set module
RY_CHECK_MODULE	         =              12		#check module status
RY_WRITE_ARITHMETIC      =              13		#write arithmetic
RY_CALCULATE1		 =	        14		#calculate 1
RY_CALCULATE2	    	 =	        15		#calculate 2
RY_CALCULATE3		 =	        16		#calculate 3
RY_DECREASE		 =	        17		#decrease module unit
RY_CALLNET               =              18              #NETROCKEY
#
#

ERR_SUCCESS		    =		0               
ERR_NO_ROCKEY		    =	        3               
ERR_INVALID_PASSWORD        =	        4               
ERR_INVALID_PASSWORD_OR_ID  =	        5               
ERR_SETID		    =		6               
ERR_INVALID_ADDR_OR_SIZE    =	        7               
ERR_UNKNOWN_COMMAND         =		8               
ERR_NOTBELEVEL3		    =	        9               
ERR_READ	            =	        10              
ERR_WRITE	            =	        11              
ERR_RANDOM		    =		12              
ERR_SEED	    	    =		13              
ERR_CALCULATE		    =	        14              
ERR_NO_OPEN		    =		15              
ERR_OPEN_OVERFLOW	    =	        16              
ERR_NOMORE		    =		17              
ERR_NEED_FIND		    =	        18              
ERR_DECREASE		    =	        19              
ERR_AR_BADCOMMAND	    =	        20              
ERR_AR_UNKNOWN_OPCODE       =	        21              
ERR_AR_WRONGBEGIN	    =	        22              
ERR_AR_WRONG_END	    =	        23              
ERR_AR_VALUEOVERFLOW	    =           24              
ERR_TOOMUCHTHREAD	    =	        25              
ERR_RECEIVE_NULL	    =	        0x100           
ERR_UNKNOWN_SYSTEM	    =	        0x102           
ERR_UNKNOWN		    =	        0xffff          
#
#Define variables
handle=c_int()
lp1=c_int()
lp2=c_int()
p1=c_int(0xc44c)                                #Demo passwords
p2=c_int(0xc8f8)
p3=c_int(0x0799)
p4=c_int(0xc43b)
arr=c_byte*1024
buf=arr()
#
#
retval=Rockey(RY_FIND,byref(handle),byref(lp1),byref(lp2),\
              byref(p1),byref(p2),byref(p3),byref(p4),byref(buf))
if retval!=ERR_SUCCESS:
    print ("No Rockey found!")
    sys.exit(0)
print ("Find Rockey: 0x%x"%lp1.value)
print ("")
#
retval=Rockey(RY_OPEN,byref(handle),byref(lp1),byref(lp2),\
              byref(p1),byref(p2),byref(p3),byref(p4),byref(buf))
if retval!=ERR_SUCCESS:
    print ("Open Rockey failed!")
    sys.exit(0)
count=1
while retval==ERR_SUCCESS:
    retval=Rockey(RY_FIND_NEXT,byref(handle),byref(lp1),byref(lp2),\
              byref(p1),byref(p2),byref(p3),byref(p4),byref(buf))
    if retval==ERR_NOMORE:
        break
    if retval!=ERR_SUCCESS:
        print ("Find next Rockey failed!")
        sys.exit(0)
    retval=Rockey(RY_OPEN,byref(handle),byref(lp1),byref(lp2),\
              byref(p1),byref(p2),byref(p3),byref(p4),byref(buf))
    if retval!=ERR_SUCCESS:
        print ("Open Rockey failed!")
        sys.exit(0)
    count=count+1
    print ("Find another Rockey: 0x%x"%lp1.value)
    print ("")
print ("Find %d rockey"%count)
print ("")
i=0
while i<count:

    handle=c_int()
    lp1=c_int()
    lp2=c_int()
    p1=c_int(0xc44c)                                #Demo passwords
    p2=c_int(0xc8f8)
    p3=c_int(0x0799)
    p4=c_int(0xc43b)
    arr=c_byte*1024
    buf=arr()

    retval=Rockey(RY_FIND,byref(handle),byref(lp1),byref(lp2),\
              byref(p1),byref(p2),byref(p3),byref(p4),byref(buf))
    if retval==ERR_NOMORE:
        break
    if retval!=ERR_SUCCESS:
        print ("Error: %d"%retval)
        print ("Find next Rockey failed!")
        sys.exit(0)


    retval=Rockey(RY_OPEN,byref(handle),byref(lp1),byref(lp2),\
              byref(p1),byref(p2),byref(p3),byref(p4),byref(buf))
    if retval!=ERR_SUCCESS:
        print ("Error: %d"%retval)
        print ("Open Rockey failed!")
        sys.exit(0)

    #write rockey
    p1=c_int(3)
    p2=c_int(5)
    buf[0]=ord('A')
    buf[1]=ord('C')
    buf[2]=ord('c')
    buf[3]=ord('B')
    buf[4]=ord('D')
    retval=Rockey(RY_WRITE,byref(handle),byref(lp1),byref(lp2),\
              byref(p1),byref(p2),byref(p3),byref(p4),byref(buf))
    if retval!=ERR_SUCCESS:
        print ("Write Rockey failed!")
        sys.exit(0)
    print ("Write test:%d %d %d %d %d"%(buf[0],buf[1],buf[2],buf[3],buf[4]))
    #
    #read rockey
    p1=c_int(3)
    p2=c_int(5)
    buf=arr()
    retval=Rockey(RY_READ,byref(handle),byref(lp1),byref(lp2),\
              byref(p1),byref(p2),byref(p3),byref(p4),byref(buf))
    if retval!=ERR_SUCCESS:
        print ("Read Rockey failed!")
        sys.exit(0)
    print ("Read test:%d %d %d %d %d"%(buf[0],buf[1],buf[2],buf[3],buf[4]))
    #
    #Get random number
    retval=Rockey(RY_RANDOM,byref(handle),byref(lp1),byref(lp2),\
              byref(p1),byref(p2),byref(p3),byref(p4),byref(buf))
    if retval!=ERR_SUCCESS:
        print ("Get random number failed!")
        sys.exit(0)
    print ("Random number: 0x%x"%p1.value)
    #
    #Get Seed
    lp2=c_int(0x12345678)
    retval=Rockey(RY_SEED,byref(handle),byref(lp1),byref(lp2),\
              byref(p1),byref(p2),byref(p3),byref(p4),byref(buf))
    if retval!=ERR_SUCCESS:
        print ("Get seed failed!")
        sys.exit(0)
    print ("Seed: 0x%x,0x%x,0x%x,0x%x"%(p1.value,p2.value,p3.value,p4.value))
    rc=(p1.value,p2.value,p3.value,p4.value)
    #
    #Write UserID
    lp1=c_int(0x888888)
    retval=Rockey(RY_WRITE_USERID,byref(handle),byref(lp1),byref(lp2),\
              byref(p1),byref(p2),byref(p3),byref(p4),byref(buf))
    if retval!=ERR_SUCCESS:
        print ("Write userID failed!")
        sys.exit(0)
    print ("Write userID: 0x%x"%lp1.value)
    #
    #Read userID
    lp1=c_int(0)
    retval=Rockey(RY_READ_USERID,byref(handle),byref(lp1),byref(lp2),\
              byref(p1),byref(p2),byref(p3),byref(p4),byref(buf))
    if retval!=ERR_SUCCESS:
        print ("Read userID failed!")
        sys.exit(0)
    print ("Read userID: 0x%x"%lp1.value)
    #
    #Set Module
    p1=c_int(7)
    p2=c_int(0x2121)
    p3=c_int(0)
    retval=Rockey(RY_SET_MODULE,byref(handle),byref(lp1),byref(lp2),\
              byref(p1),byref(p2),byref(p3),byref(p4),byref(buf))
    if retval!=ERR_SUCCESS:
        print ("Set module failed!")
        sys.exit(0)
    print ("Set module %d,pass=0x%x,Decreasement no allowed!"%(p1.value,p2.value))
    #
    #check Module
    p1=c_int(7)
    p2=c_int(0)
    p3=c_int(0)
    retval=Rockey(RY_CHECK_MODULE,byref(handle),byref(lp1),byref(lp2),\
              byref(p1),byref(p2),byref(p3),byref(p4),byref(buf))
    if retval!=ERR_SUCCESS:
        print ("Check module failed!")
        sys.exit(0)
    if p2.value!=0:
        err="Allow!"
    else:
        err="Not Allow!"
    print ("Check module %d: %s"%(p1.value,err))
    if p3.value!=0:
        print ("Decreasement is allowed!")
    else:
        print ("Decreasement is not allowed!")
    #
    #write ARITHMETIC
    p1=c_int(0)
    buf[0]=ord('H');buf[1]=ord('=');buf[2]=ord('H')
    buf[3]=ord('^');buf[4]=ord('H');buf[5]=ord(',')
    buf[6]=ord('A');buf[7]=ord('=');buf[8]=ord('A')
    buf[9]=ord('*');buf[10]=ord('2');buf[11]=ord('3')
    buf[12]=ord(',');buf[13]=ord('F');buf[14]=ord('=')
    buf[15]=ord('B');buf[16]=ord('*');buf[17]=ord('1')
    buf[18]=ord('7');buf[19]=ord(',');buf[20]=ord('A')
    buf[21]=ord('=');buf[22]=ord('A');buf[23]=ord('+')
    buf[24]=ord('F');buf[25]=ord(',');buf[26]=ord('A')
    buf[27]=ord('=');buf[28]=ord('A');buf[29]=ord('+')
    buf[30]=ord('G');buf[31]=ord(',');buf[32]=ord('A')
    buf[33]=ord('=');buf[34]=ord('A');buf[35]=ord('<')
    buf[36]=ord('C');buf[37]=ord(',');buf[38]=ord('A')
    buf[39]=ord('=');buf[40]=ord('A');buf[41]=ord('^')
    buf[42]=ord('D');buf[43]=ord(',');buf[44]=ord('B')
    buf[45]=ord('=');buf[46]=ord('B');buf[47]=ord('^')
    buf[48]=ord('B');buf[49]=ord(',');buf[50]=ord('C')
    buf[51]=ord('=');buf[52]=ord('C');buf[53]=ord('^')
    buf[54]=ord('C');buf[55]=ord(',');buf[56]=ord('D')
    buf[57]=ord('=');buf[58]=ord('D');buf[59]=ord('^')
    buf[60]=ord('D')
    retval=Rockey(RY_WRITE_ARITHMETIC,byref(handle),byref(lp1),byref(lp2),\
              byref(p1),byref(p2),byref(p3),byref(p4),byref(buf))
    if retval!=ERR_SUCCESS:
        print ("Write arithmetic1 failed!")
        sys.exit(0)
    print ("Write arithmetic1,succeed!")
    #
    #Calculate
    lp1=c_int(0)
    lp2=c_int(7)
    p1=c_int(5)
    p2=c_int(3)
    p3=c_int(1)
    p4=c_int(-1)
    retval=Rockey(RY_CALCULATE1,byref(handle),byref(lp1),byref(lp2),\
              byref(p1),byref(p2),byref(p3),byref(p4),byref(buf))
    if retval!=ERR_SUCCESS:
        print ("Calculate1 failed!")
        sys.exit(0)
    print ("Calculate Input: p1=5, p2=3, p3=1, p4=0xffff")
    print ("Result = ((5*23 + 3*17 + 0x2121) < 1) ^ 0xffff = 0xBC71")
    print ("Calculate output: p1=0x%x,p2=0x%x,p3=0x%x,p4=0x%x"%(p1.value,p2.value,p3.value,p4.value))
    #
    #
    p1=c_int(10)
    buf[0]=ord('A');buf[1]=ord('=');buf[2]=ord('A');buf[3]=ord('+');buf[4]=ord('B');buf[5]=ord(',')
    buf[6]=ord('A');buf[7]=ord('=');buf[8]=ord('A');buf[9]=ord('+');buf[10]=ord('C');buf[11]=ord(',')
    buf[12]=ord('A');buf[13]=ord('=');buf[14]=ord('A');buf[15]=ord('+');buf[16]=ord('D');buf[17]=ord(',')
    buf[18]=ord('A');buf[19]=ord('=');buf[20]=ord('A');buf[21]=ord('+');buf[22]=ord('E');buf[23]=ord(',')
    buf[24]=ord('A');buf[25]=ord('=');buf[26]=ord('A');buf[27]=ord('+');buf[28]=ord('F');buf[29]=ord(',')
    buf[30]=ord('A');buf[31]=ord('=');buf[32]=ord('A');buf[33]=ord('+');buf[34]=ord('G');buf[35]=ord(',')
    buf[36]=ord('A');buf[37]=ord('=');buf[38]=ord('A');buf[39]=ord('+');buf[40]=ord('H');buf[41]=0
    retval=Rockey(RY_WRITE_ARITHMETIC,byref(handle),byref(lp1),byref(lp2),\
              byref(p1),byref(p2),byref(p3),byref(p4),byref(buf))
    if retval!=ERR_SUCCESS:
        print ("Write arithmetic2 failed!")
        sys.exit(0)
    print ("Write arithmetic2,succeed!")
    #
    #
    lp1=c_int(10)
    lp2=c_int(0x12345678)
    p1=c_int(1)
    p2=c_int(2)
    p3=c_int(3)
    p4=c_int(4)
    retval=Rockey(RY_CALCULATE2,byref(handle),byref(lp1),byref(lp2),\
              byref(p1),byref(p2),byref(p3),byref(p4),byref(buf))
    if retval!=ERR_SUCCESS:
        print ("Calculate2 failed!")
        sys.exit(0)
    print ("Calculate Input: p1=1, p2=2, p3=3, p4=4")
    print ("Result = 0x%x+0x%x+0x%x+0x%x+1+2+3+4"%(rc[0],rc[1],rc[2],rc[3]))
    print ("Calculate result: p1=0x%x,p2=0x%x,p3=0x%x,p4=0x%x"%(p1.value,p2.value,p3.value,p4.value))
    #
    #
    p1=c_int(9)
    p2=c_int(5)
    p3=c_int(1)
    retval=Rockey(RY_SET_MODULE,byref(handle),byref(lp1),byref(lp2),\
              byref(p1),byref(p2),byref(p3),byref(p4),byref(buf))
    if retval!=ERR_SUCCESS:
        print ("Set module failed!")
        sys.exit(0)    
    p1=c_int(17)
    buf[0]=ord('A');buf[1]=ord('=');buf[2]=ord('E')
    buf[3]=ord('|');buf[4]=ord('E');buf[5]=ord(',')
    buf[6]=ord('B');buf[7]=ord('=');buf[8]=ord('F')
    buf[9]=ord('|');buf[10]=ord('F');buf[11]=ord(',')
    buf[12]=ord('C');buf[13]=ord('=');buf[14]=ord('G')
    buf[15]=ord('|');buf[16]=ord('G');buf[17]=ord(',')
    buf[18]=ord('D');buf[19]=ord('=');buf[20]=ord('H')
    buf[21]=ord('|');buf[22]=ord('H')
    retval=Rockey(RY_WRITE_ARITHMETIC,byref(handle),byref(lp1),byref(lp2),\
              byref(p1),byref(p2),byref(p3),byref(p4),byref(buf))
    if retval!=ERR_SUCCESS:
        print ("Write arithmetic3 failed!")
        sys.exit(0)
    print ("Write arithmetic3,succeed!")
    #
    #
    lp1=c_int(17)
    lp2=c_int(6)
    p1=c_int(1)
    p2=c_int(2)
    p3=c_int(3)
    p4=c_int(4)
    retval=Rockey(RY_CALCULATE3,byref(handle),byref(lp1),byref(lp2),\
              byref(p1),byref(p2),byref(p3),byref(p4),byref(buf))
    if retval!=ERR_SUCCESS:
        print ("Calculate3 failed!")
        sys.exit(0)
    print ("Show module from 6: p1=0x%x,p2=0x%x,p3=0x%x,p4=0x%x"%(p1.value,p2.value,p3.value,p4.value))
    #
    #Decrease
    p1=c_int(9)
    retval=Rockey(RY_DECREASE,byref(handle),byref(lp1),byref(lp2),\
              byref(p1),byref(p2),byref(p3),byref(p4),byref(buf))
    if retval!=ERR_SUCCESS:
        print ("Decrease failed!")
        sys.exit(0)
    print ("Decrease module 9")
    #
    #Calculate3
    lp1=c_int(17)
    lp2=c_int(6)
    p1=c_int(1)
    p2=c_int(2)
    p3=c_int(3)
    p4=c_int(4)
    retval=Rockey(RY_CALCULATE3,byref(handle),byref(lp1),byref(lp2),\
              byref(p1),byref(p2),byref(p3),byref(p4),byref(buf))
    if retval!=ERR_SUCCESS:
        print ("Calculate3 failed!")
        sys.exit(0)
    print ("Show module from 6: p1=0x%x,p2=0x%x,p3=0x%x,p4=0x%x"%(p1.value,p2.value,p3.value,p4.value))
    #
    #Close Rockey
    retval=Rockey(RY_CLOSE,byref(handle),byref(lp1),byref(lp2),\
              byref(p1),byref(p2),byref(p3),byref(p4),byref(buf))
    if retval!=ERR_SUCCESS:
        print ("Close rockey failed!")
        sys.exit(0)
    print ("Close rockey")
    print ("")
    i=i+1


    
    





