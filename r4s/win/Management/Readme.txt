------------------ ROCKEY4SMART Software Developer's Kit --------------------

This folder comprises license manager, production and remote update tools

Note:
    For Windows 98 users who use ROCKEY4SMART License Manager,
	please install device driver and update JET database engine in
	\Support for Win98

------------------ Folder Content List --------------------------------------
 
LicManager.exe      License manager
Production.exe      Production tool
RemoteUpdate.exe    Client update tool for end user



*****************************Remote Update Steps*****************************

-------------------
Part 1:Create client dongle
-------------------

1.  Create master dongle and public/private key file.

    a)Insert dongle.
    b)Run LicManager.exe.
    c)Input P1, P2, P3, P4 and click Verify Password button.
    d)Click Build button in Initialize master rockey and build the RSA key section.
    
    After a,b,c,d, Product.pub and Product.pri files are created and written to dongle.
    This dongle is called master dongle. Don't close program and go to next step.

2.  Create developer update file

    a)Click Update Manager for Developer button on the left,the Update Manager for Developer page shows.
    b)Edit License. Select some Time and Count units and input values.
    c)Click Build button at the bottom of Edit License section.

    d)Dialog prompts to ask Save chages to New update project? Click Yes button and save it.The saved file 
      includes P1,P2,P3,P4 password and public key of master dongle which will be used in updating client dongle. 

    e)In the File List,a file name like D:\Production1.3.R4Prd can be seen. This file includes Time and Count 
      value we input and a public key. This is the developer update file.

3.  Create slave dongle

    a)Insert another dongle.
    b)Run Management\Production.exe,also named developer tool or batch production tool.
    c)Input P1, P2, P3, P4 password.
    d)Select update file,that is the file created in step2.
    e)Click Build button. Normally update will be successful.

    Production tool writes Time and Count value,public key to dongle. If a,b,c,d steps are successful,a slave dongle is built.


-------------------
Part 2:Update slave dongle
-------------------

1.  Create client update file

    a)Insert master dongle
    b)Run LicManager.exe;
    c)Click Open button in tool bar and open the *.r4u file saved before.
    d)Click Update Manager for Customer button on the left section,the corresponding page shows.
    e)Click Next Step,Input P1 and P2;
    f)Click Next Step,Edit Module Word page shows.
    g)Click Next Step,Update Memory page shows.
    h)Click Next Step,Edit Time Unit and Count Unit page shows.
    i)Click Next Step,Edit Algorithm page shows.
    g)Click Next Step,Update User ID page shows.
    k)Click Buld button,then create client update file. In the File List section,a file name like "D:\ClientUpdate1.3.R4Upd" shows.
      This file is encrypted by private key in master dongle.This file and RemoteUpdate tool can be distributed to clients. 

2.  Update slave dongle

    a)Insert slave dongle.
    b)Run RemoteUpdate.exe.
    c)Open the client update file created in step 1(D:\ClientUpdate1.3.R4Upd).
    d)Click Update button. Normally it will be successful. When updating,the update file is decrypted by public key in slave dongle.
      
     If error occurs,please refer to ROCKEY4 SMART License Management dodument.
    

**********************************************************************************













